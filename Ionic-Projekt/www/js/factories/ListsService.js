/**
 * Created by Stephan on 22.02.2015.
 */

/**
 * The ListsService holds all shopping lists of the
 * current user and is capable of updating it from
 * remote sources.
 */

angular.module('starter.ListsService', [])

.factory('ListsService', function(PersistencyService, $q) {
    var lists = [];

    return {
        getLists: function() {
            var dfd = $q.defer();
            lists = PersistencyService.loadLists();
            lists.then(function resolve(result) {
                console.log("got lists: " + JSON.stringify(result));
                lists = result;

                if(lists === undefined)
                    lists = [];

                PersistencyService.updateListsLocal(lists);

                dfd.resolve(lists);
                return lists;
            });
            return dfd.promise;
        },
        getList: function(listId) {
            var dfd = $q.defer();
            lists.forEach(function(list) {
                if (list.id == listId)
                {
                    dfd.resolve(list);
                }
            });
            return dfd.promise;
        },
        removeList: function(listId) {
            for( var i = 0; i < lists.length; i++)
            {
                if( lists[i].id == listId)
                {
                    lists.splice(i, 1);
                    PersistencyService.removeList(listId, lists);
                    return;
                }
            }
        },
        removeItem: function(itemId) {
            for( var i = 0; i < lists.length; i++)
            {
                for( var j = 0; j < lists[i].items.length; j++)
                {
                    if(lists[i].items[j].id == itemId)
                    {
                        lists[i].items.splice(j,1);
                        PersistencyService.removeItem(lists, itemId);
                        return;
                    }
                }
            }
        },
        addList: function(newtitle, newgroup) {
            // id 0 is used, because the server has to add a real id
            var newid = -1;

            var newlist = { id: newid, title: newtitle, group: newgroup.name, groupid: newgroup.id, items: []};
            if( newlist.group == undefined)
                newlist.group = "";
            if( newlist.groupid == undefined )
                newlist.groupid = -1;

            if( lists === undefined) {
                lists = [newlist];
            } else {
                lists.push(newlist);
            }

            return PersistencyService.updateLists(lists);
        },
        updateList: function(newlist) {
            for( var i = 0; i < lists.length; i++)
            {
                if( lists[i].id == newlist.id)
                {
                    lists[i] = newlist;
                    return PersistencyService.updateLists(lists);
                }
            }
        }
    }
});