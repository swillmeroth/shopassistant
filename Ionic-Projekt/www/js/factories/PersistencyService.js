/**
 * Created by Stephan on 22.02.2015.
 */

/**
 * The PersistencyService is responsible for saving and loading data.
 * It usually calls the LocalStorageService and ServerDataService.
 */

angular.module('starter.PersistencyService', [])

.factory('PersistencyService', function(LocalStorageService, ServerDataService, ProfileService, $cordovaNetwork, $cordovaVibration, $ionicPopup, $q, $rootScope) {

    var wasOffline = false;
    var loginDone;

    return {
        // this method is called by the plugin cordova-network and handles the device going offline/online again
        setOnline : function( isOnline) {
            if( isOnline && wasOffline)
            {
                $cordovaVibration.vibrate(100);
                $ionicPopup.alert({
                    title: "Online",
                    template: "<div class='alert'>Sie sind wieder online!</div>"
                });
                wasOffline = false;
                ServerDataService.sendLocalData();
                return;
            }
            if(!isOnline)
            {
                $cordovaVibration.vibrate(100);
                $ionicPopup.alert({
                    title: "Offline",
                    template: "<div class='alert'>Sie sind jetzt offline!</div>"
                });
                wasOffline = true;
            }
        },
        removeList: function(listId, lists) {
            LocalStorageService.saveLists(lists);

            if(!wasOffline) {
                var data = {};
                data.listId = listId;
                data.isMobile = "true";
                ServerDataService.removeList(data);
                $rootScope.$broadcast("OnStatUpdateNeeded", null);
            }
        },
        removeItem: function(lists, itemId) {
            LocalStorageService.saveLists(lists);

            if(!wasOffline) {
                var data = {};
                data.itemId = itemId;
                data.isMobile = "true";
                ServerDataService.removeItem(data);
            }
        },
        loadLists: function() {
            if (wasOffline) {
                console.log("getting offline lists");
                return LocalStorageService.loadLists();
            } else {
                var data = {};
                data.userid = ProfileService.getUserID();
                data.isMobile = "true";
                return ServerDataService.loadLists(data);
            }
        },
        updateLists: function(lists) {
            if( wasOffline) {
                LocalStorageService.saveLists(lists);
                return true;
            } else {
                var data = {};
                data.lists = lists;
                data.userid = ProfileService.getUserID();
                data.isMobile = "true";
                $rootScope.$broadcast("OnStatUpdateNeeded", null);
                return ServerDataService.updateLists(data);
            }
        },
        updateListsLocal: function(lists) {
            LocalStorageService.saveLists(lists);
        },
        initialLogin: function() {
            var loginData = LocalStorageService.loadLoginData();

            console.log("loginData: " + loginData);

            var dfd = $q.defer();

            if( loginData == null)
                loginDone = false;
            else
            {
                loginDone = ProfileService.login(loginData);
            }

            dfd.resolve(loginDone);
            return dfd.promise;
        }
    }
});

