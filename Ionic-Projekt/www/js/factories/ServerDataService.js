/**
 * Created by Stephan on 22.02.2015.
 */

angular.module('starter.ServerDataService', [])

/**
 * The ServerDataService is responsible for all data connections betwenn the client and the server.
 * It is usually called by the PersistencyService.
 */
.factory('ServerDataService', function($rootScope, $http) {
    var errorMessage = "";

    return {
        // loads all lists for a given userid
        getErrorMessage : function() {
            return errorMessage;
        },
        loadLists : function(userid) {
            return $http.post($rootScope.requestURL + $rootScope.listPath + "/getall", JSON.stringify(userid)).
                then( function( response) {
                    var lists = {};
                    lists = response.data.lists;
                    return lists;
                }, function(err) {
                    console.error('ERROR: ', err);
                    return null;
                })
        },
        updateLists : function(lists) {
            return $http.post($rootScope.requestURL + $rootScope.listPath + "/update", JSON.stringify(lists)).
                then( function( response) {
                    return true;
                })
        },
        removeList : function(listId) {
            return $http.post($rootScope.requestURL + $rootScope.listPath + "/remove", JSON.stringify(listId)).
                then( function( response) {
                    return true;
                })
        },
        removeItem : function(itemId) {
            return $http.post($rootScope.requestURL + $rootScope.listPath + "/removeitem", JSON.stringify(itemId)).
                then( function( response) {
                    return true;
                })
        },
        renameGroup : function(groupid, newname) {
            var data = {};
            data.groupid = groupid;
            data.newname = newname;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.groupPath + "/rename", JSON.stringify(data)).
                then( function( response) {
                    return true;
                })
        },
        addMember : function(groupid, username) {
            var data = {};
            data.groupid = groupid;
            data.username = username;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.groupPath + "/addmember", JSON.stringify(data)).
                then( function( response) {
                    if(response.data.isError)
                    {
                        errorMessage = response.data.errorMessage;
                        return false;
                    }

                    return true;
            });
        },
        leaveGroup : function(groupid, userid) {
            var data = {};
            data.groupid = groupid;
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.groupPath + "/leave", JSON.stringify(data)).
                then( function( response) {
                    return true;
                })
        },
        addGroup : function(groupname, userid) {
            var data = {};
            data.groupname = groupname;
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.groupPath + "/add", JSON.stringify(data)).
                then( function( response) {
                    return true;
                })
        }
    }
});