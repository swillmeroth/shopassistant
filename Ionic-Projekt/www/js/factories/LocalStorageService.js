/**
 * Created by Stephan on 22.02.2015.
 */

/**
 * The LocalStorageService saves and loads data from the LocalStorage.
 * It is mainly used by the PersistencyService
 */

angular.module('starter.LocalStorageService', [])

.factory('LocalStorageService', function() {
    return {
        saveLoginData : function(loginData) {
            window.localStorage["loginData"] = JSON.stringify(loginData);
        },
        saveLists : function(lists) {
            window.localStorage['lists'] = JSON.stringify(lists);
        },
        saveItems : function(items) {
            window.localStorage["items"] = JSON.stringify(items);
        },
        saveTempLists : function(lists) {
            window.localStorage["templists"] = JSON.stringify(lists);
        },
        saveTempItems : function(items) {
            window.localStorage["tempitems"] = JSON.stringify(items);
        },
        loadLoginData : function() {
            var loginData = JSON.parse(window.localStorage["loginData"] || null);
            return loginData;
        },
        loadLists : function() {
            var lists = {};
            var storage = window.localStorage['lists'];

            if(storage != "undefined")
                lists = JSON.parse(window.localStorage['lists'] || '{}');

            return lists;
        },
        loadItems : function() {
            var items = JSON.parse(window.localStorage["items"] || null);
            return items;
        },
        loadTempLists : function() {
            var lists = JSON.parse(window.localStorage["templists"] || null);
            return lists;
        },
        loadTempItems : function() {
            var tempitems = JSON.parse(window.localStorage["tempitems"] || null);
            return tempitems;
        }
    }
});