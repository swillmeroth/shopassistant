/**
 * Created by Stephan on 22.02.2015.
 */

/**
 * The ProfileService handles the profile data and is in charge of the login.
 *
 */

angular.module('starter.ProfileService', [])

.factory('ProfileService', function($http, $rootScope, LocalStorageService, $q) {
    var isLoggedIn = false;
    var username;
    var password;
    var email;
    var userid;
    var authToken;
    var groups;
    var favorites;
    var details;
    var errorMessage;

    return {
        getFavorites : function () { return favorites; },
        getGroups : function() { return groups; },
        getUsername : function() { return username; },
        getAuthToken : function() { return authToken; },
        getUserID : function() { return userid; },
        getEMail : function() { return email; },
        getErrorMessage : function() { return errorMessage; },
        isLoggedIn : function() { return isLoggedIn; },
        login : function(loginData) {
            isLoggedIn = $http.post($rootScope.requestURL + $rootScope.authPath + "/login", loginData).
                then(function(response) {

                    isLoggedIn = true;
                    loginData.userid = response.data.userid;
                    userid = loginData.userid;
                    username = response.data.username;
                    email = response.data.email;
                    password = loginData.password;
                    LocalStorageService.saveLoginData(loginData);
                    console.log("Logged in!");

                    authToken = response.data.authToken;
                    $http.defaults.headers.common["IONIC-CLIENT"] = "true";
                    $http.defaults.headers.common["X-AUTH-TOKEN"] = authToken;

                    return true;
                }, function(response) {
                    console.log(JSON.stringify(response));
                    return false;
                });

            return isLoggedIn;
        },
        forgotPassword : function(loginData) {
            isLoggedIn = $http.post($rootScope.requestURL + $rootScope.authPath + "/forgotpass", loginData).
                then(function(response) {
                    return true;
                }, function(response) {
                    console.log(JSON.stringify(response));
                    return false;
                });

            return isLoggedIn;
        },
        logout : function() {
            isLoggedIn = false;
            username = null;
            password = null;
            userid = null;
            LocalStorageService.saveLoginData(null);
            $rootScope.afterLogout = true;
        },
        register : function(registerData) {
            isLoggedIn = $http.post($rootScope.requestURL + $rootScope.authPath + "/register", registerData)
                .then(function(response) {

                    if(response.data.isError)
                    {
                        errorMessage = response.data.errorMessage;
                        return false;
                    }

                    isLoggedIn = true;

                    console.log(JSON.stringify(response.data));
                    registerData.userid = response.data.userid;
                    userid = registerData.userid;
                    username = response.data.username;
                    email = response.data.email;
                    password = registerData.password1;
                    var loginData = {};
                    loginData.userid = userid;
                    loginData.username = username;
                    loginData.password = password;
                    LocalStorageService.saveLoginData(loginData);
                    console.log("Logged in!");

                    authToken = response.data.authToken;
                    $http.defaults.headers.common["X-AUTH-TOKEN"] = authToken;

                    return true;
                }, function(response) {
                    return false;
                });

            return isLoggedIn;
        },
        loadGroups : function() {
            var data = {};
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.groupPath + "/getall", data).then(function(response) {
                    groups = response.data.groups;
                    return groups;
                });
        },
        loadFavorites : function() {
            var data = {};
            data.userid = userid;
            data.isMobile = "true";
            data.favcount = 50; // get the Top 50
            return $http.post($rootScope.requestURL + $rootScope.favoritesPath + "/getall", data).then( function(response) {
                    favorites = response.data.favorites;
                    console.log(JSON.stringify(favorites));
                    return favorites;
            });
        },
        loadDetails : function () {
            var data = {};
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.profilePath + "/getdetails", data).then( function(response) {
                email = response.data.email;
                details = response.data.posts;
                console.log(JSON.stringify(details));
                return details;
            });
        },
        changeName : function (editdata) {
            var data = {};
            data.newname = editdata.username;
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.profilePath + "/changename", data).then( function(response) {

                if(response.data.isError)
                {
                    errorMessage = response.data.errorMessage;
                    return false;
                }

                username = editdata.username;
                loginData = {};
                loginData.userid = userid;
                loginData.username = username;
                loginData.password = password;
                LocalStorageService.saveLoginData(loginData);

                return true;
            }, function(response) {
                return false;
            });
        },
        changeMail : function (editdata) {
            var data = {};
            data.email = editdata.email;
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.profilePath + "/changemail", data).then( function(response) {

                if(response.data.isError)
                {
                    errorMessage = response.data.errorMessage;
                    return false;
                }

                email = editdata.email;

                return true;
            }, function(response) {
                return false;
            });
        },
        changePassword : function (editdata) {
            var data = {};
            data.password = editdata.password1;
            data.userid = userid;
            data.isMobile = "true";
            return $http.post($rootScope.requestURL + $rootScope.profilePath + "/changepassword", data).then( function(response) {

                if(response.data.isError)
                {
                    errorMessage = response.data.errorMessage;
                    return false;
                }

                password = editdata.password1;
                loginData = {};
                loginData.userid = userid;
                loginData.username = username;
                loginData.password = password;
                LocalStorageService.saveLoginData(loginData);

                return true;
            }, function(response) {
                return false;
            });
        }
    }
});