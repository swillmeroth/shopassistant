/**
 * Created by Stephan on 23.02.2015.
 */

angular.module('starter.FavoritesCtrl', [])

.controller('FavoritesCtrl', function($scope, $rootScope, ProfileService, favorites) {

    console.log("I'm here");

    $scope.favorites = favorites;

    // listen for logout event
    $rootScope.$on('OnLogoutEvent', function() {
        console.log("got logout event");
        $scope.favorites = null;
    });

    // listen for login event
    $rootScope.$on('OnLoginDone', function() {
        ProfileService.loadFavorites().then(function (newfavs) {
            $scope.favorites = newfavs;
        });
    });
});