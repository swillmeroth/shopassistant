/**
 * Created by Stephan on 22.02.2015.
 */

angular.module('starter.AppCtrl', [])

.controller('AppCtrl', function($scope, $rootScope, $state, $ionicModal, $ionicPopup, $ionicLoading, $ionicHistory, ProfileService, PersistencyService, LocalStorageService) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.isLoggedIn = ProfileService.isLoggedIn();
    $scope.isOnline = true;
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/modals/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // listen for Online event
    $scope.$on('$cordovaNetwork:online', function(event, networkState){
        PersistencyService.setOnline(true);
        $scope.isOnline = true;
    });

    // listen for Offline event
    $scope.$on('$cordovaNetwork:offline', function(event, networkState){
        PersistencyService.setOnline(false);
        $scope.isOnline = false;
    });

    // listen for login event
    $rootScope.$on('OnLoginDone', function() {
        console.log("got event");
        $scope.isLoggedIn = ProfileService.isLoggedIn();
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        ProfileService.login($scope.loginData).then(function(success) {
            if( success) {
                $scope.isLoggedIn = ProfileService.isLoggedIn();
                $scope.closeLogin();
            }
            else {
                var alertPopup = $ionicPopup.alert({
                    title: "Fehler beim Anmelden",
                    template: "<div class='alert'>Benutzername oder Passwort falsch!</div>"
                });
            }
        });
    };

    $scope.logout = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Ausloggen',
            template: '<div class="alert">M&ouml;chten sie sich wirklich ausloggen?</div>',
            cancelType: 'button-positive',
            cancelText: 'Nein',
            okText: 'Ja'
        });
        confirmPopup.then(function(res) {
            if(res) {
                console.log("doing logout!");
                ProfileService.logout();
                $scope.isLoggedIn = false;
                $rootScope.$broadcast("OnLogoutEvent", null);
                $state.go('loadingLogin');
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                window.open("#/home", '_self');
            }
        });
    };

    $scope.goBack = function() {
        $rootScope.$broadcast("OnBackEvent", null);
        $ionicHistory.goBack();
    }
});
