/**
 * Created by Stephan on 23.02.2015.
 */


angular.module('starter.GroupCtrl', [])

.controller('GroupCtrl', function($scope, $rootScope, ProfileService, ServerDataService, groups, $ionicPopup) {

    $scope.groups = groups;
    $scope.newgroup = { id: -1, name: "", members: []};

    // listen for logout event
    $rootScope.$on('OnLogoutEvent', function() {
        console.log("got logout event");
        $scope.groups = null;
    });

    // listen for login event
    $rootScope.$on('OnLoginDone', function() {
        ProfileService.loadGroups().then(function (newgroups) {
            $scope.groups = newgroups;
        });
    });

    $scope.onExitClick = function(group) {
        $scope.group = group;
        var confirmPopup = $ionicPopup.confirm({
            title: 'Best&auml;tigung',
            template: 'Wollen sie die Gruppe wirklich verlassen?',
            cancelText: 'Nein',
            okText: 'Ja'
        });
        confirmPopup.then(function(res) {
            if(res) {
                $scope.leaveGroup();
            }
        });
    };

    $scope.onNewClick = function() {
        var myPopup = $ionicPopup.show({
            template: '<ion-item class="item-input"><input type="text" name="name" ng-model="newgroup.name" required/></ion-item>',
            title: 'Neue Gruppe',
            scope: $scope,
            buttons: [
                { text: 'Abbrechen' },
                {
                    text: 'Erstellen',
                    type: 'button-positive',
                    onTap: function(e) {
                        if ($scope.newgroup.name == "") {
                            e.preventDefault();
                        } else {
                            $scope.addGroup();
                        }
                    }
                }
            ]
        });
    };

    $scope.onAddClick = function(group) {
        $scope.group = group;
        var myPopup = $ionicPopup.show({
            template: '<ion-item class="item-input"><input type="text" name="name" ng-model="group.newmember" required/></ion-item>',
            title: 'Mitglied hinzf&uuml;gen',
            scope: $scope,
            buttons: [
                { text: 'Abbrechen' },
                {
                    text: 'Hinzuf&uuml;gen',
                    type: 'button-positive',
                    onTap: function(e) {
                        if ($scope.newmember == "") {
                            e.preventDefault();
                        } else {
                            $scope.addMember();
                        }
                    }
                }
            ]
        });
    };

    $scope.onEditClick = function(group) {
        $scope.group = group;
        var myPopup = $ionicPopup.show({
            template: '<ion-item class="item-input"><input type="text" name="name" ng-model="group.name" required/></ion-item>',
            title: 'Umbenennen',
            scope: $scope,
            buttons: [
                { text: 'Abbrechen' },
                {
                    text: 'Speichern',
                    type: 'button-positive',
                    onTap: function(e) {
                        if ($scope.group.name == "") {
                            e.preventDefault();
                        } else {
                            $scope.renameGroup();
                        }
                    }
                }
            ]
        });
    };

    $scope.addGroup = function() {
        return ServerDataService.addGroup($scope.newgroup.name, ProfileService.getUserID()).then( function(response) {
            ProfileService.loadGroups().then( function (response) {
                $scope.groups = ProfileService.getGroups();
                $rootScope.$broadcast("OnGroupEvent", null);
                return true;
            })
        });
    };

    $scope.leaveGroup = function() {
        return ServerDataService.leaveGroup($scope.group.id, ProfileService.getUserID()).then( function(response) {
            ProfileService.loadGroups().then( function (response) {
                $scope.groups = ProfileService.getGroups();
                $rootScope.$broadcast("OnGroupEvent", null);
                return true;
            })
        });
    };

    $scope.renameGroup = function()
    {
        return ServerDataService.renameGroup($scope.group.id, $scope.group.name).then( function(response) {
            ProfileService.loadGroups().then( function (response) {
                $scope.groups = ProfileService.getGroups();
                $rootScope.$broadcast("OnGroupEvent", null);
                return true;
            })
        });
    };

    $scope.addMember = function()
    {
        return ServerDataService.addMember($scope.group.id, $scope.group.newmember).then( function(success) {
            if( success)
            {
                ProfileService.loadGroups().then( function (response) {
                    $scope.groups = ProfileService.getGroups();
                    $rootScope.$broadcast("OnGroupEvent", null);
                    return true;
                });
            }
            else
            {
                var errorMessage = ServerDataService.getErrorMessage();
                var alertPopup = $ionicPopup.alert({
                    title: 'Fehler',
                    template: errorMessage
                });
            }
        });
    };
});