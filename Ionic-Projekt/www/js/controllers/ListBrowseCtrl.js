/**
 * Created by Stephan on 22.02.2015.
 */

angular.module('starter.ListBrowseCtrl', [])

.controller('ListBrowseCtrl', function($scope, $rootScope, $ionicHistory, $ionicPopup, $ionicModal, $ionicListDelegate, $http, $state, $timeout, ListsService, ProfileService, lists, favorites, groups) {

    if($rootScope.afterLogout)
    {
        console.log("doing clear");
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
    }

    $scope.lists = lists;
    $scope.noLists = (lists.length == 0);

    $scope.newlist = { id: -1, title: "", group: "", groupid: -1, items: []};
    $scope.testlist = "HALLO";
    $scope.groups = groups;

    // add empty group
    var emptygroup = {};
    emptygroup.id = -1;
    emptygroup.name = "";
    emptygroup.members = [];
    $scope.groups.push(emptygroup);

    $scope.data = {};
    $scope.data.selectedGroup = {};

    // listen for login event
    $rootScope.$on('OnLogoutEvent', function() {
        console.log("got logout event");
        $scope.lists = null;
    });

    // listen for login event
    $rootScope.$on('OnLoginDone', function() {
        ListsService.getLists().then(function (newlists) {
            $scope.lists = newlists;
            $scope.noLists = (lists.length == 0);
        });
    });

    // listen for back event from list
    $rootScope.$on("OnBackEvent", function() {
        console.log("got back event");
        ListsService.getLists().then(function (newlists) {
            $scope.lists = newlists;
            $scope.noLists = (lists.length == 0);
        })
    });

    // when changes to groups happen, this function will be called.
    $rootScope.$on("OnGroupEvent", function() {
        console.log("got group event");
        $scope.groups = ProfileService.getGroups();
    });

    // Create the add modal that we will use later
    $ionicModal.fromTemplateUrl('templates/modals/addlist.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the add modal to close it
    $scope.closeAdd = function() {
        $scope.modal.hide();
    };

    // Open the add modal
    $scope.showAdd = function() {
        console.log(JSON.stringify($scope.groups));
        $scope.modal.show();
    };

    $scope.addList = function(){
        if( $scope.newlist.title == "") {
            var alertPopup = $ionicPopup.alert({
                title: 'Fehler beim Hinzuf&uuml;gen',
                template: '<div class="alert">Bitte geben sie einen Titel ein!</div>'
            });
        }
        else {

            console.log(JSON.stringify($scope.data.selectedGroup));
            ListsService.addList($scope.newlist.title, $scope.data.selectedGroup).then( function reload(response) {
                ListsService.getLists().then(function (newlists) {
                    $scope.lists = newlists;
                    $scope.noLists = (lists.length == 0);
                    $scope.newlist = { id: -1, title: "", group: "", groupid: -1};
                    $scope.data.selectedGroup = {};

                    $scope.closeAdd();
                })
            });
        }
    };

    $scope.removeList = function(list) {
        ListsService.removeList(list.id);
        ListsService.getLists().then(function (newlists) {
            $scope.lists = newlists;
            $scope.noLists = (lists.length == 0);
        });
    };

    $ionicModal.fromTemplateUrl('templates/modals/editlist.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.editmodal = modal;
    });

    $scope.closeEdit = function() {
        $scope.editmodal.hide();
    };

    $scope.showEdit = function(list) {
        $scope.list = list;
        $scope.prevtitle = list.title;
        $scope.prevgroupid = list.groupid;
        $scope.editmodal.show();
    };

    $scope.editList = function() {
        var list = $scope.list;

        if( $scope.prevtitle != list.title || $scope.prevgroupid != list.groupid){
            ListsService.updateList(list).then( function (success) {
                ListsService.getLists().then(function (newlists) {
                    $scope.lists = newlists;
                    $scope.noLists = (lists.length == 0);
                });
            });
            $ionicListDelegate.closeOptionButtons();
            $rootScope.$broadcast("closeOptionButtons", null);
        }
        else {
            $ionicListDelegate.closeOptionButtons();
            $rootScope.$broadcast("closeOptionButtons", null);
        }

        $scope.closeEdit();
    };

});
