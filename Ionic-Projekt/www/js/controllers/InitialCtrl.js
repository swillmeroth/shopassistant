/**
 * Created by Stephan on 22.02.2015.
 */

angular.module('starter.InitialCtrl', [])

.controller('InitialCtrl', function($scope, $state, $rootScope, $ionicModal, $ionicPopup, $ionicHistory, ProfileService, loginDone) {


    var afterLogin = function() {
        $rootScope.$broadcast("OnLoginDone", null);
        $state.go('app.listbrowse');
    };

    if(loginDone) {
        afterLogin();
    }
    else
    {
        // Form data for the login modal
        $scope.loginData = {};
        $scope.loginData.isMobile = "true";
        $scope.noclose = true;

        /*
        LOGIN MODAL
         */

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/modals/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.login();
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function() {
            ProfileService.login($scope.loginData).then(function(success) {
                if( success) {
                    $scope.closeLogin();
                    afterLogin();
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        title: "Fehler beim Anmelden",
                        template: "<div class='alert'>Benutzername oder Passwort falsch!</div>"
                    });
                }
            });
        };

        /*
         FORGOT PASSWORD MODAL
         */

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/modals/forgotpassword.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.forgotmodal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeForgotPassword = function() {
            $scope.forgotmodal.hide();
            $scope.modal.show();
        };

        // Open the login modal
        $scope.showForgotPassword = function() {
            $scope.modal.hide();
            $scope.forgotmodal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.forgotPassword = function() {
            ProfileService.forgotPassword($scope.loginData).then(function(success) {
                if( success) {
                    var alertPopupSuccess = $ionicPopup.alert({
                        title: "Passwort vergessen",
                        template: "<div class='alert'>Ihr neues Passwort wurde an ihre E-Mail-Adresse versandt!</div>"
                    });
                    alertPopupSuccess.then( function() {
                        $scope.closeForgotPassword();
                    })
                }
                else {
                    $ionicPopup.alert({
                        title: "Fehler",
                        template: "<div class='alert'>Der angegebene Username ist nicht bekannt!</div>"
                    });
                }
            });
        };

        /*
         REGISTER MODAL
         */

        $scope.registerdata = {};
        $scope.registerdata.isMobile = "true";

        $ionicModal.fromTemplateUrl('templates/modals/register.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.registermodal = modal;
        });

        $scope.closeRegister = function() {
            $scope.registermodal.hide();
            $scope.modal.show();
        };

        $scope.onlyCloseRegister = function() {
            $scope.registermodal.hide();
        };

        $scope.showRegister = function() {
            $scope.modal.hide();
            $scope.registermodal.show();
        };

        $scope.doRegister = function() {
            if($scope.registerdata.password1 != $scope.registerdata.password2)
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Fehler',
                    template: '<div class="alert">Ihre Passw&ouml;rter stimmen nicht &uuml;berein!</div>'
                });
            }
            else
            {
                ProfileService.register($scope.registerdata).then(function(success) {
                    console.log("after login: " + success);
                    if( success)
                    {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Registrierung',
                            template: '<div class="alert">Registrierung erfolgreich!</div>'
                        });
                        alertPopup.then(function(res) {
                            $scope.onlyCloseRegister();
                            $scope.closeLogin();
                            afterLogin();
                        });
                    }
                    else
                    {
                        var errorMessage = ProfileService.getErrorMessage();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Fehler',
                            template: errorMessage
                        });
                    }
                })
            }
        };
    }
});