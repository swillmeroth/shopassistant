/**
 * Created by Stephan on 22.02.2015.
 */

angular.module('starter.ShoppingListCtrl', [])

    .controller('ShoppingListCtrl', function ($scope, $rootScope, $state, $ionicPopup, $ionicListDelegate, $ionicHistory, $ionicModal, list, ListsService, $cordovaVibration, favorites) {


        //-------------------------------------------//
        //                 VARIABLES                 //
        //-------------------------------------------//

        $scope.showDeleted = false;
        $scope.emptyList = false;
        $scope.listdata = list;
        $scope.favorites = favorites;
        $scope.item = {};                                                                            // variable for accessing item via scope (when editing)
        $scope.newitem = { text: "", checked: false, store: "", storedetail: "", number: 1, detail:"", id: -1};  // default item for adding new ones
        $scope.groups = [];                                                                     // array of groups (filled after sorting)
        $scope.doneList = [];                                                                   // list of bought elements
        $scope.data = {
            predicate: "count",
            reverse: true,
            sortText: "3 2 1",
            showDelete: false,
            showReorder: false
        };                                                                                      // data for list functions (delete and reorder)


        //-------------------------------------------//
        //                   MODALS                  //
        //-------------------------------------------//

        $ionicModal.fromTemplateUrl('templates/modals/additem.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.addmodal = modal;
        });

        $scope.closeAdd = function() {
            $scope.addmodal.hide();
        };

        $scope.showAdd = function() {
            $scope.addmodal.show();
        };

        $ionicModal.fromTemplateUrl('templates/modals/edititem.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.editmodal = modal;
        });

        $scope.closeEdit = function() {
            $scope.editmodal.hide();
        };

        $scope.showEdit = function(item) {
            $scope.item = item;
            //TODO: find way to store complete previous item
            $scope.prevtext = item.text;
            $scope.prevnumber = item.number;
            $scope.prevstore = item.store;
            $scope.prevdetail = item.detail;
            $scope.prevstoredetail = item.storedetail;

            $scope.editmodal.show();
        };

        $ionicModal.fromTemplateUrl('templates/modals/addfavorite.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.addfavmodal = modal;
        });

        $scope.closeAddFav = function() {
            $scope.addfavmodal.hide();
        };

        $scope.showAddFav = function(item) {
            $scope.addfavmodal.show();
        };

        $scope.toggleSort = function() {
            if($scope.data.predicate == "name") {
                $scope.data.predicate = "count";
                $scope.data.reverse = true;
                $scope.data.sortText = "3 2 1";
            }
            else {
                $scope.data.predicate = "name";
                $scope.data.reverse = false;
                $scope.data.sortText = "a b c";
            }
        };

        //-------------------------------------------//
        //            TRIGGERED FUNCTIONS            //
        //-------------------------------------------//

        $scope.onAddFav = function(favorite) {
            console.log(JSON.stringify(favorite));

            $scope.newitem.text = favorite.name;
            $scope.newitem.detail = favorite.detail;
            $scope.newitem.store = favorite.store;
            $scope.newitem.storedetail = favorite.storedetail;

            $scope.closeAddFav();
        };

        $scope.toggleShowDeleted = function()
        {
            $scope.showDeleted = !$scope.showDeleted;
            doListReset();
        };

        $scope.onItemDelete = function(item) {
            for( var i = 0; i < $scope.groups.length; i++)
            {
                // find the right subgroup (by store)
                if( $scope.groups[i].name == item.store)
                {
                    // remove element from lists (sorted and full)
                    ListsService.removeItem(item.id);
                    $scope.groups[i].items.splice($scope.groups[i].items.indexOf(item), 1);

                    // if no items remain, remove empty group
                    if($scope.groups[i].items.length == 0)
                        $scope.groups.splice($scope.groups.indexOf($scope.groups[i]), 1);

                    break;
                }
            }
        };

        $scope.onItemChecked = function(item) {
            /* if an item is clicked:
             step 1: add item to bought items
             step 2: remove item from old list
             step 3: set item in listdata as checked
             step 4: persistent saving (local and remote)
             step 5: if all items are bought, a completion dialog is shown
             */


            if(window.cordova)
            {
                $cordovaVibration.vibrate(100);
            }

            // Schritt 1
            $scope.doneList.unshift(item);

            // Schritt 2
            for( var i = 0; i < $scope.groups.length; i++)
            {
                // find the right subgroup (by store)
                if( $scope.groups[i].name == item.store)
                {
                    // remove element from lists (sorted and full)
                    $scope.groups[i].items.splice($scope.groups[i].items.indexOf(item), 1);

                    // if no items remain, remove empty group
                    if($scope.groups[i].items.length == 0)
                        $scope.groups.splice($scope.groups.indexOf($scope.groups[i]), 1);

                    break;
                }
            }

            // Schritt 3
            var previtem = item;
            previtem.checked = false;
            var i = $scope.listdata.items.indexOf(previtem);
            previtem.checked = true;
            $scope.listdata.items[i] = item;

            // Schritt 4
            ListsService.updateList($scope.listdata);

            console.log(JSON.stringify($scope.listdata));

            // Schritt 5
            if($scope.getUncheckedCount() == 0)
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Fertig!',
                    template: '<div class="alert">Sie haben alles gekauft!<br>Wollen Sie die Liste l&ouml;schen?</div>',
                    cancelType: 'button-positive',
                    cancelText: 'Nein',
                    okText: 'Ja'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        ListsService.removeList($scope.listdata.id);
                        $rootScope.$broadcast("OnBackEvent", null);
                        $state.go('app.listbrowse');
                    }
                });
            }
        };

        $scope.getUncheckedCount = function() {
            var count = 0;

            for (var i = 0; i < $scope.listdata.items.length; i++)
            {
                if( !$scope.listdata.items[i].checked)
                {
                    count++;
                }
            }

            console.log("unchecked: " + count);
            return count;
        };

        $scope.onItemUnchecked = function(item) {
            /* if an already bought item is clicked:
             step 1: remove from bought items
             step 2: add to normal list
             step 3: persistent saving
             step 4: reset list
             */

            // Schritt 1
            $scope.doneList.splice($scope.doneList.indexOf(item), 1);

            // Schritt 2
            var previtem = item;
            previtem.checked = true;
            var i = $scope.listdata.items.indexOf(previtem);
            console.log("found checked item at " + i);
            previtem.checked = false;
            $scope.listdata.items[i] = item;
            console.log(JSON.stringify($scope.listdata));

            // Schritt 3
            ListsService.updateList($scope.listdata);

            // Schritt 4
            doListReset();
        };

        $scope.onStoreClick = function(name) {
            // erstmal nichts ben�tigt
        };

        $scope.onBoughtListClick = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Aufr&auml;umen',
                template: '<div class="alert">M&ouml;chten Sie alle gekauften Elemente l&ouml;schen?</div>',
                cancelType: 'button-positive',
                cancelText: 'Nein',
                okText: 'Ja'
            });
            confirmPopup.then(function(res) {
                if(res) {
                    $scope.doneList = [];
                }
            });
        };


        //-------------------------------------------//
        //              CALLED FUNCTIONS             //
        //-------------------------------------------//

        $scope.addItem = function() {
            if( $scope.newitem.text == "")
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Fehler beim Hinzuf�gen',
                    template: '<div class="alert">Bitte geben sie eine Bezeichnung ein!</div>'
                });
            }
            else
            {
                $scope.listdata.items.push($scope.newitem);
                ListsService.updateList($scope.listdata).then( function reload(response) {
                    ListsService.getLists().then(function (newlists) {
                        // find list in new lists
                        for( var i = 0; i < newlists.length; i++)
                        {
                            if( $scope.listdata.id == newlists[i].id)
                                $scope.listdata = newlists[i];
                        }
                        doListReset();
                        $scope.newitem = { text: "", checked: false, store: "", storedetail: "", number: 1, detail:"", id: -1};
                        $scope.closeAdd();
                    })
                });
            }
        };

        $scope.editItem = function() {
            var item = $scope.item;

            if( $scope.prevstore != item.store) {
                doListReset();
                ListsService.updateList($scope.listdata);
            }
            // TODO: change to complete item comparison
            else if( $scope.prevtext != item.text || $scope.prevnumber != item.number || $scope.prevdetail != item.detail || $scope.prevstoredetail != item.storedetail) {
                ListsService.updateList($scope.listdata);
                $ionicListDelegate.closeOptionButtons();
                $rootScope.$broadcast("closeOptionButtons", null);
            }
            else {
                $rootScope.$broadcast("closeOptionButtons", null);
                $ionicListDelegate.closeOptionButtons();
            }

            $scope.closeEdit();
        };

        $scope.sortByStore = function(a,b){
            if(a.store < b.store) {
                return -1;
            }
            if (a.store > b.store) {
                return 1;
            }
            return 0;
        };

        var doListReset = function() {

            if($scope.listdata.items.length == 0) {
                $scope.emptyList = true;
                return;
            }

            $scope.emptyList = false;

            $scope.doneList = [];

            // sort out bought elements
            for ( var i = 0; i < $scope.listdata.items.length; i++)
            {
                var item = $scope.listdata.items[i];
                if( item.checked && $scope.showDeleted) {
                    $scope.doneList.unshift(item);
                }
            }

            // sort list by store
            $scope.listdata.items.sort($scope.sortByStore);

            // reset the groups
            $scope.groups = [];

            var storeValue = "_INVALID_STORE_VALUE_";
            var storedetailValue = "_INVALID_STOREDETAIL_VALUE_";

            // loop over each item and create new group if different than storeValue
            for ( var i = 0 ; i < $scope.listdata.items.length ; i++ )
            {
                var item = $scope.listdata.items[i];

                if(!item.checked) {
                    // Should we create a new group?
                    if (item.store != storeValue || item.storedetail != storedetailValue)
                    {
                        var group = {
                            name: item.store,
                            detail: item.storedetail,
                            items: []
                        };

                        storeValue = group.name;
                        storedetailValue = group.detail;

                        $scope.groups.push(group);
                    }

                    // Add the item to the currently active group
                    group.items.push(item);
                }
            }
        };

        //-------------------------------------------//
        //              EXECUTED CODE                //
        //-------------------------------------------//

        doListReset();
});