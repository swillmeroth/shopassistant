/**
 * Created by Stephan on 09.03.2015.
 */

angular.module('starter.ProfileCtrl', [])

.controller('ProfileCtrl', function($scope, $rootScope, ProfileService, $ionicPopup, $ionicModal, details) {
    $scope.profile = {};
    $scope.editdata = {};
    $scope.profile.name = ProfileService.getUsername();
    $scope.profile.email = ProfileService.getEMail();
    $scope.profile.details = details;
    $scope.details = details;

    // listen for logout event
    $rootScope.$on('OnLogoutEvent', function() {
        console.log("got logout event");
        $scope.profile = {};
    });

    // listen for login event
    $rootScope.$on('OnLoginDone', function() {
        ProfileService.loadDetails().then(function (newdetails) {
            $scope.profile.details = newdetails;
            $scope.profile.name = ProfileService.getUsername();
            $scope.profile.email = ProfileService.getEMail();
        });
    });

    // listen for login event
    $rootScope.$on('OnStatUpdateNeeded', function() {
        ProfileService.loadDetails().then(function (newdetails) {
            $scope.profile.details = newdetails;
            $scope.profile.name = ProfileService.getUsername();
            $scope.profile.email = ProfileService.getEMail();
        });
    });

    $scope.afterUpdate = function() {
        $scope.profile.name = ProfileService.getUsername();
        console.log(JSON.stringify($scope.profile.name));
        $scope.profile.email = ProfileService.getEMail();
    };

    /*
     Username modal
     */

    $ionicModal.fromTemplateUrl('templates/modals/changename.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.namemodal = modal;
    });

    $scope.closeChangeName = function() {
        $scope.namemodal.hide();
    };

    $scope.showChangeName = function() {
        $scope.editdata = {};
        $scope.editdata.username = $scope.profile.name;
        $scope.namemodal.show();
    };

    $scope.changeName = function() {
        ProfileService.changeName($scope.editdata).then(function(success) {
            if( success)
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Username &auml;ndern',
                    template: '<div class="alert">&Auml;ndern erfolgreich!</div>'
                });
                alertPopup.then(function(res) {
                    $scope.closeChangeName();
                    $scope.afterUpdate();
                });
            }
            else
            {
                var errorMessage = ProfileService.getErrorMessage();
                var alertPopup = $ionicPopup.alert({
                    title: 'Fehler',
                    template: errorMessage
                });
            }
        })
    };

    /*
     Password modal
     */

    $ionicModal.fromTemplateUrl('templates/modals/changepassword.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.passmodal = modal;
    });

    $scope.closeChangePassword = function() {
        $scope.passmodal.hide();
    };

    $scope.showChangePassword = function() {
        $scope.editdata = {};
        $scope.passmodal.show();
    };

    $scope.changePassword = function() {
        if($scope.editdata.password1 != $scope.editdata.password2)
        {
            var alertPopup = $ionicPopup.alert({
                title: 'Fehler',
                template: '<div class="alert">Ihre Passw&ouml;rter stimmen nicht &uuml;berein!</div>'
            });
        }
        else {
            ProfileService.changePassword($scope.editdata).then(function (success) {
                if (success) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Passwort &auml;ndern',
                        template: '<div class="alert">&Auml;ndern erfolgreich!</div>'
                    });
                    alertPopup.then(function (res) {
                        $scope.closeChangePassword();
                    });
                }
                else {
                    var errorMessage = ProfileService.getErrorMessage();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Fehler',
                        template: errorMessage
                    });
                }
            })
        }
    };

    /*
     E-Mail modal
     */

    $ionicModal.fromTemplateUrl('templates/modals/changemail.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.emailmodal = modal;
    });

    $scope.closeChangeMail = function() {
        $scope.emailmodal.hide();
    };

    $scope.showChangeMail = function() {
        $scope.editdata = {};
        $scope.editdata.email = $scope.profile.email;
        $scope.emailmodal.show();
    };

    $scope.changeMail = function() {
        ProfileService.changeMail($scope.editdata).then(function(success) {
            if( success)
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'E-Mail &auml;ndern',
                    template: '<div class="alert">&Auml;ndern erfolgreich!</div>'
                });
                alertPopup.then(function(res) {
                    $scope.closeChangeMail();
                    $scope.afterUpdate();
                });
            }
            else
            {
                var errorMessage = ProfileService.getErrorMessage();
                var alertPopup = $ionicPopup.alert({
                    title: 'Fehler',
                    template: errorMessage
                });
            }
        })
    };
});