angular.module('starter', [
    'ionic',
    'ui.router',
    'starter.AppCtrl',
    'starter.InitialCtrl',
    'starter.ListBrowseCtrl',
    'starter.ProfileCtrl',
    'starter.ShoppingListCtrl',
    'starter.GroupCtrl',
    'starter.FavoritesCtrl',
    'starter.ServerDataService',
    'starter.PersistencyService',
    'starter.LocalStorageService',
    'starter.ProfileService',
    'starter.ListsService',
    'ngCordova'
])

.run(function($rootScope) {
    // config for webservice
    $rootScope.serverAddress    = "http://193.175.86.175";
    //$rootScope.serverAddress    = "http://10.0.2.2";
    //$rootScope.serverAddress    = "http://localhost";

    $rootScope.serverPort       = "8080";
    $rootScope.baseURL          = "/ShopAssistant/webresources";

    $rootScope.authPath         = "/auth";
    $rootScope.listPath         = "/data/lists";
    $rootScope.groupPath        = "/data/groups";
    $rootScope.favoritesPath    = "/data/favorites";
    $rootScope.profilePath      = "/data/profile";

    $rootScope.requestURL       = $rootScope.serverAddress + ":" + $rootScope.serverPort + $rootScope.baseURL;
    })

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.directive('holdForOptionsWrapper', [function() {
    return {
        restrict: 'A',
        controller: function($scope, $rootScope) {

            $rootScope.$on("closeOptionButtons", function() {
                $scope.$broadcast('closeOptions');
            });

            this.closeOptions = function() {
                $scope.$broadcast('closeOptions');
            }
        }
    };
}])

.directive('holdForOptions', ['$ionicGesture', function($ionicGesture) {
    return {
        restrict: 'A',
        scope: false,
        require: '^holdForOptionsWrapper',
        link: function (scope, element, attrs, parentController) {
            // A basic variable that determines wether the element was currently clicked
            var clicked;

            // Set an initial attribute for the show state
            attrs.$set('optionButtons', 'hidden');

            // Grab the content
            var content = element[0].querySelector('.item-content');

            // Grab the buttons and their width
            var buttons = element[0].querySelector('.item-options');

            var closeAll = function() {
                element.parent()[0].$set('optionButtons', 'show');
            };

            // Add a listener for the broadcast event from the parent directive to close
            var previouslyOpenedElement;
            scope.$on('closeOptions', function() {
                if (!clicked) {
                    attrs.$set('optionButtons', 'hidden');
                }
            });

            // Function to show the options
            var showOptions = function() {
                // close all potentially opened items first
                parentController.closeOptions();

                var buttonsWidth = buttons.offsetWidth;
                ionic.requestAnimationFrame(function() {
                    // Add the transition settings to the content
                    content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';

                    // Make the buttons visible and animate the content to the left
                    buttons.classList.remove('invisible');
                    content.style[ionic.CSS.TRANSFORM] = 'translate3d(-' + buttonsWidth + 'px, 0, 0)';

                    // Remove the transition settings from the content
                    // And set the "clicked" variable to false
                    setTimeout(function() {
                        content.style[ionic.CSS.TRANSITION] = '';
                        clicked = false;
                    }, 250);
                });
            };

            // Function to hide the options
            var hideOptions = function() {
                var buttonsWidth = buttons.offsetWidth;
                ionic.requestAnimationFrame(function() {
                    // Add the transition settings to the content
                    content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';

                    // Move the content back to the original position
                    content.style[ionic.CSS.TRANSFORM] = '';

                    // Make the buttons invisible again
                    // And remove the transition settings from the content
                    setTimeout(function() {
                        buttons.classList.add('invisible');
                        content.style[ionic.CSS.TRANSITION] = '';
                    }, 250);
                });
            };

            // Watch the open attribute for changes and call the corresponding function
            attrs.$observe('optionButtons', function(value){
                if (value == 'show') {
                    showOptions();
                } else {
                    hideOptions();
                }
            });

            // Change the open attribute on tap
            $ionicGesture.on('hold', function(e){
                clicked = true;
                if (attrs.optionButtons == 'show') {
                    attrs.$set('optionButtons', 'hidden');
                } else {
                    attrs.$set('optionButtons', 'show');
                }
            }, element);
        }
    };
}])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // only show back icon, no text
    $ionicConfigProvider.backButton.text("").previousTitleText(false);

    $stateProvider

        .state('loadingLogin', {
            url: "/home",
            templateUrl: "index.html",
            controller: 'InitialCtrl',
            cache: false,
            resolve: {
                loginDone: function (PersistencyService) {
                    return PersistencyService.initialLogin();
                }
            }
        })

    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    .state('app.profile', {
        url: "/profile",
        views: {
            'menuContent': {
                templateUrl: "templates/profile.html",
                controller: 'ProfileCtrl',
                resolve: {
                    details: function(ProfileService) {
                        return ProfileService.loadDetails();
                    }
                }
            }
        }
    })

    .state('app.favorites', {
        url: "/favorites",
        views: {
            'menuContent': {
                templateUrl: "templates/favorites.html",
                controller: 'FavoritesCtrl',
                resolve: {
                    favorites: function(ProfileService) {
                        return ProfileService.loadFavorites();
                    }
                }
            }
        }
    })

    .state('app.groups', {
        url: "/groups",
        views: {
            'menuContent': {
                templateUrl: "templates/groups.html",
                controller: 'GroupCtrl',
                resolve: {
                    groups: function (ProfileService) {
                        return ProfileService.loadGroups();
                    }
                }
            }
        }
    })

    .state('app.listbrowse', {
        url: "/listbrowse",
        views: {
            'menuContent': {
                templateUrl: "templates/listbrowse.html",
                controller: 'ListBrowseCtrl',
                resolve: {
                    lists: function(ListsService) {
                        return ListsService.getLists();
                    },
                    favorites: function(ProfileService) {
                        return ProfileService.loadFavorites();
                    },
                    groups: function (ProfileService) {
                        return ProfileService.loadGroups();
                    }
                }
            }
        }
    })

    .state('app.list', {
        url: "/listbrowse/:listId",
        views: {
            'menuContent': {
                templateUrl: "templates/list.html",
                controller: 'ShoppingListCtrl',
                resolve: {
                    list: function($stateParams, ListsService) {
                        return ListsService.getList($stateParams.listId)
                    },
                    favorites: function(ProfileService) {
                        return ProfileService.loadFavorites();
                    }
                }
            }
        }
    });

  // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home');
});