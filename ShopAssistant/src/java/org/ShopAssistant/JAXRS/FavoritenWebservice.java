/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JAXRS;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.FavoritesResult;
import org.ShopAssistant.Entities.Listenpositionen;
import org.ShopAssistant.webservice.PersistenceWrapper;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
@Path("data/favorites")
public class FavoritenWebservice {  
    
    private String FAVORITEN_SQL_STRING = "SELECT lp.ID, lp.Name, lp.Kommentar, lp.Geschaeftsname, lp.Geschaeftsdetails, count(lp.ID) AS anzahl FROM Listenpositionen lp WHERE lp.ErstelltVon = ? GROUP BY lp.Name, lp.Kommentar, lp.Geschaeftsname, lp.Geschaeftsdetails ORDER BY count(lp.ID) DESC";
    /**
     * Bekommt vom Client eine Userid und liefert einen String mit allen Favoriten (Name und Anzahl der Verwendungen).
     * Die Favoriten sind eine Liste an FavoritesResult-Entities.
     * 
     * @param content JSON mit Userid
     * @return  JSON-String mit allen Gruppen
     */
    @Path("/getall")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getUsersFavorites(String content)
    {  
        try {
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            JSONArray json_favoriten = new JSONArray();
            JSONObject json_favorit;
            
            JSONObject data = new JSONObject(content);
            int userid = data.getInt("userid");
            int favcount = data.getInt("favcount");
            
            String isMobileString;
            boolean isMobile = false;
            isMobileString = data.getString("isMobile");
            if (isMobileString.equals("true"))
                isMobile = true;
            
            List<FavoritesResult> result = new ArrayList();
            System.out.println("Getting all favorites for user " + userid);
            
            //clear 2nd level cache
            em.getEntityManagerFactory().getCache().evictAll();
            
            Query query = em.createNativeQuery(FAVORITEN_SQL_STRING, "favoritesMap").setParameter(1, userid).setMaxResults(favcount);
            result.addAll(query.getResultList());
            
           
            System.out.println("Resultsize: " + result.size());
            
            if(!isMobile) {
                System.out.println("not mobile");
                /*// convert FavoritesResults to Listenpositionen
                
                List<Listenpositionen> nonMobileResult = new ArrayList<>();
                
                for ( FavoritesResult fav : result)
                {
                    Listenpositionen pos = new Listenpositionen();
                    
                    pos.setAnzahl(1);
                    pos.setName(fav.getName());
                    pos.setKommentar(fav.getDetail());
                    pos.setGeschaeftsname(fav.getStore());
                    pos.setGeschaeftsdetails(fav.getStoredetails());
                    
                    nonMobileResult.add(pos);
                }*/
                
                return Response.ok(result).build();
            }
            
            for( FavoritesResult favorit : result)
            {
                json_favorit = new JSONObject();
                
                json_favorit.put("id", favorit.getId());
                json_favorit.put("name", favorit.getName());
                json_favorit.put("detail", favorit.getDetail());
                json_favorit.put("store", favorit.getStore());
                json_favorit.put("storedetail", favorit.getStoredetails());
                json_favorit.put("count", favorit.getAnzahl());
                
                json_favoriten.put(json_favorit);
            }
            
            JSONObject resultObject = new JSONObject().put("favorites", json_favoriten);
            System.out.println(resultObject.toString());
            
            return Response.ok(resultObject.toString()).build();
        } catch (JSONException ex) {
            Logger.getLogger(FavoritenWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}