/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JAXRS;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Entities.Listen;
import org.ShopAssistant.Entities.Listenpositionen;
import org.ShopAssistant.webservice.PersistenceWrapper;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
@Path("data/lists")
public class ListWebservice 
{   
    /**
     * Bekommt vom Client eine Userid und liefert einen String mit allen Listen (inkl aller Listenpositionen) zurück.
     * Der zurückgelieferte String wird aus einzelnen JSONObjects zusammengesetzt. Siehe hierzu die Methode entities2json.
     * 
     * @param content JSON vom Client
     * @return  JSON-String mit allen Listen inkl Positionen 
     */
    @Path("/getall")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getListsJSON(String content)
    {        
        try {
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            
            JSONObject object = new JSONObject(content);
            int userID = Integer.parseInt(object.getString("userid"));
            
            List<Listen> result = getUsersLists(userID, false, em);
            
            if( result.isEmpty())
            {
                JSONObject response = new JSONObject();
                return Response.ok(response.toString()).build();
            }
            
            for( Listen liste : result)
            {
                ArrayList<Listenpositionen> finalpositions = new ArrayList<>();
                
                // remove all deleted items
                for (Listenpositionen position : liste.getListenpositionenCollection())
                {
                    if(!position.getIstGeloescht())
                        finalpositions.add(position);
                }

                liste.setListenpositionenCollection(finalpositions);
            }
            
            /*Query posquery = em.createQuery("SELECT DISTINCT lp FROM Listenpositionen lp WHERE lp.liste.id = :id");
            
            for( Listen liste : result)
            {
            List<Listenpositionen> positionen = posquery.setParameter("id", liste.getId()).getResultList();
            liste.setListenpositionenCollection(positionen);
            }*/
            
            JSONObject resultObject = entities2json(result);
            System.out.println(resultObject.toString());
            
            return Response.ok(resultObject.toString()).build();
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    @Path("/getallweb")
    @POST
    @Consumes("application/json")
    @Produces("application/json") 
    public Response getLists(String content)
    {
        try {
            List<Listen> lists;
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            
            JSONObject object = new JSONObject(content);
            int userID = object.getInt("userid");
            
            lists = getUsersLists(userID, false, em);
            
            return Response.ok(lists).build();
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @Path("/getitems")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getItems(String content)
    {
        try {
            List<Listenpositionen> items;
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            
            JSONObject data = new JSONObject(content);
            int listid = data.getInt("listid");
            
            Query query = em.createNamedQuery("Listenpositionen.findByListenId").setParameter("id", listid);
            items = query.getResultList();
        
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN);

            for(Listenpositionen pos : items) {
                if(pos.getGekauftDatum() != null) {
                    Date date = pos.getGekauftDatum();
                    String dateString = df.format(date);
                    pos.setGekauftDatumString(dateString);
                }
                
                if(!pos.getGekauft())
                    pos.setGekauftDatum(null);
            }
            
            return Response.ok(items).build();
            
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @Path("/addlist")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addList(Listen list)
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        
        em.persist(list);
        
        transaction.commit();
        
        return Response.ok().build();
    } 
    
    @Path("/additem")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addItem(Listenpositionen pos)
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        
        em.persist(pos);
        
        transaction.commit();
        
        return Response.ok().build();
    }    
    
    
    /**
     * Bekommt vom Client ein JSON-Objekt mit allen seinen Listen (inkl Listenpositionen).
     * Verarbeitet diese Liste und aktualisiert die entsprechenden Tabellen in der Datenbank.
     * Die verarbeitete Liste hat das mobilclient-spezifische Format, ist somit nicht für die Webseite verwendbar.
     * 
     * @param content   JSON vom Client
     * @return HTTP-Status
     */
    @Path("/update")
    @POST
    @Consumes("application/json")
    public Response updateUsersLists(String content)
    {
        try {
            JSONObject data = new JSONObject(content);
            
            // 1. get current Listentitites
            // 2. update entities if possible
            // 3. add new entities
            // 4. persist
            
            // get user's current list entities
            int userid = data.getInt("userid");
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            List<Listen> list_entities = getUsersLists(userid, false, em);
            JSONArray lists = data.getJSONArray("lists");
            
            for( int i = 0; i < lists.length(); i++)
            {                
                JSONObject list = lists.getJSONObject(i);
                int listid = list.getInt("id");        
                boolean isNew = true;
                Listen list_entity = new Listen();                  
                
                // get old list, if possible
                if( listid >= 0)
                {
                    for( Listen curlist : list_entities)
                    {
                        if( curlist.getId() == listid)
                        {
                            list_entity = curlist;
                            list_entities.remove(curlist);
                            isNew = false;
                            break;
                        }
                    }   
                }
                
                list_entity.setName(list.getString("title"));
                
                if(isNew)
                    list_entity.setErsteller(new Benutzer(userid));
                
                int groupid = list.getInt("groupid");
                if (!isNew && list_entity.getGruppe() != null && groupid == -1) 
                {
                    list_entity.setGruppe(null);
                }
                else if((isNew && groupid >= 0) 
                        || (!isNew && groupid >= 0 && list_entity.getGruppe() == null)
                        || (!isNew && list_entity.getGruppe() != null && groupid != list_entity.getGruppe().getId())) 
                {
                    list_entity.setGruppe(new Gruppen(groupid));
                }
                
                JSONArray items = list.getJSONArray("items");
                Collection<Listenpositionen> item_entities = list_entity.getListenpositionenCollection();
                
                if( item_entities == null)
                    item_entities = new ArrayList<>();
                
                if(items.length() > 0)
                {
                    for( int j = 0; j < items.length(); j++)
                    {
                        JSONObject item = items.getJSONObject(j);                        
                        Listenpositionen item_entity = new Listenpositionen();
                        item_entity.setListe(list_entity);
                        int itemid = item.getInt("id");
                        
                        if(itemid >= 0) // old item
                        {
                            for( Listenpositionen curitem : item_entities)
                            {
                                if(curitem.getId() == itemid)
                                {
                                    item_entity = curitem;
                                    item_entities.remove(curitem);
                                    break;
                                }
                            }
                        }
                        
                        item_entity.setAnzahl(item.getInt("number"));
                        item_entity.setName(item.getString("text"));
                        item_entity.setGeschaeftsname(item.getString("store"));
                        item_entity.setGeschaeftsdetails(item.getString("storedetail"));
                        item_entity.setKommentar(item.getString("detail"));
                        
                        if(itemid < 0)
                            item_entity.setErstelltVon(new Benutzer(userid));
                        
                        // TODO: bei gekauft: true schauen, ob vorher schon gekauft. Wenn nicht: gekauftVon und Datum setzen.
                        boolean wasbought = false;
                        if(item_entity.getGekauft() && itemid >= 0)
                            wasbought = true;
                            
                        item_entity.setGekauft(item.getBoolean("checked"));
                        if(item_entity.getGekauft() && !wasbought)
                        {
                            item_entity.setGekauftVon(new Benutzer(userid));
                            Date date = new Date();
                            item_entity.setGekauftDatum(date);
                        }
                        else if(!item_entity.getGekauft() && wasbought)
                        {
                            item_entity.setGekauftVon(null);
                            item_entity.setGekauftDatum(null);
                        }
                        
                        item_entities.add(item_entity);
                    }
                }
                
                list_entity.setListenpositionenCollection(item_entities);
                list_entities.add(list_entity);
            }
            
            // persist
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            for( Listen list : list_entities)
                em.merge(list);
            transaction.commit();   
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine ID für eine Liste und löscht diese.
     * Die Liste wird nicht wirklich gelöscht, sondern der Wert istGeloescht auf true gesetzt. (eingeführt für Statistiken)
     * 
     * @param content JSON vom Client
     * @return HTTP-Status
     */
    @Path("/remove")
    @POST
    @Consumes("application/json")
    public Response removeList(String content)
    {
        try {
            JSONObject data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            
            int listid = data.getInt("listId");
            System.out.println("removing list with ID: " + listid);
            
            Listen list = (Listen)em.createNamedQuery("Listen.findById").setParameter("id", listid).getSingleResult();
            list.setIstGeloescht(true);
            System.out.println(list.toString());
            
            // persist
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(list);
            transaction.commit();   
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine ID für eine Listenposition und löscht diese.
     * 
     * @param content JSON vom Client
     * @return HTTP-Status
     */
    @Path("/removeitem")
    @POST
    @Consumes("application/json")
    public Response removeItem(String content)
    {
        try {
            JSONObject data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            
            int itemid = data.getInt("itemid");
            
            Listenpositionen pos = (Listenpositionen)em.createNamedQuery("Listenpositionen.findById").setParameter("id", itemid).getSingleResult();
            pos.setIstGeloescht(true);
            
            // persist
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(pos);
            transaction.commit();   
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
  
    /**
     * Bekommt vom Webclient eine Liste von Listen und persisitiert diese.
     * 
     * @param lists
     * @return 
     */
    @Path("/mergelists")
    @POST
    @Consumes("application/json")
    public Response mergeLists(List<Listen> lists)
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        for(Listen lis : lists){
            System.out.println(lis);
            em.merge(lis);
        }
       
        
        transaction.commit();
        
        return Response.ok().build();
    }
    
    /**
     * Bekommt vom Webclient eine Liste von Listenpositionen und persisitiert diese.
     * 
     * @param positions
     * @return 
     */
    @Path("/mergeitems")
    @POST
    @Consumes("application/json")
    public Response mergeItems(List<Listenpositionen> positions)
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        em.getEntityManagerFactory().getCache().evictAll();
        
        EntityTransaction transaction = em.getTransaction();
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN);
        
        for (Listenpositionen pos : positions) {
            if(pos.getGekauftDatumString() != null && !pos.getGekauftDatumString().equals("")) {
                String dateString = pos.getGekauftDatumString();
                try {
                    Date date = df.parse(dateString);
                    pos.setGekauftDatum(date);
                } catch (ParseException ex) {
                    Logger.getLogger(ListWebservice.class.getName()).log(Level.SEVERE, null, ex);
                    return Response.serverError().build();
                }
            }
        }
        
        transaction.begin();
        
        for( Listenpositionen pos : positions)
            em.merge(pos);
        
        transaction.commit();
        
        return Response.ok().build();
    }
    
    /**
     * Bekommt eine Userid und holt alle Listen des Nutzers (privat und Gruppen) aus der Datenbank.
     * Desweiteren kann festgelegt werden, ob gelöschte Listen enthalten sein sollen. Ein EntityManager muss übergeben werden.
     * 
     * @param userID ID des Users
     * @param includeDeleted Boolean, welches angibt, ob gelöschte Listen ebenfalls ausgegeben werden sollen. True, wenn das der Fall sein soll.
     * @param em EntityManager, um das Singleton Muster einzuhalten
     * @return Eine Liste von Listen-Entities zur weiteren Verarbeitung
     */
    public List<Listen> getUsersLists(int userID, boolean includeDeleted, EntityManager em)
    {        
        System.out.println("Getting all lists for user " + userID);
                
        List<Listen> userResult, groupResult;
        String query1 = "SELECT DISTINCT l FROM Benutzer b, Listen l WHERE b.id = :id AND b.id = l.ersteller.id";
        String query2 = "SELECT DISTINCT l FROM Benutzer b, Benutzergruppen bg, Gruppen g, Listen l WHERE b.id = :id AND b.id = bg.benutzerID.id AND bg.gruppenID.id = g.id AND l.gruppe.id = g.id";
        Query query = em.createQuery(query1);        
        userResult = query.setParameter("id", userID).getResultList();       
        
        query = em.createQuery(query2);
        groupResult = query.setParameter("id", userID).getResultList();
        for( Listen liste : groupResult)
        {
            if(!userResult.contains(liste))
                userResult.add(liste);
        }
        
        List<Listen> completeResult = new ArrayList<>();
        
        if(!includeDeleted)
        {
            // remove all deleted lists
            for( Listen liste : userResult)
            {
                if(!liste.getIstGeloescht())
                {
                    completeResult.add(liste);
                }
            }
        }
        else
            completeResult = userResult;
        
        // sort by name
        Collections.sort(completeResult);
        
        return completeResult;
    }
    
    /**
     * Hilfsklasse, welche eine Liste von Listen-Entities in das mobilclient-spezifische JSON-Format umwandelt.
     * 
     * @param listen
     * @return JSONObject mit dem spezifischen Format
     * @throws JSONException 
     */
    private JSONObject entities2json(List<Listen> listen) throws JSONException
    {
        JSONObject returnObject = new JSONObject();
        JSONArray json_listen = new JSONArray();
        JSONObject json_liste;
        JSONArray json_listenpositionen;
        JSONObject json_listenposition;
        
        for( Listen liste : listen)
        {
            json_liste = new JSONObject();
            json_liste.put("id", liste.getId());
            json_liste.put("title", liste.getName());
            
            if(liste.getGruppe() != null)
            {
                json_liste.put("group", liste.getGruppe().getName());
                json_liste.put("groupid", liste.getGruppe().getId());
            }
            else
            {
                json_liste.put("group", "");
                json_liste.put("groupid", -1);
            }
            
            json_listenpositionen = new JSONArray();
            
            for( Listenpositionen position : liste.getListenpositionenCollection())
            {
                String store = position.getGeschaeftsname();
                String storedetail = position.getGeschaeftsdetails();
                String detail = position.getKommentar();
                
                // the following if statements are used to be able to put empty elements in the JSONObject. With null value, it would be removed.
                
                if(store == null)
                    store = "";
                
                if(storedetail == null)
                    storedetail = "";
                
                if(detail == null)
                    detail = "";
                
                json_listenposition = new JSONObject();
                json_listenposition.put("id", position.getId());
                json_listenposition.put("text", position.getName());
                json_listenposition.put("checked", position.getGekauft());
                json_listenposition.put("store", store);
                json_listenposition.put("storedetail", storedetail);
                json_listenposition.put("number", position.getAnzahl());
                json_listenposition.put("detail", detail);
                    
                
                json_listenpositionen.put(json_listenposition);
            }
            
            json_liste.put("items", json_listenpositionen);
            
            json_listen.put(json_liste);
        }
        
        return returnObject.put("lists", json_listen);
    }
    
}
