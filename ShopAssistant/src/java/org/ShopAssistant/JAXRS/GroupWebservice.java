/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JAXRS;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Benutzergruppen;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.webservice.PersistenceWrapper;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
@Path("data/groups")
public class GroupWebservice {
    /**
     * Bekommt vom Client eine Userid und liefert einen String mit allen Gruppen (Gruppenid, Name und ihre Mitglieder (nur Username)) zurück.
     * 
     * @param content JSON mit Userid
     * @return  JSON-String mit allen Gruppen 
     */
    @Path("/getall")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getUsersGroups(String content)
    {
        try {
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            JSONArray json_groups = new JSONArray();
            JSONObject json_group;
            JSONArray json_members;
            
            JSONObject data = new JSONObject(content);
            int userID = Integer.parseInt(data.getString("userid"));
            
            System.out.println("Getting all groups for user " + userID);
            
            String groupQuery   = "SELECT DISTINCT g FROM Benutzer b, Benutzergruppen bg, Gruppen g WHERE b.id = bg.benutzerID.id AND g.id = bg.gruppenID.id AND b.id = :id";
            String memberQuery  = "SELECT DISTINCT b FROM Benutzer b, Benutzergruppen bg, Gruppen g WHERE b.id = bg.benutzerID.id AND g.id = bg.gruppenID.id AND g.id = :id";
            
            List<Gruppen> result;
            Query query = em.createQuery(groupQuery);
            result = query.setParameter("id", userID).getResultList();
                       
            String isMobileString;
            boolean isMobile = false;
            isMobileString = data.getString("isMobile");
            if (isMobileString.equals("true"))
                isMobile = true;
            
            if(!isMobile) {
                return Response.ok(result).build();
            }
            
            query = em.createQuery(memberQuery);
            for( Gruppen group : result)
            {
                json_group = new JSONObject();
                json_members = new JSONArray();
                
                json_group.put("id", group.getId());
                json_group.put("name", group.getName());
                
                List<Benutzer> memberResult = query.setParameter("id", group.getId()).getResultList();
                for( Benutzer user : memberResult)
                {
                    json_members.put(user.getName());
                }
                
                json_group.put("members", json_members);
                json_groups.put(json_group);
            }
            
            JSONObject resultObject = new JSONObject().put("groups", json_groups);
            System.out.println(resultObject.toString());
            
            return Response.ok(resultObject.toString()).build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    @Path("/findbyname")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response findByName(String name)
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        Query query = em.createNamedQuery("Gruppen.findByName").setParameter("name", name);
        
        Gruppen foundGroup = (Gruppen) query.getSingleResult();
        
        return Response.ok(foundGroup).build();
    }
    
    /**
     * Bekommt vom Client eine GruppenId und den neue Namen der Gruppe und benennt die entsprechende Gruppe um.
     * 
     * @param content JSON mit GruppenId und neuem Namen
     * @return Response mit Status 200 oder 500
     */
    @Path("/rename")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response renameGroup(String content)
    {
        try {
            JSONObject data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            int groupid = data.getInt("groupid");
            String newname = data.getString("newname");
            
            Query query = em.createNamedQuery("Gruppen.findById");
            Gruppen group = (Gruppen)query.setParameter("id", groupid).getSingleResult();
            
            group.setName(newname);
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(group);
            transaction.commit();
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine GruppenId und einen Usernamen und fügt den entsprechenden User der Gruppe hinzu.
     * Ist der Username nicht zuzuordnen oder ist der enstprechende User bereits Mitglied der Gruppe, wird der JSON-Antwort 
     * ein Boolean isError und eine passende Fehlermeldung hinzugefügt, die im Client angezeigt werden kann.
     * 
     * @param content JSON mit GruppenId und Username
     * @return Response mit Status 200 oder 500. Bei 200 kann eine Fehlermeldung in JSON enthalten sein.
     */
    @Path("/addmember")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addMember(String content)
    {
        final String ERRMSG_ALREADY_MEMBER = "Der Benutzer ist bereits Mitglied.";
        final String ERRMSG_USER_NOT_FOUND = "Der Benutzername wurde nicht gefunden.";
        
        try {           
            JSONObject data, response;
            data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            int groupid = data.getInt("groupid");
            String username = data.getString("username");
            
            Query query = em.createNamedQuery("Gruppen.findById");
            Gruppen group = (Gruppen)query.setParameter("id", groupid).getSingleResult();
            
            ArrayList<Benutzergruppen> members = new ArrayList(group.getBenutzergruppenCollection());
            
            // check if user is already a member
            for (Benutzergruppen member : members)
            {
                if( member.getBenutzerID().getName().equals(username))
                {
                    response = new JSONObject();
                    response.put("isError", true);
                    response.put("errorMessage", ERRMSG_ALREADY_MEMBER);
                    return Response.ok(response.toString()).build();
                }
            }
            
            // find user for username
            Query userquery = em.createNamedQuery("Benutzer.findByName");
            List<Benutzer> users = userquery.setParameter("name", username).getResultList(); // would like to use getSingleResult(), but that gives a NoResultException instead of null...
            
            // error if no user found
            if( users.size() != 1)
            {
                response = new JSONObject();
                response.put("isError", true);
                response.put("errorMessage", ERRMSG_USER_NOT_FOUND);
                return Response.ok(response.toString()).build();
            }
            
            Benutzergruppen newmember = new Benutzergruppen();
            newmember.setBenutzerID(users.get(0));
            newmember.setGruppenID(group);
                    
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.persist(newmember);
            transaction.commit();
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine GruppenId und eine Userid und entfernt den entsprechenden User aus der Gruppe.
     * 
     * @param content JSON mit GruppenId und Username
     * @return Response mit Status 200 oder 500.
     */
    @Path("/leave")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response leaveGroup(String content)
    {        
        try {           
            JSONObject data;
            data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            int groupid = data.getInt("groupid");
            int userid = data.getInt("userid");
            
            Query query = em.createNamedQuery("Benutzergruppen.findByGruppenId");            
            List<Benutzergruppen> usergroups = query.setParameter("id", groupid).getResultList();
            
            Benutzergruppen toRemoveGroup = new Benutzergruppen();
            // find the usergroup that needs to be deleted
            for (Benutzergruppen usergroup : usergroups)
            {
                if( usergroup.getBenutzerID().getId() == userid)
                {
                    toRemoveGroup = usergroup;
                    break;
                }
            }
                    
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.remove(toRemoveGroup);
            transaction.commit();
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine Userid und einen Gruppennamen und erstellt eine entsprechende Gruppe.
     * Der User wird ebenfalls der Gruppe hinzugefügt.
     * 
     * @param content JSON mit GruppenId und Username
     * @return Response mit Status 200 oder 500.
     */
    @Path("/add")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addGroup(String content)
    {        
        try {           
            JSONObject data;
            data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            String groupname = data.getString("groupname");
            int userid = data.getInt("userid");
            
            Gruppen newgroup = new Gruppen();
            newgroup.setName(groupname);         
            newgroup.setGruppenErsteller(new Benutzer(userid));
            
            
            Benutzergruppen newusergroup = new Benutzergruppen();
            newusergroup.setBenutzerID(new Benutzer(userid));
            newusergroup.setGruppenID(newgroup);
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.persist(newgroup);
            em.persist(newusergroup);
            transaction.commit();
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt vom Client eine GruppenId und eine Userid und löscht die referenzierte Gruppe.
     * Nur der Ersteller einer Gruppe kann diese auch löschen. Diese Funktion wird momentan nicht im Client angesprochen.
     * 
     * @param content JSON mit GruppenId und Username
     * @return Response mit Status 200 oder 500.
     */
    @Path("/remove")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response removeGroup(String content)
    {        
        try {           
            JSONObject data;
            data = new JSONObject(content);
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            int groupid = data.getInt("groupid");
            int userid = data.getInt("userid");
            
            Query query = em.createNamedQuery("Gruppen.findById").setParameter("id", groupid);
            Gruppen group = (Gruppen) query.getSingleResult();
            
            // nur der Ersteller einer Gruppe kann diese auch löschen
            if( group.getGruppenErsteller().getId() != userid)
                return Response.status(Response.Status.UNAUTHORIZED).build();
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.remove(group);
            transaction.commit();
            
            return Response.ok().build();
        } catch (JSONException ex) {
            Logger.getLogger(GroupWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}
