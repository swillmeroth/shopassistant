/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JAXRS;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Entities.Listen;
import org.ShopAssistant.Entities.Listenpositionen;
import org.ShopAssistant.webservice.PersistenceWrapper;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
@Path("data/profile")
public class ProfileWebservice {
    
    /**
     * Bekommt eine UserID und liefert ein JSON-Objekt mit Statistiken zurück.
     * Es handelt sich jeweils um Objekte mit dem Aufbau "text":"$STATISTIK"
     * 
     * @param content
     * @return Status 200 (mit JSONObject mit Statistiken) oder 500
     */
    @Path("/getdetails")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getDetails(String content)
    {
        try {
            JSONObject data, response;
            JSONArray posts;
            int userid;
            
            data = new JSONObject(content);
            userid = data.getInt("userid");
            
            // email
            response = new JSONObject();
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findById").setParameter("id", userid);
            Benutzer user = (Benutzer)query.getSingleResult();
            response.put("email", user.getEMail());
            
            // statistics (statistics are a dynamically built array, so it's easy to add new ones)
            posts = new JSONArray();
            posts.put(getActiveListCount(userid, em));
            
            JSONObject completeListCount = getCompleteListCount(userid, em);
            if(completeListCount != null)
                posts.put(completeListCount);
            
            posts.put(getGroupCount(userid, em));
            posts.put(getActiveItemsCount(userid, em));
            posts.put(getAddedItemsCount(userid, em));
            posts.put(getBoughtItemsCount(userid, em));
            response.put("posts", posts);
            
            return Response.ok(response.toString()).build();
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt eine UserID und einen neuen Namen, und ändert den entsprechenden Nutzernamen.
     * 
     * @param content
     * @return Status 200 oder 500
     */
    @Path("/changename")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response changeName(String content)
    {
        try {
            JSONObject data;
            int userid;
            String newname;
            
            data = new JSONObject(content);
            userid = data.getInt("userid");
            newname = data.getString("newname");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findById").setParameter("id", userid);
            Benutzer user = (Benutzer) query.getSingleResult();
            
            user.setName(newname);
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(user);
            transaction.commit();
            
            return Response.ok().build();
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt eine UserID und eine E-Mail-Adresse und speichert diese Adresse für den Nutzer ab.
     * 
     * @param content
     * @return Status 200 oder 500
     */
    @Path("/changemail")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response changeMail(String content)
    {
        try {
            JSONObject data;
            int userid;
            String newmail;
            
            data = new JSONObject(content);
            userid = data.getInt("userid");
            newmail = data.getString("email");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findById").setParameter("id", userid);
            Benutzer user = (Benutzer) query.getSingleResult();
            
            user.setEMail(newmail);
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(user);
            transaction.commit();
            
            return Response.ok().build();
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt eine UserID und ein Passwort und speichert dieses als neues Passwort für den Nutzer ab.
     * 
     * @param content
     * @return Status 200 oder 500
     */
    @Path("/changepassword")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response changePassword(String content)
    {
        try {
            JSONObject data;
            int userid;
            String newpassword;
            
            data = new JSONObject(content);
            userid = data.getInt("userid");
            newpassword = data.getString("password");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findById").setParameter("id", userid);
            Benutzer user = (Benutzer) query.getSingleResult();
            
            user.setPasswort(newpassword);
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(user);
            transaction.commit();
            
            return Response.ok().build();
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt Mail, Passwort uns UserId und merged den entsprechenden DB-Eintrag.
     * 
     * @param content
     * @return Status 200 oder 500
     */
    @Path("/changeuser")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response changeUser(String content)
    {
        try {
            JSONObject data;
            int userid;
            String mail, password;
            Benutzer user;
            
            data = new JSONObject(content);
            userid = data.getInt("userid");
            mail = data.getString("mail");
            password = data.getString("password");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findById").setParameter("id", userid);
            user = (Benutzer) query.getSingleResult();
            
            user.setEMail(mail);
            user.setPasswort(password);
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(user);
            transaction.commit();
            
            return Response.ok().build();
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    @Path("/getbymail")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getUserByMail(String mail)
    {
        try {
            JSONObject cred = new JSONObject(mail);
            String usermail = cred.getString("mail");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findByEMail").setParameter("eMail", usermail);
            
            Benutzer user = (Benutzer) query.getSingleResult();
            
            return Response.ok(user).build();
            
        } catch (JSONException | javax.persistence.NoResultException ex) {
            Logger.getLogger(ProfileWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.ok(null).build();
        }
    }
    
    @Path("/remove")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response removeUser(String content)
    {
        try {
            
            JSONObject object = new JSONObject(content);
            int userid = object.getInt("userid");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.remove(new Benutzer(userid));
            transaction.commit();
            
            return Response.ok().build();
            
        } catch (JSONException ex) {
            Logger.getLogger(ProfileWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    
    
    //-------------------------------------------------------------//
    //                   STATISTIK-METHODEN                        //
    //-------------------------------------------------------------//
    
    /**
     * Private Statistikmethode für die Anzahl aktiver (nicht gelöschter) Listen.
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getActiveListCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        ListWebservice listservice = new ListWebservice();
        
        int count = listservice.getUsersLists(userid, false, em).size();
        post.put("text", "Aktive Listen: " + count);
        
        return post;
    }
    
    /**
     * Private Statistikmethode für die Anzahl Listen (aktiv und gelöscht).
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getCompleteListCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        ListWebservice listservice = new ListWebservice();
        
        int activecount = listservice.getUsersLists(userid, false, em).size();
        int completecount = listservice.getUsersLists(userid, true, em).size();
        
        if(completecount == activecount)
            return null;
        
        post.put("text", "Anzahl Listen: " + completecount);
        return post;
    }
    
    /**
     * Private Statistikmethode für die Anzahl Gruppen.
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getGroupCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        
        String groupQuery   = "SELECT DISTINCT g FROM Benutzer b, Benutzergruppen bg, Gruppen g WHERE b.id = bg.benutzerID.id AND g.id = bg.gruppenID.id AND b.id = :id";
            
        List<Gruppen> result;
        Query query = em.createQuery(groupQuery);
        result = query.setParameter("id", userid).getResultList();
        
        int count = result.size();
        post.put("text", "Anzahl Gruppen: " + count);
        
        return post;
    }
    
    /**
     * Private Statistikmethode für die Anzahl durch den Nutzer gekaufter Elemente.
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getBoughtItemsCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        
        Query query = em.createNamedQuery("Listenpositionen.findByGekauftVon").setParameter("id", userid);       
        int count = query.getResultList().size();
        post.put("text", "Gekaufte Artikel: " + count);
        
        return post;
    }
    
    /**
     * Private Statistikmethode für die Anzahl zu kaufender Elemente inden Listen eines Nutzers.
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getActiveItemsCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        ListWebservice listservice = new ListWebservice();
        int count = 0;
        
        List<Listen> lists = listservice.getUsersLists(userid, false, em);
        
        for (Listen list : lists)
        {
            Collection<Listenpositionen> listenpositionenCollection = list.getListenpositionenCollection();
            for( Listenpositionen listpos : listenpositionenCollection)
            {
                if(!listpos.getGekauft() && !listpos.getIstGeloescht())
                    count++;
            }
        }
           
        post.put("text", "Zu kaufende Artikel: " + count);
        
        return post;
    }
    
    /**
     * Private Statistikmethode für die Anzahl durch den Nutzer hinzugefügter Elemente.
     * Aufgerufen von getDetails.
     * 
     * @param userid ID des Users
     * @param em EntityManager
     * @return JSONObject mit anzeigbarem Text
     * @throws JSONException 
     */
    private JSONObject getAddedItemsCount(int userid, EntityManager em) throws JSONException
    {
        JSONObject post = new JSONObject();
        
        Query query = em.createNamedQuery("Listenpositionen.findByErstelltVon").setParameter("id", userid);       
        int count = query.getResultList().size();
        post.put("text", "Hinzugefügte Artikel: " + count);
        
        return post;
    }
}
