/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JAXRS;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.LogicClasses.MailSender;
import org.ShopAssistant.LogicClasses.PasswordGenerator;
import org.ShopAssistant.LogicClasses.PasswordGenerator.PasswordMode;
import org.ShopAssistant.webservice.ClientAuthenticator;
import org.ShopAssistant.webservice.PersistenceWrapper;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
@Path("auth")
public class AuthWebservice {
    
    private static final PasswordMode PASSWORD_MODE = PasswordMode.SPECIAL;
    private static final int PASSWORD_LENGTH = 12;
  
    /**
     * Login-Methode, die einen User mit name und Passwort einloggt.
     * Private Methode, die aus login und register heraus aufgerufen wird, nicht direkt vom Client.
     * 
     * @param username
     * @param password
     * @return Status 200 oder 401
     * @throws JSONException 
     */
    private Response doLogin(String username, String password, boolean isMobile) throws JSONException
    {
        EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
        
        // get all users with that name
        Query query = em.createNamedQuery("Benutzer.findByName").setParameter("name", username);
        System.out.println("Query: " + query.toString());
        List<Benutzer> results = query.getResultList();
        
        if(results.isEmpty())
        {
            query = em.createNamedQuery("Benutzer.findByEMail").setParameter("eMail", username);
            System.out.println("Query: " + query.toString());
            results = query.getResultList();
            
            if(results.isEmpty())
                return Response.status(Response.Status.UNAUTHORIZED).build();
        }
            
        for( Benutzer user : results)
        {
            if(user.getPasswort().equals(password))
            {
                if(isMobile)
                {
                    ClientAuthenticator authenticator = ClientAuthenticator.getInstance();
                    UUID uuid = UUID.randomUUID();
                    authenticator.storeAuthToken(username, uuid.toString());

                    JSONObject response = new JSONObject();
                    response.put("authToken", uuid.toString());
                    response.put("userid", user.getId());
                    response.put("username", user.getName());
                    response.put("email", user.getEMail());

                    return Response.ok(response.toString()).build();
                }
                else
                {
                    return Response.ok(user).build();
                }
            }
        }
        
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
    
    /**
     * Bekommt Username oder Email und Passwort als JSON-Objekt und loggt den entsprechenden User ein.
     * Ruft intern doLogin auf.  
     * 
     * @param credentials JSON mit Username und Passwort
     * @return Rückgabe von doLogin oder Status 500.
     */
    @Path("/login")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response login(String credentials)
    {
        try {
            String username, password;
            
            System.out.println("Got credentials: " + credentials);
            
            JSONObject data = new JSONObject(credentials);
            username = data.getString("username");
            password = data.getString("password");
            
            String isMobileString;
            boolean isMobile = false;
            isMobileString = data.getString("isMobile");
            if (isMobileString.equals("true"))
                isMobile = true;
            
            return doLogin(username, password, isMobile);
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt Userinformationen als JSON und registriert einen neuen User.
     * 
     * @param credentials JSON mit Username, Email und Passwort
     * @return Status 200 oder 500. 200 kann Fehler enthalten (Name oder EMail schon existent), die als JSON angehängt werden.
     */
    @Path("/register")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response register(String credentials)
    {
        try {
            final String ERRMSG_USER_EXISTS = "Der Username ist bereits vergeben.";
            final String ERRMSG_EMAIL_EXISTS = "Diese E-Mail ist bereits registriert.";
            
            JSONObject data, response;
            String username;
            String email;
            String password;
            
            data = new JSONObject(credentials);
            username = data.getString("username");
            email = data.getString("email");
            password = data.getString("password1");
            
            String isMobileString;
            boolean isMobile = false;
            isMobileString = data.getString("isMobile");
            if (isMobileString.equals("true"))
                isMobile = true;
            
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query1 = em.createNamedQuery("Benutzer.findByName").setParameter("name", username);
            List<Benutzer> results1 = query1.getResultList();
            
            if(results1.size() > 0)
            {
                response = new JSONObject();
                response.put("isError", true);
                response.put("errorMessage", ERRMSG_USER_EXISTS);
                if(isMobile)
                    return Response.ok(response.toString()).build();
                else
                    return Response.status(Response.Status.BAD_REQUEST).build();
                
            }   
            
            Query query2 = em.createNamedQuery("Benutzer.findByEMail").setParameter("eMail", email);
            List<Benutzer> results2 = query2.getResultList();
            if(results2.size() > 0)
            {
                System.out.println("EMAILERROR");
                response = new JSONObject();
                response.put("isError", true);
                response.put("errorMessage", ERRMSG_EMAIL_EXISTS);
                if(isMobile)
                    return Response.ok(response.toString()).build();
                else
                    return Response.status(Response.Status.BAD_REQUEST).build();
            }   
            
            Benutzer user = new Benutzer();
            user.setName(username);
            user.setEMail(email);
            user.setPasswort(password);
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.persist(user);
            transaction.commit();
            
            // send welcome email
            MailSender sender = MailSender.getInstance();
            sender.sendWelcomeMail(user);
            
            return doLogin(username, password, isMobile);
            
        } catch (JSONException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    
    /**
     * Bekommt einen Usernamen, generiert für den entsprechenden Nutzer ein neues Passwort und sendet es an die hinterlegte E-Mail-Adresse.
     * Das generierte Passwort ist 12 Zeichen lang und enthält mindestens ein Sonderzeichen.
     * 
     * @param credentials JSON mit Usernamen
     * @return Status 200 oder 500
     */
    @Path("/forgotpass")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response forgotPassword(String credentials)
    {
        try {
            String username;
            JSONObject data = new JSONObject(credentials);
            username = data.getString("username");
            
            EntityManager em = PersistenceWrapper.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Benutzer.findByName").setParameter("name", username);
            Benutzer user = (Benutzer) query.getSingleResult();
            
            String newPassword = PasswordGenerator.generateRandomPassword(PASSWORD_LENGTH, PASSWORD_MODE);
            
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            user.setPasswort(newPassword);
            em.merge(user);
            transaction.commit();
            
            // TODO: send email with new password
            MailSender sender = MailSender.getInstance();
            sender.sendPasswordMail(user);
            
            return Response.ok().build();
        } catch (JSONException |javax.persistence.NoResultException ex) {
            Logger.getLogger(AuthWebservice.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}
