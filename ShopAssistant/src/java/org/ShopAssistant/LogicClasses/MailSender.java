/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.LogicClasses;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.ShopAssistant.Entities.Benutzer;

/**
 * Klasse zum Versenden einer Mail
 * @author Stephan kommentiert durch Andre Hantke
 */
public final class MailSender {
    private static MailSender mailsender = null;
    
    private final String USERNAME   = "shopassistantmail";
    private final String PASSWORD   = "sH0p@ss!staNT";
    private final String MAIL_FROM  = "shopassistantmail@gmail.com";
    
    private final Authenticator authenticator;
    private Properties properties = new Properties();
    
    /**
     * Konstruktor
     */
    private MailSender() {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        
        authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD); 
            }
        };
    }
    
    /**
     * Liefert die Instanz dieser Klasse
     * @return 
     */
    public static synchronized MailSender getInstance()
    {
        if( mailsender == null)
            mailsender = new MailSender();
        
        return mailsender;
    }
    
    /**
     * Sendet eine Email mit dem angegebenen Betreff und Text an die angegebene Adresse.
     * @param recipient E-Mail-Adresse des Empfängers
     * @param subject Betreff der E-Mail
     * @param text Inhalt der Mail
     */
    public void sendGenericMail(String text, String subject, String recipient)
    {
        try {
            Session session = Session.getDefaultInstance(properties, authenticator);
            
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(MAIL_FROM));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(MailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Sendet eine Email an den Nutzer mit seinem Passwort.
     * @param user Entityklasse des Benutzers
     */
    public void sendPasswordMail(Benutzer user)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Hallo ");
        builder.append(user.getName());
        builder.append(",");
        builder.append("\n\nIhr neues Passwort lautet: ");
        builder.append(user.getPasswort());
        builder.append("\n\n");
        builder.append("Wir wünschen weiterhin viel Spaß mit dem ShopAssistant!");
        builder.append("\n\n");
        builder.append("Ihr ShopAssistant-Team");

        String subject = "Ihr Passwort für den ShopAssistant";
        String text = builder.toString();

        sendGenericMail(text, subject, user.getEMail());
    }
    
    /**
     * Sendet eine Willkommensmail an einen Benutzer. 
     * Diese Methode sollte nach der Registrierung aufgerufen werden und
     * enthält neben einer Begrüßung die Zugangsdaten des Nutzers.
     * @param user Entityklasse des Benutzers
     */
    public void sendWelcomeMail(Benutzer user)
    {
        String subject = "Willkommen beim ShopAssistant";
        
        StringBuilder builder = new StringBuilder();
        builder.append("Herzlichen Willkommen ");
        builder.append(user.getName());
        builder.append(",\n\n");
        builder.append("Ihre Zugangsdaten sind: ");
        builder.append("\n\n");
        builder.append("E-Mail: ");
        builder.append(user.getEMail());
        builder.append("\n");
        builder.append("Passwort: ");
        builder.append(user.getPasswort());
        builder.append("\n\n");
        builder.append("Wir wünschen viel Spaß mit dem ShopAssistant!");
        builder.append("\n\n");
        builder.append("Ihr ShopAssistant-Team");
        String text = builder.toString();
        
        sendGenericMail(text, subject, user.getEMail());
    }
}
