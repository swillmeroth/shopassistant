/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.LogicClasses;

import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Stephan
 */
public class NonMobileJSONObject extends JSONObject {

    public NonMobileJSONObject() {
        super();
        
        try {
            this.put("isMobile", "false");
        } catch (JSONException ex) {
            // do nothing, should never get here in the first place
        }
    }
}
