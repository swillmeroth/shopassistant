/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Jersey;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;

/**
 * Klasse des Jersey Clients im Hinblick auf die Methoden im Webservice ListWebservice.java
 * JAX-RS
 * @author Andre
 */
public class ListWebservice_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/ShopAssistant/webresources";

        /**
         * Kosntruktor 
         * Definiert den Pfad zum Webservice
         */
        public ListWebservice_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.READ_TIMEOUT, 0);
            webTarget = client.target(BASE_URI).path("data/lists");
        }

        /**
         * Liefert Listen
         * @param requestEntity User ID
         * @return Listen
         * @throws ClientErrorException 
         */
        public Response getLists(Object requestEntity) throws ClientErrorException {
            return webTarget.path("getallweb").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Liefert Listenpositionen
         * @param requestEntity ID der Liste
         * @return Listenpositionen
         * @throws ClientErrorException 
         */
        public Response getItems(Object requestEntity) throws ClientErrorException {
            return webTarget.path("getitems").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Hinzufügen einer Liste
         * @param requestEntity Listen Entity
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response addList(Object requestEntity) throws ClientErrorException {
            return webTarget.path("addlist").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Hinzufügen eines Eintrages (Listenposition)
         * @param requestEntity Listenposition
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response addItem(Object requestEntity) throws ClientErrorException {
            return webTarget.path("additem").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }
        
        /**
         * Entfernen einer Liste
         * @param requestEntity ID der Liste
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response removeList(Object requestEntity) throws ClientErrorException {
            return webTarget.path("remove").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Entfernen eines Eintrages (Listenposition
         * @param requestEntity ID des Eintrags (Listenposition)
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response removeItem(Object requestEntity) throws ClientErrorException {
            return webTarget.path("removeitem").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Die zu persitierenden Listen.
         * @param requestEntity Liste von Listen
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response mergeLists(Object requestEntity) throws ClientErrorException {
            return webTarget.path("mergelists").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }
        
        /**
         * Die zu persistierenden Listenpositionen.
         * @param requestEntity Liste von Listenpositionen
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response mergeItems(Object requestEntity) throws ClientErrorException {
            return webTarget.path("mergeitems").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Client schließen.
         */
        public void close() {
            client.close();
        }
}
    

