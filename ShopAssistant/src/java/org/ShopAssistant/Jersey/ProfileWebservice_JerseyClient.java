/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Jersey;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;

/**
 * Klasse des Jersey Clients im Hinblick auf die Methoden im Webservice ProfileWebservice.java
 * JAX-RS
 * @author Andre
 */
public class ProfileWebservice_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/ShopAssistant/webresources";

        /**
         * Konstruktor 
         * Definiert den Pfad zum Webservice
         */
        public ProfileWebservice_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.READ_TIMEOUT, 0);
            webTarget = client.target(BASE_URI).path("data/profile");
        }

        /**
         * Ändern des Namens
         * @param requestEntity User ID und neuer Name
         * @return HTTP OK oder Server Error
         * @throws ClientErrorException 
         */
        public Response changeName(Object requestEntity) throws ClientErrorException {
            return webTarget.path("changename").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Passwort wechseln.
         * @param requestEntity User ID und Passwort
         * @return HTTP OK oder Server Error
         * @throws ClientErrorException 
         */
        public Response changePassword(Object requestEntity) throws ClientErrorException {
            return webTarget.path("changepassword").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Mailadresse wechseln.
         * @param requestEntity User ID und Mailadressen
         * @return HTTP OK oder Server Error
         * @throws ClientErrorException 
         */
        public Response changeMail(Object requestEntity) throws ClientErrorException {
            return webTarget.path("changemail").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Liefert zu einer Mailadresse den Nutzer
         * @param requestEntity Mailadresse
         * @return Benutzer
         * @throws ClientErrorException 
         */
        public Response getUserByMail(Object requestEntity) throws ClientErrorException {
            return webTarget.path("getbymail").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Entfernen eines Nutzers
         * @param requestEntity ID des Nutzers
         * @return HTTP OK oder Server Error
         * @throws ClientErrorException 
         */
        public Response removeUser(Object requestEntity) throws ClientErrorException {
            return webTarget.path("remove").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Client schließen
         */
        public void close() {
            client.close();
        }
}
    

