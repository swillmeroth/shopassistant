package org.ShopAssistant.Jersey;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;

/**
 * Klasse des Jersey Clients im Hinblick auf die Methoden im Webservice GroupWebservice.java
 * JAX-RS
 * @author Andre
 */
public class GroupWebservice_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/ShopAssistant/webresources";

        /**
         * Konstruktor
         * Definiert den Pfad zum Webservice
         */
        public GroupWebservice_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.READ_TIMEOUT, 0);
            webTarget = client.target(BASE_URI).path("data/groups");
        }
        
        /**
         * Ruft den Webservice auf zum Erstellen einer Gruppe
         * @param requestEntity Benutzer ID und Gruppenname
         * @return HTTP Ok
         * @throws ClientErrorException 
         */
        public Response addGroup(Object requestEntity) throws ClientErrorException {
            return webTarget.path("add").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Ruft den Webservice auf zum Hinzufügen eines Member zu einer Gruppe
         * @param requestEntity Gruppen ID und Nutzername
         * @return HTTP OK oder Server Error
         * @throws ClientErrorException 
         */
        public Response addMember(Object requestEntity) throws ClientErrorException {
            return webTarget.path("addmember").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Liefert Gruppe zu einem Namen
         * @param requestEntity Gruppenname
         * @return gefundene Gruppe
         * @throws ClientErrorException 
         */
        public Response findByName(Object requestEntity) throws ClientErrorException {
            return webTarget.path("findbyname").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * ruft den Webservice auf, zum Löschen einer Gruppe
         * @param requestEntity Gruppen ID und User ID
         * @return HTTP Unauthorized oder OK
         * @throws ClientErrorException 
         */
        public Response removeGroup(Object requestEntity) throws ClientErrorException {
            return webTarget.path("remove").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        //
        /**
         * Ruft den Webservice auf zum Verlassen einer Gruppe
         * @param requestEntity GruppenID und NutzerID
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response leaveGroup(Object requestEntity) throws ClientErrorException {
            return webTarget.path("leave").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Ruft den Webservice um an die USerGruppen des aktuellen Nutzers zu kommen
         * @param requestEntity Nutzer ID
         * @return Gruppen zu einem Nutzer
         * @throws ClientErrorException 
         */
        public Response getUsersGroups(Object requestEntity) throws ClientErrorException {
            return webTarget.path("getall").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }
        
        /**
         * Client schließen.
         */
        public void close() {
            client.close();
        }

}