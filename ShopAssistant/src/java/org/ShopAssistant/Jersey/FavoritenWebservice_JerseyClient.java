/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Jersey;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;

/**
 * Klasse des Jersey Clients im Hinblick auf die Methode im Webservice FavoritenWebservice.java
 * JAX-RS
 * @author Andre
 */
public class FavoritenWebservice_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/ShopAssistant/webresources";

        /**
         * Konstruktor
         * Definiert den Pfad zum Webservice
         */
        public FavoritenWebservice_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.READ_TIMEOUT, 0);
            webTarget = client.target(BASE_URI).path("data/favorites");
        }
        
        /**
         * Liefert Favoriten
         * @param requestEntity Benutzer
         * @return Favoriten 
         * @throws ClientErrorException 
         */
        public Response getUsersFavorites(Object requestEntity) throws ClientErrorException {
            return webTarget.path("getall").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Client schließen.
         */
        public void close() {
            client.close();
        }
}
    

