/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Jersey;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.glassfish.jersey.client.ClientProperties;

/**
 * Klasse des JErsey Clients im Hinblick auf die Methode im Webservice AuthWebservice.java
 * JAX-RS
 * @author Andre
 */
public class AuthWebservice_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/ShopAssistant/webresources";

        /**
         * Konstruktor definiert den Pfad zum Webservice
         */
        public AuthWebservice_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.READ_TIMEOUT, 0);
            webTarget = client.target(BASE_URI).path("auth");
        }

        /**
         * Registrierung eines Nutzers
         * @param requestEntity Benutzer der registriert werden soll
         * @return HTTP Antwort OK oder BAD_REQUEST
         * @throws ClientErrorException 
         */
        public Response register(Object requestEntity) throws ClientErrorException {
            return webTarget.path("register").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Passwort anfordern
         * @param requestEntity Name eines Users in JSON Objekt
         * @return HTTP OK
         * @throws ClientErrorException 
         */
        public Response forgotPassword(Object requestEntity) throws ClientErrorException {
            return webTarget.path("forgotpass").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Login Methode
         * @param requestEntity Benutzer
         * @return Benutzer oder HTTP Unauthorized
         * @throws ClientErrorException 
         */
        public Response login(Object requestEntity) throws ClientErrorException {
            return webTarget.path("login").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
        }

        /**
         * Client schließen.
         */
        public void close() {
            client.close();
        }
}
    

