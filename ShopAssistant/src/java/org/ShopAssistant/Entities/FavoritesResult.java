/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Stephan
 */
@Entity
@SqlResultSetMapping(
    name="favoritesMap",
    entities={
        @EntityResult(
            entityClass=FavoritesResult.class,
            fields={
                @FieldResult(name="id", column="id"),
                @FieldResult(name="name", column="name"),
                @FieldResult(name="kommentar", column="kommentar"),
                @FieldResult(name="geschaeftsname", column="geschaeftsname"),
                @FieldResult(name="geschaeftsdetails", column="geschaeftsdetails"),
                @FieldResult(name="anzahl", column="anzahl")
            } 
        )
    }
)
public class FavoritesResult implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "name")
    private String name;
    @Column(name = "kommentar")
    private String detail = "";
    @Column(name = "geschaeftsname")
    private String store = "";
    @Column(name = "geschaeftsdetails")
    private String storedetails = "";
    @Basic(optional = false)
    @NotNull
    @Column(name = "anzahl")
    private int anzahl;

    public FavoritesResult() {
    }

    public FavoritesResult(Long id) {
        this.id = id;
    }

    public FavoritesResult(Long id, String name, int anzahl) {
        this.id = id;
        this.name = name;
        this.anzahl = anzahl;
    }

    public FavoritesResult(Long id, String name, String detail, String store, String storedetails, int anzahl) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.store = store;
        this.storedetails = storedetails;
        this.anzahl = anzahl;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getStoredetails() {
        return storedetails;
    }

    public void setStoredetails(String storedetails) {
        this.storedetails = storedetails;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FavoritesResult)) {
            return false;
        }
        FavoritesResult other = (FavoritesResult) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ShopAssistant.Entities.FavoritesResult[ id=" + id + " name = " + name + " count = " + anzahl + " ]";
    }
    
}
