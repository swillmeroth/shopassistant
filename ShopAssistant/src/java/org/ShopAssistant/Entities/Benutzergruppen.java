/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "BenutzerGruppen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Benutzergruppen.findAll", query = "SELECT b FROM Benutzergruppen b"),
    @NamedQuery(name = "Benutzergruppen.findById", query = "SELECT b FROM Benutzergruppen b WHERE b.id = :id"),
    @NamedQuery(name = "Benutzergruppen.findByBenutzerId", query = "SELECT b FROM Benutzergruppen b WHERE b.benutzerID.id = :id"),
    @NamedQuery(name = "Benutzergruppen.findByGruppenId", query = "SELECT b FROM Benutzergruppen b WHERE b.gruppenID.id = :id")})
public class Benutzergruppen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "BenutzerID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Benutzer benutzerID;
    @JoinColumn(name = "GruppenID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Gruppen gruppenID;

    public Benutzergruppen() {
    }

    public Benutzergruppen(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Benutzer getBenutzerID() {
        return benutzerID;
    }

    public void setBenutzerID(Benutzer benutzerID) {
        this.benutzerID = benutzerID;
    }

    public Gruppen getGruppenID() {
        return gruppenID;
    }

    public void setGruppenID(Gruppen gruppenID) {
        this.gruppenID = gruppenID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Benutzergruppen)) {
            return false;
        }
        Benutzergruppen other = (Benutzergruppen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ShopAssistant.Entities.Benutzergruppen[ id=" + id + " ]";
    }
    
}
