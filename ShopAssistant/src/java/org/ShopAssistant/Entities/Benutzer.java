/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "Benutzer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Benutzer.findAll", query = "SELECT b FROM Benutzer b"),
    @NamedQuery(name = "Benutzer.findById", query = "SELECT b FROM Benutzer b WHERE b.id = :id"),
    @NamedQuery(name = "Benutzer.findByName", query = "SELECT b FROM Benutzer b WHERE b.name = :name"),
    @NamedQuery(name = "Benutzer.findByEMail", query = "SELECT b FROM Benutzer b WHERE b.eMail = :eMail"),
    @NamedQuery(name = "Benutzer.findByPasswort", query = "SELECT b FROM Benutzer b WHERE b.passwort = :passwort")})
public class Benutzer implements Serializable {
    @OneToMany(mappedBy = "GruppenErsteller")
    private Collection<Gruppen> gruppenCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "EMail")
    private String eMail;
    @Size(max = 255)
    @Column(name = "Passwort")
    private String passwort;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "benutzerID")
    private Collection<Benutzergruppen> benutzergruppenCollection;
    @OneToMany(mappedBy = "ErstelltVon")
    private Collection<Listenpositionen> listenpositionenCollection;
    @OneToMany(mappedBy = "GekauftVon")
    private Collection<Listenpositionen> listenpositionenCollection1;
    @OneToMany(mappedBy = "Ersteller")
    private Collection<Listen> listenCollection;

    public Benutzer() {
    }

    public Benutzer(Integer id) {
        this.id = id;
    }

    public Benutzer(Integer id, String name, String eMail) {
        this.id = id;
        this.name = name;
        this.eMail = eMail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    @XmlTransient
    public Collection<Benutzergruppen> getBenutzergruppenCollection() {
        return benutzergruppenCollection;
    }

    public void setBenutzergruppenCollection(Collection<Benutzergruppen> benutzergruppenCollection) {
        this.benutzergruppenCollection = benutzergruppenCollection;
    }

    @XmlTransient
    public Collection<Listenpositionen> getListenpositionenCollection() {
        return listenpositionenCollection;
    }

    public void setListenpositionenCollection(Collection<Listenpositionen> listenpositionenCollection) {
        this.listenpositionenCollection = listenpositionenCollection;
    }

    @XmlTransient
    public Collection<Listenpositionen> getListenpositionenCollection1() {
        return listenpositionenCollection1;
    }

    public void setListenpositionenCollection1(Collection<Listenpositionen> listenpositionenCollection1) {
        this.listenpositionenCollection1 = listenpositionenCollection1;
    }

    @XmlTransient
    public Collection<Listen> getListenCollection() {
        return listenCollection;
    }

    public void setListenCollection(Collection<Listen> listenCollection) {
        this.listenCollection = listenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Benutzer)) {
            return false;
        }
        Benutzer other = (Benutzer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ShopAssistant.Entities.Benutzer[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Gruppen> getGruppenCollection() {
        return gruppenCollection;
    }

    public void setGruppenCollection(Collection<Gruppen> gruppenCollection) {
        this.gruppenCollection = gruppenCollection;
    }
    
}
