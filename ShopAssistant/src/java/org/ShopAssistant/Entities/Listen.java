/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "Listen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Listen.findAll", query = "SELECT l FROM Listen l"),
    @NamedQuery(name = "Listen.findById", query = "SELECT l FROM Listen l WHERE l.id = :id"),
    @NamedQuery(name = "Listen.findByName", query = "SELECT l FROM Listen l WHERE l.name = :name"),
    @NamedQuery(name = "Listen.findByGroupID", query = "SELECT DISTINCT l FROM Gruppen g, Listen l WHERE g.id = :id AND g.id = l.gruppe.id"),
    @NamedQuery(name = "Listen.findByIstGeloescht", query = "SELECT l FROM Listen l WHERE l.istGeloescht = :istGeloescht")})
public class Listen implements Serializable, Comparable<Listen> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IstGeloescht")
    private boolean istGeloescht;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liste")
    private Collection<Listenpositionen> listenpositionenCollection;
    @JoinColumn(name = "Ersteller", referencedColumnName = "ID")
    @ManyToOne
    private Benutzer ersteller;
    @JoinColumn(name = "Gruppe", referencedColumnName = "ID")
    @ManyToOne
    private Gruppen gruppe;

    public Listen() {
    }

    public Listen(Integer id) {
        this.id = id;
    }

    public Listen(Integer id, String name, boolean istGeloescht) {
        this.id = id;
        this.name = name;
        this.istGeloescht = istGeloescht;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIstGeloescht() {
        return istGeloescht;
    }

    public void setIstGeloescht(boolean istGeloescht) {
        this.istGeloescht = istGeloescht;
    }

    @XmlTransient
    public Collection<Listenpositionen> getListenpositionenCollection() {
        return listenpositionenCollection;
    }

    public void setListenpositionenCollection(Collection<Listenpositionen> listenpositionenCollection) {
        this.listenpositionenCollection = listenpositionenCollection;
    }

    public Benutzer getErsteller() {
        return ersteller;
    }

    public void setErsteller(Benutzer ersteller) {
        this.ersteller = ersteller;
    }

    public Gruppen getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppen gruppe) {
        this.gruppe = gruppe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Listen)) {
            return false;
        }
        Listen other = (Listen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ShopAssistant.Entities.Listen[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Listen liste) {
        return this.name.compareToIgnoreCase(liste.getName());
    }  
}
