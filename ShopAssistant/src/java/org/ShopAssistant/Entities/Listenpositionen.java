/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "Listenpositionen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Listenpositionen.findAll", query = "SELECT l FROM Listenpositionen l"),
    @NamedQuery(name = "Listenpositionen.findById", query = "SELECT l FROM Listenpositionen l WHERE l.id = :id"),
    @NamedQuery(name = "Listenpositionen.findByName", query = "SELECT l FROM Listenpositionen l WHERE l.name = :name"),
    @NamedQuery(name = "Listenpositionen.findByErstelltVon", query = "SELECT l FROM Listenpositionen l WHERE l.erstelltVon.id = :id"),
    @NamedQuery(name = "Listenpositionen.findByGeschaeftsname", query = "SELECT l FROM Listenpositionen l WHERE l.geschaeftsname = :geschaeftsname"),
    @NamedQuery(name = "Listenpositionen.findByGeschaeftsdetails", query = "SELECT l FROM Listenpositionen l WHERE l.geschaeftsdetails = :geschaeftsdetails"),
    @NamedQuery(name = "Listenpositionen.findByAnzahl", query = "SELECT l FROM Listenpositionen l WHERE l.anzahl = :anzahl"),
    @NamedQuery(name = "Listenpositionen.findByGekauftDatum", query = "SELECT l FROM Listenpositionen l WHERE l.gekauftDatum = :gekauftDatum"),
    @NamedQuery(name = "Listenpositionen.findByGekauft", query = "SELECT l FROM Listenpositionen l WHERE l.gekauft = :gekauft"),
    @NamedQuery(name = "Listenpositionen.findByListenId", query = "SELECT l FROM Listenpositionen l WHERE l.liste.id = :id"),
    @NamedQuery(name = "Listenpositionen.findByGekauftVon", query = "SELECT l FROM Listenpositionen l WHERE l.gekauftVon.id = :id")})
public class Listenpositionen implements Serializable, Comparable<Listenpositionen> {
    @Basic(optional = false)
    @NotNull
    @Column(name = "istGeloescht")
    private boolean istGeloescht;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Name")
    private String name;
    @Size(max = 255)
    @Column(name = "Geschaeftsname")
    private String geschaeftsname;
    @Size(max = 255)
    @Column(name = "Geschaeftsdetails")
    private String geschaeftsdetails;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Anzahl")
    private int anzahl;
    @Lob
    @Size(max = 65535)
    @Column(name = "Kommentar")
    private String kommentar;
    @Column(name = "GekauftDatum")
    @Temporal(TemporalType.DATE)
    private Date gekauftDatum;
    @Transient
    private String gekauftDatumString;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Gekauft")
    private boolean gekauft;
    @JoinColumn(name = "ErstelltVon", referencedColumnName = "ID")
    @ManyToOne
    private Benutzer erstelltVon;
    @JoinColumn(name = "GekauftVon", referencedColumnName = "ID")
    @ManyToOne
    private Benutzer gekauftVon;
    @JoinColumn(name = "Liste", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Listen liste;

    public Listenpositionen() {
    }

    public Listenpositionen(Integer id) {
        this.id = id;
    }

    public Listenpositionen(Integer id, String name, int anzahl, boolean gekauft) {
        this.id = id;
        this.name = name;
        this.anzahl = anzahl;
        this.gekauft = gekauft;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeschaeftsname() {
        return geschaeftsname;
    }

    public void setGeschaeftsname(String geschaeftsname) {
        this.geschaeftsname = geschaeftsname;
    }

    public String getGeschaeftsdetails() {
        return geschaeftsdetails;
    }

    public void setGeschaeftsdetails(String geschaeftsdetails) {
        this.geschaeftsdetails = geschaeftsdetails;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public Date getGekauftDatum() {
        return gekauftDatum;
    }

    public void setGekauftDatum(Date gekauftDatum) {
        this.gekauftDatum = gekauftDatum;
    }

    public boolean getGekauft() {
        return gekauft;
    }

    public void setGekauft(boolean gekauft) {
        this.gekauft = gekauft;
    }

    public Benutzer getErstelltVon() {
        return erstelltVon;
    }

    public void setErstelltVon(Benutzer erstelltVon) {
        this.erstelltVon = erstelltVon;
    }

    public Benutzer getGekauftVon() {
        return gekauftVon;
    }

    public void setGekauftVon(Benutzer gekauftVon) {
        this.gekauftVon = gekauftVon;
    }

    public Listen getListe() {
        return liste;
    }

    public void setListe(Listen liste) {
        this.liste = liste;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Listenpositionen)) {
            return false;
        }
        Listenpositionen other = (Listenpositionen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ShopAssistant.Entities.Listenpositionen[ id=" + id + " ]";
    }

    public boolean getIstGeloescht() {
        return istGeloescht;
    }

    public void setIstGeloescht(boolean istGeloescht) {
        this.istGeloescht = istGeloescht;
    }

    public String getGekauftDatumString() {
        return gekauftDatumString;
    }

    public void setGekauftDatumString(String gekauftDatumString) {
        this.gekauftDatumString = gekauftDatumString;
    }
    
    @Override
    public int compareTo(Listenpositionen o) {
        return this.name.compareToIgnoreCase(o.getName());
    }
    
}
