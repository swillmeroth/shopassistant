package org.ShopAssistant.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.webapp.FacetTag;
import org.ShopAssistant.JPA.GruppenJPA;
import org.ShopAssistant.JPA.ListenJPA;
import org.ShopAssistant.Entities.FavoritesResult;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Entities.Listen;
import org.ShopAssistant.Entities.Listenpositionen;
import org.ShopAssistant.JPA.FavoritenJPA;
import org.ShopAssistant.beans.LoginLogout;
import org.primefaces.behavior.ajax.AjaxBehavior;
import org.primefaces.behavior.ajax.AjaxBehaviorListenerImpl;
import org.primefaces.component.celleditor.CellEditor;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.separator.UISeparator;
import org.primefaces.component.spacer.Spacer;

/**
 * Hauptbean die alle Listen für den CenterContent erstellt
 * @autor Andre Hantke
 * kommentiert am 19.05.2015
 * Zusatz: Panel beinhaltet alle Listen. Verknüpfung zu listen.xhtml!!
*/
@ManagedBean
@SessionScoped
public class ShopList implements Serializable{
    
    /**
     * Kommunikation zwischen den Beans
     * hiermit kann auf den eingeloggten Nutzer zugegriffen werden
    */
    @ManagedProperty(value="#{loginLogout}")
    private LoginLogout loginLogout;
    
    //zustandsloses Panel
    private transient Panel zettel;
    //Positionen einer Liste
    private List<Listenpositionen> aktuellePositioneneinerListe = null;
    //Alle Positionen aller Listen
    private List<Listenpositionen> aktuellePositionenallerListen = null;
    //Favoritelliste
    private List<FavoritesResult> favoritenListe = null;
    //weitere favoritenliste
    private List<FavoritesResult> fav = null;
    //Arraylist zwischenspreichern der nicht zustandsbehaftetetn Fieldsets
    private transient List<Fieldset> fliste;
    
    //Listennamen
    private String listname;
    
    //ListenID zu löschen
    private int deletelistID;
    //anzahl
    private int anzahl;
    //id des Favoriten
    private int favoritListID;
    //gruppen id
    private int gruppenlistid;
    //selektiertes item des Drop Down
    private String item;
    
    //persist
    private ListenJPA list;
    
    //Gruppennamen
    private String gruppennamen;
    /**
     * Liste der Gruppennamen für das Drop Down
     * Beinhaltet ausschließlich die Gruppennamen als String
     */ 
    public List<String> gruppenlistest;

    /**
     * Konstruktor
     * Initilisiert das Panel, dass alle Listen umfasst, sowie alle ArrayListen.
     */
    public ShopList() {
        zettel = new Panel();
        zettel.setId("zettel");
        aktuellePositionenallerListen=new ArrayList();
        favoritenListe=new ArrayList();
        fav = new ArrayList();
        fliste=new ArrayList();
    }
    
    /**
     * Getter Methode von Panel Zettel
     * @return Panel mit allen Listen    
    */
    public Panel getZettel() {
        return zettel;
    }
    
    /**
     * Setter Methode von Panel Zettel
     * @param zettel Panel mit den Listen
     */
    public void setZettel(Panel zettel) {
        this.zettel = zettel;
    }
    
    /**
     * Liefert alle Listenpositionen einer Liste als ArrayList
     * @return Liste von Listenpositionen
     */
    public List<Listenpositionen> getAktuellePositioneneinerListe() {
        return aktuellePositioneneinerListe;
    }

    /**
     * Setzt alle Listenpositionen einer Liste
     * @param aktuellePositioneneinerListe Listenpositionen einer Liste
     */
    public void setAktuellePositioneneinerListe(List<Listenpositionen> aktuellePositioneneinerListe) {
        this.aktuellePositioneneinerListe = aktuellePositioneneinerListe;
    }

    /**
     * Liefert alle Positionen aller Listen
     * @return Liste von Listenpositionen
     */
    public List<Listenpositionen> getAktuellePositionenallerListen() {
        return aktuellePositionenallerListen;
    }

    /**
     * Setzen aller Listenpositionen aller Listen
     * @param aktuellePositionenallerListen Listenpositionen aller Listen
     */
    public void setAktuellePositionenallerListen(List<Listenpositionen> aktuellePositionenallerListen) {
        this.aktuellePositionenallerListen = aktuellePositionenallerListen;
    }

    /**
     * Liefert die Favoriten in Form einer ArrayList
     * @return Liste von FavoritenResult
     */
    public List<FavoritesResult> getFavoritenListe() {
        return favoritenListe;
    }

    /**
     * Setzen der Favoriten
     * @param favoritenListe FavoritenListe
     */
    public void setFavoritenListe(List<FavoritesResult> favoritenListe) {
        this.favoritenListe = favoritenListe;
    }
    
    /**
     * Liefert den Gruppennamen
     * @return String
     */
    public String getGruppennamen() {
        return gruppennamen;
    }
    
    /**
     * Setzen des Gruppennamens
     * @param gruppennamen Gruppenname
     */
    public void setGruppennamen(String gruppennamen) {
        this.gruppennamen = gruppennamen;
    }

    /**
     * Liefert eine Liste von Gruppenamen gesammelt als String in
     * einer ArrayList
     * 
     * @return Liste von Strings
     */
    public List<String> getGruppenlistest() {
        return gruppenlistest;
    }

    /**
     * Setzen der ArrayList mit den Gruppennamen (String)
     * @param gruppenlistest 
     */
    public void setGruppenlistest(List<String> gruppenlistest) {
        this.gruppenlistest = gruppenlistest;
    }
    
    /**
     * Selektierte Item aus dem Drop Down Menü mit den
     * Gruppennamen
     * @return String
     */
    public String getItem(){
        return item;
    }

    /**
     * Setzen des selektierten Items des Drop Down Menüs mit 
     * den Gruppennamen
     * @param item 
     */
    public void setItem(String item) {
        System.out.println("Selected Gruppe:" + item);
        this.item = item;
    }

    /**
     * Liefert die Gruppen Id einer Liste
     * @return Interger
     */
    public int getGruppenlistid() {
        return gruppenlistid;
    }

    /**
     * Setzen der GruppenID einer Liste
     * Setzen des Listennamens
     * Aufruf der MEthode makeZettel zur Aktualisierung aller Listen
     * @param gruppenlistid
     * @param name 
     */
    public void setGruppenlistid(int gruppenlistid, String name) {
        this.gruppenlistid = gruppenlistid;
        this.setListname(name);
        makeZettel();
    }

    /**
     * Liefert die Id der Liste zu Favoriten
     * @return Integer
     */
    public int getFavoritListID() {
        return favoritListID;
    }

    /**
     * Setzen der ID zu Favoriten
     * Favoritengenerierung
     * @param favoritListID 
     */
    public void setFavoritListID(int favoritListID) {
        generateFavoriten();
        this.favoritListID = favoritListID;
    }
    
    /**
     * Liefert die Favoriten in Form einer ArrayList
     * @return Liste von FavoritenResult
     */
    public List<FavoritesResult> getFav() {
        return fav;
    }

    /**
     * Stezn der FavoritenListe
     * @param fav Favoritenliste
     */
    public void setFav(List<FavoritesResult> fav) {
        this.fav = fav;
    }
    
    /**
     * Liefert Anzahl an UserListen
     * @return Integer
     */
    public int getAnzahl() {
        return anzahl;
    }

    /**
     * Setzen der Anzahl an USersListen
     * @param anzahl UserListen
     */
    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    /**
     * Kommunikation mit der ManagedBean LoginLogout
     * Getter Methode
     * @return LoginLogout
     */
    public LoginLogout getLoginLogout() {
        return loginLogout;
    }

    /**
     * Kommunikation mit der ManagedBean LoginLogout
     * Setter Methode
     * @param loginLogout Referenz auf die Bean LoginLogout
     */
    public void setLoginLogout(LoginLogout loginLogout) {
        this.loginLogout = loginLogout;
    }

    /**
     * Liefert die ID der Liste, die zu löschen ist.
     * @return Interger
     */
    public int getDeletelistID() {
        return deletelistID;
    }

    /**
     * Setzen der ID der Liste, die zu löschen ist.
     * @param deletelistID ID der Liste
     */
    public void setDeletelistID(int deletelistID) {
        this.deletelistID = deletelistID;
    }
    
    /**
     * Liefert den Namen einer Liste
     * @return String
     */
    public String getListname() {
        return listname;
    }

    /**
     * Setzen des Listennamens
     * @param listname Name der Liste
     */
    public void setListname(String listname) {
        this.listname = listname;
    }
    
   
    /**
     * Methode zum Erstellen einer neuen Liste
     * Setzen des Ersteller durch den Nutzer in LoginLogout Bean
     * Aufruf HauptAlgorithmus: makeZettel()
     * 
     */
    public void makeNewList(){
        //Listen JPA
        ListenJPA ldi = new ListenJPA();        
        
        //neue Liste
        Listen a = new Listen();
        
        //ID
        int id;
        
        try{        
            //Setze Namen der Liste
            a.setName(listname);
            
            //Setzen des Erstellers
            a.setErsteller(loginLogout.getRegistrierterNutzer());        
            
            //Persist neue Liste
            ldi.addList(a);
            
            //Panel clearen
            zettel.getChildren().clear();
            
            //hole ID des eingeloggten Nutzers
            id=loginLogout.getRegistrierterNutzer().getId();
            
            //Aktualisierung der Userlisten des eingeloggten Nutzers
            loginLogout.setUsersListen(ldi.getListenByUserID(id));

            //Dialog mit Eingabe des Namens für die neue Liste schließen
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('listnamenDialog').hide()");
            
            //Ansicht aller Zettel neu laden
            makeZettel();
        }catch(java.lang.NullPointerException e){
            Logger.getLogger(ShopList.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /** 
     *  Methode zum Erstellen eines neuen Eintrages in eine Liste
     * 
     *  @param idd ID der Liste
     *  @param listname zum togglen
    */
    public void makeNewEntry(int idd, String listname){
        
        ListenJPA ldi = new ListenJPA();
        Listen l=null;
        
        //hole Liste mit der entsprechenden ID
        for(int i=0;i<loginLogout.getUsersListen().size();i++){
            if(loginLogout.getUsersListen().get(i).getId()==idd){
                l = loginLogout.getUsersListen().get(i);
            }
        }
        
        //neue Listenposition
        Listenpositionen a = new Listenpositionen();
        
        //setze Defaultnamen
        a.setName("");
        
        //setze DefaultKommentar
        a.setKommentar("");
        
        //setze Default Details
        a.setGeschaeftsdetails("");
        
        //Setze Geschaeftsnamen
        a.setGeschaeftsname("");
        
        //Setze Anzahl
        a.setAnzahl(1);
        
        //Setze Liste
        a.setListe(l);
        
        //setze gekauft
        a.setGekauft(false);
        
        //setze gekauft Datum
        a.setGekauftDatum(null);
        
        //setze gekauft von
        a.setGekauftVon(null);
        
        //setze erstellt von, durch den eingeloggten Nutzer in der Bean
        a.setErstelltVon(loginLogout.getRegistrierterNutzer());
        
        //persist neue Position zur aktuellen Liste
        ldi.addNewEintrag(a);
        
        //Anzeige aktualisieren
        makeZettel();
        
        //generiere Favoriten neu
        generateFavoriten();
        
        //Steurung der Fieldsets
        for (int j=0; j<fliste.size();j++){
            if(fliste.get(j).getLegend().equals(listname)){
                fliste.get(j).setCollapsed(false);
            }else{
                fliste.get(j).setCollapsed(true);
            }
        }
    }
    
    /**
     * Löschen einer Liste
     */
    public void deleteList(){
        ListenJPA ldi = new ListenJPA();
        //Liste löschen über entsprechende ID
        ldi.delList(this.getDeletelistID());
        
        //hole ID des eingeloggten Nutzers
        int userid=loginLogout.getRegistrierterNutzer().getId();
        
        //Listen des Users aktualisieren
        loginLogout.setUsersListen(ldi.getListenByUserID(userid));
        
        //zettel clearen
        zettel.getChildren().clear();
        
        //Dialog schließen
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('listeloeschenDialog').hide()");
        
        //Zettel neu anzeigen
        makeZettel();
    }
    
    
    /**
     * Event Methode bei Eingabe von Werte in einer Zelle
     */
    public void onCellEdit(){
        ListenJPA ldi = new ListenJPA();
        
        //alle Positionen werden neu persisitiert
        ldi.persistChangedPositionen(aktuellePositionenallerListen);
        
        RequestContext context = RequestContext.getCurrentInstance();
        
        //Favoriten neu generieren
        generateFavoriten();
        
        //Favoriten Anzeige aktualisieren
        context.update(":favoritenform");
    }
    
    
    /**
     * Methode, die aufgerufen wird, wenn erledigt geklickt wird
     * @param name Name der Liste fürs Toogln des Fieldsets
     */
    public void onChange(String name){
        ListenJPA ldi = new ListenJPA();
        
        //Datum
        Date date = new Date();
        
        //Setzen des Datums + Persistierung
        for(int i=0; i<aktuellePositionenallerListen.size();i++){
            if((aktuellePositionenallerListen.get(i).getGekauft()) 
                && (aktuellePositionenallerListen.get(i).getGekauftVon()==null)
                && (aktuellePositionenallerListen.get(i).getGekauftDatum()==null)){
                
                aktuellePositionenallerListen.get(i).setGekauftDatum(date);
                aktuellePositionenallerListen.get(i).setGekauftVon(loginLogout.getRegistrierterNutzer());
            }
        }
        for(int i=0; i<aktuellePositionenallerListen.size();i++){
            if((aktuellePositionenallerListen.get(i).getGekauft()==false)){
                aktuellePositionenallerListen.get(i).setGekauftDatum(null);
                aktuellePositionenallerListen.get(i).setGekauftVon(null);
            }
        }
        ldi.persistChangedPositionen(aktuellePositionenallerListen);
        
        //Neu Laden aller Zettel
        makeZettel();
        
        //Toggel
        for (int j=0; j<fliste.size();j++){
           if(fliste.get(j).getLegend().equals(name)){
               fliste.get(j).setCollapsed(false);
           }else{
               fliste.get(j).setCollapsed(true);
           }
        } 
    }
  
    /**
     * Favoriten Generierung
     */
    public void generateFavoriten(){
        FavoritenJPA favoriten= new FavoritenJPA();
        
        //Setze Liste der Favoriten neu, in Abhängigkleit des Nutzers
        if(loginLogout.getUsersListen()!=null)
        {
            int id = loginLogout.getRegistrierterNutzer().getId();
            this.favoritenListe.clear();
            this.setFavoritenListe(favoriten.getFavorites(id));
        }
    }
    
    /**
     * Hinzufügen eines Favoriten zu einer Liste    
     * @param event aus dem Facelet beim Selektieren der Reihe in der Datenbank
     */
    public void addNewFavorit(RowEditEvent event){   
        RequestContext context = RequestContext.getCurrentInstance();
        FavoritesResult fav = (FavoritesResult) event.getObject();
        ListenJPA ldi = new ListenJPA();
        Listen favl=null;
    
        //Hole die aktuelle Liste des Nutzers, und adde die neue Position hinzu
        //anschließende Persistierung
        //und Dialog schließen
        for(int i=0;i<loginLogout.getUsersListen().size();i++){
            if(loginLogout.getUsersListen().get(i).getId()==this.getFavoritListID()){
                 favl = loginLogout.getUsersListen().get(i);
                 
                 //neue Listenposition
                 Listenpositionen posneu = new Listenpositionen();
                 
                    //setze Anzahl
                    posneu.setAnzahl(1);
                    
                    //setze Namen
                    posneu.setName(fav.getName());
                    
                    //setze Kommentar
                    posneu.setKommentar(fav.getDetail());
                    
                    //setze Geschaeftsnamen
                    posneu.setGeschaeftsname(fav.getStore());
                    
                    //setze Details
                    posneu.setGeschaeftsdetails(fav.getStoredetails());
                    
                    //setze Datum
                    posneu.setGekauftDatum(null);
                    
                    //setze gekauftvon
                    posneu.setGekauftVon(null);
                    
                    //setze Liste
                    posneu.setListe(favl);
                    
                    //setze erstellt von
                    posneu.setErstelltVon(loginLogout.getRegistrierterNutzer());
                    
                 //persist   
                 ldi.addFavorit(posneu);
                 
                 //Dialog schließen
                 context.execute("PF('favoritenDialog').hide()");
            }else{
                 System.out.println("Liste not existed");
            }
        }
        
        //neu Anzeigen
        makeZettel();
        
        //Favoriten neu generieren
        generateFavoriten();
        
        //Chartkomponenten updaten
        context.update("chart");   
    }
    
    /**
     * Setzen der Gruppe zu einer Liste
     */
    public void setGruppezurListe(){
        GruppenJPA gruppe = new GruppenJPA();
        ListenJPA ldi = new ListenJPA();
        
        //update der Gruppen eines Users
        loginLogout.setGlisten(gruppe.getByUserID(loginLogout.getRegistrierterNutzer().getId()));
        
        Gruppen g=null;
        //hole Gruppe übder das seletktierte Item im DropDown
        for(int i=0; i<loginLogout.getGlisten().size();i++){
            if(loginLogout.getGlisten().get(i).getName().equals(this.getItem())){
                g = loginLogout.getGlisten().get(i);
            }
        }
        
        Listen l=null;
        
        //setze gruppe in Liste
        for(int i=0; i<loginLogout.getUsersListen().size();i++){
            if(loginLogout.getUsersListen().get(i).getId()==gruppenlistid){
                
                //hole Liste des Users
                l = loginLogout.getUsersListen().get(i);
                
                //setze ID
                l.setId(loginLogout.getUsersListen().get(i).getId());
                
                //Setze Ersteller
                l.setErsteller(loginLogout.getUsersListen().get(i).getErsteller());
                
                //Setze Gruppe
                l.setGruppe(g);
                
                //setze ist gelöscht
                l.setIstGeloescht(loginLogout.getUsersListen().get(i).getIstGeloescht());
                
                //setze Collection
                l.setListenpositionenCollection(loginLogout.getUsersListen().get(i).getListenpositionenCollection());
                
                //setze Namen
                l.setName(loginLogout.getUsersListen().get(i).getName());
            }
        }
        //persistiere liste
        ldi.setListGruppe(loginLogout.getUsersListen());
        
        //Aktualisiere User Listenm
        loginLogout.setUsersListen(ldi.getListenByUserID(loginLogout.getRegistrierterNutzer().getId()));    
        
        //Dialog schließen
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('gruppenselectDialog').hide()");
        
        //Zettel neu laden
        makeZettel();
        
        //toggle
        for (int j=0; j<fliste.size();j++){
          if(fliste.get(j).getLegend().equals(this.getListname())){
              fliste.get(j).setCollapsed(false);
          }else{
              fliste.get(j).setCollapsed(true);
          }
        }
    }
    
    /**
     * Liste von Gruppe lösen (Papierkorb)
     * @param id der Liste
     * @param liste Name der Liste
     */
    public void exitListevonGruppe(int id, String liste){
        ListenJPA ldi = new ListenJPA();
        Listen l=null;
        
        //hole die aktuelle Liste und setze Gruppe auf null
        for(int i=0; i<loginLogout.getUsersListen().size();i++){
            if(loginLogout.getUsersListen().get(i).getId()==id){
                l = loginLogout.getUsersListen().get(i);
                l.setGruppe(null);
            }
        }
        
        //persist
        ldi.setListGruppe(loginLogout.getUsersListen());
        
        //aktualisiere die Listen des Users
        loginLogout.setUsersListen(ldi.getListenByUserID(loginLogout.getRegistrierterNutzer().getId()));    
        
        //Aktualisiere die Gruppen Listen des Users
        GruppenJPA gruppe = new GruppenJPA();
        loginLogout.setGlisten(gruppe.getByUserID(loginLogout.getRegistrierterNutzer().getId()));
        
        //Zettel neu Laden
        makeZettel();
        
        //Aktualisierungen + Dialog schließen
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('gruppenselectDialog').hide()");
        context.update("formy:za");
        context.update(":selectform");
        context.update(":exitform");
        context.update(":groupinform");
        context.update(":delform");
        
    }
    
    /**
     * Generierung des Inhaltes für das Drop Down Menü zur Auswahl 
     * der Gruppe (Gruppennamen)
     */
    public void generateGruppenDropDown(){
        this.gruppenlistest=new ArrayList();
        GruppenJPA gruppenUpdate = new GruppenJPA();
        
        try{
            //Gruppen des Users clearen und neu Laden
            loginLogout.getGlisten().clear();
            this.loginLogout.getGlisten().addAll(gruppenUpdate.getByUserID(this.loginLogout.getRegistrierterNutzer().getId()));

            //wenn Gruppen existieren, Flags setzen zur Anzeige
            if(this.loginLogout.getGlisten().size()>0){
                this.loginLogout.setGruppenvorhanden(true);
                this.loginLogout.setGruppennichtvorhanden(false);
            }else{
                this.loginLogout.setGruppenvorhanden(false);
                this.loginLogout.setGruppennichtvorhanden(true);
            }

            //StringArrayList des Drop Down MEnüs füllen mit den Gruppennamen
            if(loginLogout.getGlisten()!=null){
                this.gruppenlistest.clear();
                for(int i=0; i<loginLogout.getGlisten().size();i++){
                    this.gruppenlistest.add(loginLogout.getGlisten().get(i).getName());
            }
        }else{
            this.gruppenlistest.add("");
        }
        }catch(java.lang.NullPointerException e){
            Logger.getLogger(ShopList.class.getName()).log(Level.SEVERE, null, e + "generateGruppenDropDown");
        }
    }
    
    /**
     * Hauptalgorithmus zur Anzeige der Listen zu einem Nutzer
    */
    public void makeZettel(){ 
            list = new ListenJPA();
            
            //ID
            int id;
            
            //Name
            String name;
            
            //GruppenID
            int gruppenid;
            
            //Panel
            Panel temp=new Panel();
            
            RequestContext request = RequestContext.getCurrentInstance();
            
            //ArrayList der Positionen einer Liste
            List<Listenpositionen> t = new ArrayList();
            
            //Zettel clearen
            zettel.getChildren().clear();
        
            //generiere Favoriten
            generateFavoriten();
            
            //generiere das Drop Down
            generateGruppenDropDown();
            
            
            //wenn keine Listen vorhanden sind, dann wird das entsprechend angezeigt
            if((loginLogout.getUsersListen()==null)||(loginLogout.getUsersListen().isEmpty())){
                
                //entsprechendes Panel generieren, mit Keine Listen vorhanden...
                temp=noLists();
                
                //HauptPanel clearen
                zettel.getChildren().clear();
                
                //temp Panel hinzufügen
                zettel.getChildren().add(temp);
            }else{
                  //hole Anzhal der Listen des Users  
                  anzahl = loginLogout.getUsersListen().size();
                  
                  //Positionen clearen
                  t.clear();
                  
                  //alles clearen
                  aktuellePositionenallerListen.clear();
                  
                  //neu setzen
                  for(int j=0;j<anzahl;j++){
                    
                     //clearen
                    aktuellePositioneneinerListe = new ArrayList();
                    aktuellePositioneneinerListe.clear();
                    t.clear();
                    
                    //hole ID der Liste
                    id=loginLogout.getUsersListen().get(j).getId();
                    
                    //hole Namen der Liste
                    name=loginLogout.getUsersListen().get(j).getName();
                    
                    //hole alle Positionen der aktuellen Liste
                    t.addAll(list.getListPositionenByListID(id));
                    
                    //hole Gruppenamen! Anzeigesteuerung
                    if(loginLogout.getUsersListen().get(j).getGruppe()!=null)
                    {
                        //Hole die ID der Gruppe
                        gruppenid=loginLogout.getUsersListen().get(j).getGruppe().getId();
                        for(int i=0; i<loginLogout.getGlisten().size();i++){
                            if(loginLogout.getGlisten().get(i).getId().equals(gruppenid)){
                                this.setGruppennamen(loginLogout.getGlisten().get(i).getName());
                                System.out.println("this: " + this.getGruppennamen());
                            }
                        }
                    }else{
                        this.setGruppennamen("");
                    }
                    
                    //Abruf der ITEMS zu einer Liste
                    for(int i=0; i<t.size();i++){
                        if(t.get(i).getIstGeloescht()==false){
                            aktuellePositioneneinerListe.add(t.get(i));
                            aktuellePositionenallerListen.add(t.get(i));
                        }
                    } 
                    
                    //hole Ersteller der Liste
                    String ersteller=loginLogout.getUsersListen().get(j).getErsteller().getName();
                    
                    //Aufruf zur Generierung der Liste !!!!
                    temp=Zettel(j, id, name, aktuellePositioneneinerListe, this.getGruppennamen(), ersteller);     
                    
                    //HauptPanel hinzufügen
                    zettel.getChildren().add(temp);
                 }
            }
        //Update des Hauptpanels im Facelet Listen.xhtml    
        request.update("formy:za");    
    }
    
    
    /**
     *   Methode zur Erstellung einer Liste als Panel
     *   Für jede Liste eines Users, wird ein neues Panel erstellt, 
     *   und ist somit unabhängig von den Anderen
     * 
     *   @param ita Wert der Iteration
     *   @param id ID der Liste
     *   @param listenname Name der Liste
     *   @param tempPos temporäre Listenpositionen
     *   @param gruppennamen Name der Gruppe
     *   @param ersteller Ersteller der Liste
     *   @return Primefaces Panel, mit Listeninhalt
    */
    public Panel Zettel(int ita, int id, String listenname, List<Listenpositionen> tempPos, String gruppennamen, String ersteller){
        //FacesContext
        FacesContext context = FacesContext.getCurrentInstance(); 
        //ELContext
        ELContext elc = context.getELContext();
        //EL Facotries
        ExpressionFactory ef = context.getApplication().getExpressionFactory();
        ExpressionFactory ef2 = context.getApplication().getExpressionFactory();
        ExpressionFactory ef3 = context.getApplication().getExpressionFactory();
        //Panel
        Panel shopPanel = new Panel();
      
        //Fieldset
        //togglebar + speed 200 ms + Namen der Liste
        Fieldset liste = new Fieldset();
        liste.setToggleable(true);
        liste.setToggleSpeed(200);
        liste.setLegend(listenname);
        
        //Panelgrid linksausrichtung
        HtmlPanelGrid listenkopflinks = new HtmlPanelGrid();
        listenkopflinks.setColumns(4);
        listenkopflinks.setStyle("float: left");
        
        //Label wer hats erstellt
        OutputLabel erstellerLabel = new OutputLabel();
        erstellerLabel.setValue("Liste erstellt von:");      
        listenkopflinks.getChildren().add(erstellerLabel);
        
        //ersteller setzen
        OutputLabel erstelltvon = new OutputLabel();
        erstelltvon.setValue(ersteller);
        listenkopflinks.getChildren().add(erstelltvon);
        liste.getChildren().add(listenkopflinks);
        
        //Spacer
        Spacer sp = new Spacer();
        listenkopflinks.getChildren().add(sp);
        Spacer sp1 = new Spacer();
        listenkopflinks.getChildren().add(sp1);
        
        //Gruppen Label
        OutputLabel gruppenLabel = new OutputLabel();
        gruppenLabel.setValue("Gruppe:");      
        listenkopflinks.getChildren().add(gruppenLabel);
        
        OutputLabel gruppevon = new OutputLabel();
        
        //CommandButton zum Setzen der Gruppe
        CommandButton gcmd = new CommandButton();
        //Icon
        gcmd.setIcon("ui-icon-pencil");
        //Ajaxable
        gcmd.setAjax(true);
        //Aufruf der Methode zum Setzen der Gruppe zu einer Liste
        MethodExpression exp000 = ef3.createMethodExpression(elc, "#{shopList.setGruppenlistid("+id+",'"+listenname+"')}", String.class, null);
        //setze Expression
        gcmd.setActionExpression(exp000);
        
        //CommandButton Papierkorb
        CommandButton gcmdexit = new CommandButton();
        //Icon
        gcmdexit.setIcon("ui-icon-trash");
        MethodExpression e000 = ef3.createMethodExpression(elc, "#{shopList.exitListevonGruppe("+id+",'"+listenname+"')}", String.class, null);
        //setze Ausdruck
        gcmdexit.setActionExpression(e000);
        
        //Anzeigensteuerung, was die Gruppen angeht
        //existieren zum aktuellen eingeloggten Nutzer keine Gruppen
        //so wird nichts angezeigt
        //sobald eine Gruppe erstellt wird, werden Komponeten angezeigt
        //Gruppe + Namen + Auswahl der Gruppe + Papierkorb
        //Update der Komponenten
        if(loginLogout.getGlisten().isEmpty()||loginLogout.getGlisten()==null){
            gcmdexit.setRendered(false);
            gcmd.setRendered(false);
            gruppenLabel.setRendered(false);
            gruppevon.setRendered(false);
        }else{
            if(ersteller.equals(loginLogout.getRegistrierterNutzer().getName())){
                gcmd.setRendered(true);
                gcmdexit.setRendered(true);
            }else{
                gcmd.setRendered(false);
                gcmdexit.setRendered(false);
            }
            gruppevon.setRendered(true);
            gruppenLabel.setRendered(true);
            
            if(gruppennamen.equals("")){
                gruppevon.setValue("Keiner Gruppe zugeordnet!");
            }else{
                gruppevon.setValue(gruppennamen);
            }
            gcmd.setOnclick("PF('gruppenselectDialog').show()");
            gcmd.setUpdate(":selectform");
        } 
        
        //setzen in die Panelgrids
        listenkopflinks.getChildren().add(gruppevon);
        listenkopflinks.getChildren().add(gcmd);     
     
        listenkopflinks.getChildren().add(gcmdexit);
        liste.getChildren().add(listenkopflinks);
        
        //Panelgrid rehtsausrichtung
        HtmlPanelGrid listenkopfrechts = new HtmlPanelGrid();
        listenkopfrechts.setColumns(3);
        listenkopfrechts.setStyle("float: right");
        
        //Liste löschen
        CommandLink delete = new CommandLink();
        delete.setValue("Liste löschen");
        MethodExpression exp0 = ef2.createMethodExpression(elc, "#{shopList.setDeletelistID("+id+")}", String.class, null);
        delete.setActionExpression(exp0);
        delete.setOnclick("PF('listeloeschenDialog').show()");
        listenkopfrechts.getChildren().add(delete);
     
        //Favoriten Button
        CommandButton button = new CommandButton();
        button.setValue("Favoriten");
        button.setIcon("ui-icon-star");
        MethodExpression exp00 = ef3.createMethodExpression(elc, "#{shopList.setFavoritListID("+id+")}", String.class, null);
        button.setActionExpression(exp00);
        button.setOnclick("PF('favoritenDialog').show()");
        button.setUpdate(":favoritenform");
        listenkopfrechts.getChildren().add(button);
  
        liste.getChildren().add(listenkopfrechts);
        Spacer sp3 = new Spacer();
        
        sp3.setHeight("65px");
        liste.getChildren().add(sp3);
        UISeparator sep = new UISeparator();
        liste.getChildren().add(sep);
        
        //Wenn es Positionen zur Aktuellen Liste gibt, dann...
        if(tempPos.size()>0)
        {
            //Erstelle Datentabelle
            //editable
            //responsive: reflow
            //edit Mode: Zellen abhängig
            DataTable dt = new DataTable();        
            ValueExpression ve = ef.createValueExpression(tempPos, ArrayList.class);
            dt.setValueExpression("value", ve);
            dt.setVar("l");
            dt.setEditable(true);
            dt.setEditMode("cell");
            dt.setReflow(true);

            //Methodenaufruf/Registrierung bei Eingabe in eine Zelle
            MethodExpression me = ef.createMethodExpression(elc, "#{shopList.onCellEdit()}", String.class, null);
            //Ajax Verhalten
            AjaxBehavior ajax = new AjaxBehavior();
            ajax.addAjaxBehaviorListener(new AjaxBehaviorListenerImpl(me,me));
            dt.addClientBehavior("cellEdit", ajax);

            //Spalte erledigt
            Column erledigt = new Column();
            //responsive
            erledigt.setPriority(2);
                //Name der Spalte
                erledigt.setHeaderText("Erledigt?");
                //Spaltenbreite
                erledigt.setWidth("40");                  
                //Checkbox
                SelectBooleanCheckbox erledigtSBC = new SelectBooleanCheckbox();
                //ValueExpression Wert aus der Datenbank
                ValueExpression cve = ef.createValueExpression(elc, "#{l.gekauft}", String.class);
                erledigtSBC.setValueExpression("value", cve);
                AjaxBehavior ajax2 = new AjaxBehavior();
                
                //MEthode registrieren, die aufgerufen wird, wenn Checkbox selektiert wurde
                MethodExpression me2 = ef.createMethodExpression(elc, "#{shopList.onChange('"+listenname+"')}", String.class, null);
                ajax2.addAjaxBehaviorListener(new AjaxBehaviorListenerImpl(me2,me2));
                erledigtSBC.addClientBehavior("change", ajax2);            
                erledigt.getChildren().add(erledigtSBC);
            //add to Tabelle    
            dt.getChildren().add(erledigt);

            //Spalte Anzahl
            Column menge = new Column();
                //Name der Spalte
                menge.setHeaderText("Anzahl");
                //Spaltenbreite
                menge.setWidth("30");
                //responsive
                menge.setPriority(2);
                
                //doppelt Zelle für Input & Output
                CellEditor ceAnzahl = new CellEditor();
                    FacetTag ftinput = new FacetTag();
                    ftinput.setName("input");
                    HtmlInputText inputAnzahl = new HtmlInputText();
                    inputAnzahl.setStyle("width:100%");
                    ValueExpression inputveAnzahl = ef.createValueExpression(elc, "#{l.anzahl}", String.class);
                    inputAnzahl.setValueExpression("value", inputveAnzahl);
                    ceAnzahl.getFacets().put("input", inputAnzahl);
          
                    FacetTag ftoutput = new FacetTag();
                    ftoutput.setName("output");
                    HtmlOutputText outputAnzahl = new HtmlOutputText();
                    ValueExpression outputveAnzahl = ef.createValueExpression(elc, "#{l.anzahl}", String.class);
                    outputAnzahl.setValueExpression("value", outputveAnzahl);
                ceAnzahl.getFacets().put("output", outputAnzahl);
            menge.getChildren().add(ceAnzahl);
            //add to Tabelle
            dt.getChildren().add(menge);

            //Spalte Produktnamen
            Column produktname = new Column();
                produktname.setHeaderText("Produktname");
                produktname.setWidth("80");
                produktname.setPriority(2);

                CellEditor ceProduktname = new CellEditor();
                    FacetTag ftinputName = new FacetTag();
                    ftinputName.setName("input");
                    HtmlInputText inputProduktname = new HtmlInputText();
                    inputProduktname.setStyle("width:100%");
                    ValueExpression inputveProduktname = ef.createValueExpression(elc, "#{l.name}", String.class);
                    inputProduktname.setValueExpression("value", inputveProduktname);
                    ceProduktname.getFacets().put("input", inputProduktname);
                    
                    FacetTag ftoutputName = new FacetTag();
                    ftoutputName.setName("output");
                    HtmlOutputText outputProduktname = new HtmlOutputText();
                    ValueExpression outputveProduktname = ef.createValueExpression(elc, "#{l.name}", String.class);
                    outputProduktname.setValueExpression("value", outputveProduktname);
                ceProduktname.getFacets().put("output", outputProduktname);
                produktname.getChildren().add(ceProduktname);
            dt.getChildren().add(produktname);


            //Spalte Hinweis 
            Column hinweis = new Column();
                hinweis.setHeaderText("Hinweis");
                hinweis.setWidth("125");
                hinweis.setPriority(4);

                CellEditor ceHinweis = new CellEditor();
                    FacetTag ftinputHinweis = new FacetTag();
                    ftinputHinweis.setName("input");
                    HtmlInputText inputHinweis = new HtmlInputText();
                    inputHinweis.setStyle("width:100%");
                    ValueExpression inputveHinweis = ef.createValueExpression(elc, "#{l.kommentar}", String.class);
                    inputHinweis.setValueExpression("value", inputveHinweis);
                ceHinweis.getFacets().put("input", inputHinweis);
            
                    FacetTag ftoutputHinweis = new FacetTag();
                    ftoutputHinweis.setName("output");
                    HtmlOutputText outputHinweis = new HtmlOutputText();
                    ValueExpression outputveHinweis = ef.createValueExpression(elc, "#{l.kommentar}", String.class);
                    outputHinweis.setValueExpression("value", outputveHinweis);
                ceHinweis.getFacets().put("output", outputHinweis);
                hinweis.getChildren().add(ceHinweis);
            dt.getChildren().add(hinweis);


            //Geschäft
            Column shop = new Column();
                shop.setHeaderText("Geschäft");
                shop.setWidth("80");
                shop.setPriority(2);

                CellEditor ceShop = new CellEditor();
                    FacetTag ftinputShop = new FacetTag();
                    ftinputShop.setName("input");
                    HtmlInputText inputShop = new HtmlInputText();
                    inputShop.setStyle("width:100%");
                    ValueExpression inputveShop = ef.createValueExpression(elc, "#{l.geschaeftsname}", String.class);
                    inputShop.setValueExpression("value", inputveShop);
                ceShop.getFacets().put("input", inputShop);
                
                    FacetTag ftoutputShop = new FacetTag();
                    ftoutputShop.setName("output");
                    HtmlOutputText outputShop = new HtmlOutputText();
                    ValueExpression outputveShop = ef.createValueExpression(elc, "#{l.geschaeftsname}", String.class);
                    outputShop.setValueExpression("value", outputveShop);
                ceShop.getFacets().put("output", outputShop);
                shop.getChildren().add(ceShop);
            dt.getChildren().add(shop);


            //shopdetails
            Column shopd = new Column();
                shopd.setHeaderText("Geschäftsdetails");
                shopd.setWidth("125");
                shopd.setPriority(4);

                CellEditor ceShopd = new CellEditor();
                    FacetTag ftinputShopd = new FacetTag();
                    ftinputShopd.setName("input");
                    HtmlInputText inputShopd = new HtmlInputText();
                    inputShopd.setStyle("width:100%");
                    ValueExpression inputveShopd = ef.createValueExpression(elc, "#{l.geschaeftsdetails}", String.class);
                    inputShopd.setValueExpression("value", inputveShopd);
                ceShopd.getFacets().put("input", inputShopd);
                    FacetTag ftoutputShopd = new FacetTag();
                    ftoutputShopd.setName("output");
                    HtmlOutputText outputShopd = new HtmlOutputText();
                    ValueExpression outputveShopd = ef.createValueExpression(elc, "#{l.geschaeftsdetails}", String.class);
                    outputShopd.setValueExpression("value", outputveShopd);
                ceShopd.getFacets().put("output", outputShopd);
                shopd.getChildren().add(ceShopd);
            dt.getChildren().add(shopd);


            
            //Spalte gekauft von 
            Column gekauftvon = new Column();
                gekauftvon.setHeaderText("gekauft von");
                gekauftvon.setWidth("40");
                gekauftvon.setPriority(5);
                HtmlOutputText outputgekauftvon = new HtmlOutputText();
                ValueExpression outputvegekauftvon = ef.createValueExpression(elc, "#{l.gekauftVon.name}", String.class);
                outputgekauftvon.setValueExpression("value", outputvegekauftvon);
                gekauftvon.getChildren().add(outputgekauftvon);
            dt.getChildren().add(gekauftvon);

            //Spalte Eintrag löschen
            Column loeschen = new Column();
                loeschen.setHeaderText("Löschen?");
                loeschen.setWidth("40");             
                loeschen.setPriority(5);
                SelectBooleanCheckbox loeschenSBC = new SelectBooleanCheckbox();
                ValueExpression cve2 = ef.createValueExpression(elc, "#{l.istGeloescht}", String.class);
                loeschenSBC.setValueExpression("value", cve2);
                AjaxBehavior ajax3 = new AjaxBehavior();
                MethodExpression me3 = ef.createMethodExpression(elc, "#{shopList.onChange('"+listenname+"')}", String.class, null);
                ajax3.addAjaxBehaviorListener(new AjaxBehaviorListenerImpl(me3,me3));
                loeschenSBC.addClientBehavior("change", ajax3);            
                loeschen.getChildren().add(loeschenSBC);
            dt.getChildren().add(loeschen);
            
            //adde Tabelle zum aktuellen Fieldset
            liste.getChildren().add(dt);

            //CommandButton neuer Eintrag
            CommandButton cmd = new CommandButton();
                //value des Buttons
                cmd.setValue("Neuer Eintrag");
                //setze Update ID
                cmd.setUpdate(liste.getId());
                MethodExpression exp = ef.createMethodExpression(elc, "#{shopList.makeNewEntry("+id+",'"+listenname+"')}", String.class, null);
                cmd.setActionExpression(exp);
            liste.getChildren().add(cmd);
        }else{
                //sonst
                //Panelgrid
                HtmlPanelGrid grid = new HtmlPanelGrid(); 
                //kein Rahmen
                grid.setBorder(0);
                //eine Spalte
                grid.setColumns(1);
                //Label mit Text
                OutputLabel noEntries = new OutputLabel();
                noEntries.setValue("Keine Einträge in der Datenbank vorhanden!");
                grid.getChildren().add(noEntries);
                //Link für neuen Eintrag
                CommandLink cmd = new CommandLink();
                    //wert des Links
                    cmd.setValue("Neuer Eintrag?");
                    //setze Style
                    cmd.setStyle("color: blue;");
                    //Update
                    cmd.setUpdate(liste.getId());
                    MethodExpression exp = ef.createMethodExpression(elc, "#{shopList.makeNewEntry("+id+",'"+listenname+"')}", String.class, null);
                    cmd.setActionExpression(exp);
                grid.getChildren().add(cmd);    
                liste.getChildren().add(grid);
        }
        //ArrayList von Fieldsets mit diesem Fieldset erweitern, wegen toggeln und wegen stateless
        fliste.add(liste);
        shopPanel.getChildren().add(liste);
        shopPanel.saveTransientState(context);
        shopPanel.saveState(context);
        
        //Rückgabe des Panels
        return shopPanel;
    }
    
    /**
     * Erzeugung eines Panels, wenn keine Listen zu einem User existieren.
     * @return Panel ohne Listen
     */
    public Panel noLists() {
        FacesContext context = FacesContext.getCurrentInstance();
        //Keine LIsten keine Inhalte!
        Panel keineListe = new Panel();
        keineListe.setHeader("Keine Liste vorhanden!");
        OutputLabel nol = new OutputLabel();
        nol.setValue("Um Listen zu sehen, muss zuvor eine erstellt worden sein! Klicken Sie oben auf Listenaktionen / Liste erstellen...");
        keineListe.getChildren().add(nol);
        keineListe.saveState(context);
        //return generiertes Panel
        return keineListe;
    }
    
}
