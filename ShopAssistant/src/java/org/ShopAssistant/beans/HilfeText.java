package org.ShopAssistant.beans;

import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 * 
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class HilfeText implements Serializable{
    
    private ArrayList<String> helpaction;

    public ArrayList<String> getHelpaction() {
        return helpaction;
    }

    public void setHelpaction(ArrayList<String> helpaction) {
        this.helpaction = helpaction;
    }
    
    public HilfeText(){
        helpaction = new ArrayList();
        helpaction.add("Registrierung - Zur Registrierung klicken Sie einfach oben auf 'Registrierung' und folgen Sie dann den angegebeben Schritten.");
        helpaction.add("Passwort vergessen - Wenn Sie Ihr Passwort vergessen haben, forden Sie einfach ein Neues über 'Passwort vergessen' an.");
        helpaction.add("Mein Account - Hier können SieIhren Namen, Email und Passwort ändern.");
        helpaction.add("Einkaufsliste - Hier werden alle Listen angezeigt, die sie selbst erstellt haben, oder zu denen Sie eingeladen wurden.");
        helpaction.add("Liste erstellen - Hierüber können Sie neue Listen erstellen. Einfach Name eingeben. Fertig.");
        helpaction.add("Gruppe erstellen - Möchten Sie, dass mehrere eine Liste bearbeiten. Dann erstellen Sie doch einfach eine Gruppe.");
        helpaction.add("Einladung zur Gruppenbearbeitung - Um mehreren Personene die Bearbeitung an einer Liste möglich zu machen, laden Sie sie einfach ein.");
        helpaction.add("Austreten aus einer Gruppe - Natürlich können Sie auch aus einer Gruppe austreten.");
        helpaction.add("Gruppe löschen - Hierüber kann eine existierende Gruppe gelöscht werden. Das ist auschließlich dem Ersteller der Gruppe vorbehalten.");
        helpaction.add("Favoriten anzeigen - Hierüber können Sie sich die Favoriten anzeigen lassen. Top 10 !");
        helpaction.add("Favoriten bearbeiten/hinzufügen - Nach der Anzeige des Dialogs können sie rechts auf den Stift klicken und den Eintrag bearbeiten / hinzufügen.");
        helpaction.add("Statistik - Hier werden die Favoriten in Form eines Charts aufbereitet mit einer darunterliegenden Legende.");
    }
}
