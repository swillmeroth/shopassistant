package org.ShopAssistant.beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 * JavaBean zur Seitennavigation im ShopAssistant.
 * Liefert das Facelet, dass angezeigt werden soll.
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class PageNavigation implements Serializable{
   
    /**
     * Konstruktor.
     */
    public PageNavigation(){    
    }
    
    /**
     * Startseite
     * @return index.xhtml
     */
    public String gotoStart(){
        return "index.xhtml";
    }
    
    /**
     * Hilfeseite
     * @return hilfe.xhtml
     */
    public String gotoHilfe(){
        return "hilfe.xhtml";
    }
    
    /**
     * Listenseite
     * @return listen.xhtml
     */
    public String gotoListen(){     
        return "listen.xhtml";
    }
    
    /**
     * Statistikseite
     * @return statistik.xhtml
     */
    public String gotoStatistik(){
        return "statistik.xhtml";
    }
}
