package org.ShopAssistant.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import org.ShopAssistant.Component.ShopList;
import org.ShopAssistant.JPA.GruppenJPA;
import org.ShopAssistant.JPA.ListenJPA;
import org.ShopAssistant.JPA.UserJPA;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Entities.Listen;
import org.primefaces.context.RequestContext;


/**
 * JavaBean für die Gruppenaktivitäten
 *
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class GruppenBean implements Serializable{
    
     //Serial wegen java.io.InvalidClassException
    private static final long serialVersionUID = 1L;
    
    /**
     * Dient der Kommunikation zwischen der GruppenBean und der LoginBean
     */
    @ManagedProperty(value="#{loginLogout}")
    private LoginLogout loginLogout;
    
    /**
     * Dient der Kommunikation zwischen der GruppenBean und der ShopList
     * wo die Listen Komponenten erstellt werden
     */
    @ManagedProperty(value="#{shopList}")
    private ShopList shopList;
    
    
    //Gruppenname
    private String gruppennamen;
    
    //existiert die Gruppe?
    private boolean groupExists;
    
    //gesuchter Nutzer vorhanden, Hilfsvariable
    private String uservorhanden;
    
    //Email des USers
    private String userEmail;
    
    //Hilfsvariable, Anzeigen einer Nachricht im UI
    private boolean renderMessage;
    
    //Ausgewählte Gruppe
    private String selectedGroup;
    
    //ausgewählte Gruppe zum Löschen
    private String selecteddelGroup;
    //Platzhalter
    private boolean placeholder;
    private boolean placeholder2;
    
    //Hilfsvariable
    private boolean nutzergefunden;
    private boolean suchen;
    
    //Nutzer der eingeladen werden sll
    private Benutzer invitationUser;
    
    //Gruppe die zu löschen ist
    private Gruppen delGruppe;
    
    //Nachricht fürs UI + Hilfsvariablen
    private String delgruppemessage;
    private boolean renderdelmessage;
    private String delgruppemessage2;
    private boolean renderdelmessage2;
    private boolean rendergruppesuchenbutton;
    private boolean renderdelgruppebutton;

    
    
    /**
     * Konstruktor. Initilisierung der Hilfsvariablen
     */
    public GruppenBean(){
        this.groupExists=true;
        this.nutzergefunden=false;
        this.suchen=true;
        this.renderMessage=false; 
        this.renderdelgruppebutton=false;
        this.rendergruppesuchenbutton=true;
        this.renderdelmessage=false;
        this.renderdelmessage2=false;
        this.renderMessage=false; 
        this.placeholder=true;
    }
    
    /**
     * Platzhalter
     * @return true || false
     */
    public boolean isPlaceholder2() {
        return placeholder2;
    }

    /**
     * Setzen des Platzhalters. UI
     * @param placeholder2 
     */
    public void setPlaceholder2(boolean placeholder2) {
        this.placeholder2 = placeholder2;
    }
    
    /**
     * Platzhalter UI
     * @return true || false
     */
    public boolean isPlaceholder() {
        return placeholder;
    }

    /**
     * Setzen Platzhalter. UI
     * @param placeholder 
     */
    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }

    /**
     * Ausgewählte Gruppe, die gelöscht werden soll.
     * @return String
     */
    public String getSelecteddelGroup() {
        return selecteddelGroup;
    }

    /**
     * Setzen der Gruppe, die gelöscht werden soll.
     * @param selecteddelGroup Gruppenname als String
     */
    public void setSelecteddelGroup(String selecteddelGroup) {
        this.selecteddelGroup = selecteddelGroup;
    }

    /**
     * Gruppen Entity komplett die gelöscht werden soll.
     * @return Gruppen Entity
     */
    public Gruppen getDelGruppe() {
        return delGruppe;
    }

    /**
     * Setzen der Gruppen Entity, die gelöscht werden soll
     * @param delGruppe Gruppen Entity
     */
    public void setDelGruppe(Gruppen delGruppe) {
        this.delGruppe = delGruppe;
    }
    
    /**
     * Liefert Nachricht im Bezug auf Löschen.
     * @return String
     */
    public String getDelgruppemessage2() {
        return delgruppemessage2;
    }

    /**
     * Setzen des Inhaltes der Nachricht.
     * @param delgruppemessage2 Nachricht als String
     */
    public void setDelgruppemessage2(String delgruppemessage2) {
        this.delgruppemessage2 = delgruppemessage2;
    }
    
    /**
     * Hilfsvariable zur Anzeige der Nachricht.
     * @return true || false
     */
    public boolean isRenderdelmessage2() {
        return renderdelmessage2;
    }

    /**
     * Setzen der Hilfsvariablen, ob die Nachricht angezeigt werden soll, oder nicht.
     * @param renderdelmessage2 
     */
    public void setRenderdelmessage2(boolean renderdelmessage2) {
        this.renderdelmessage2 = renderdelmessage2;
    }
    
    /**
     * Hilfsvariable, Button wechsel
     * @return true || false
     */
    public boolean isRendergruppesuchenbutton() {
        return rendergruppesuchenbutton;
    }

    /**
     * Setzen der Hilfsvariablen zum Button Wechsel
     * @param rendergruppesuchenbutton true || false
     */
    public void setRendergruppesuchenbutton(boolean rendergruppesuchenbutton) {
        this.rendergruppesuchenbutton = rendergruppesuchenbutton;
    }

    /**
     * Hilfsvariablen zum Button Wechsel
     * @return true || false
     */
    public boolean isRenderdelgruppebutton() {
        return renderdelgruppebutton;
    }

    /**
     * Setzen der Hilfsvariablen zum Button Wechsel
     * @param renderdelgruppebutton 
     */
    public void setRenderdelgruppebutton(boolean renderdelgruppebutton) {
        this.renderdelgruppebutton = renderdelgruppebutton;
    }
 
    /**
     * Hilfsvariable zum Anzeigen einer Nachricht
     * @return true || false
     */
    public boolean isRenderdelmessage() {
        return renderdelmessage;
    }

    /**
     * Setzen der Hilfsvariable zum Anzeigen einer Nachricht.
     * @param renderdelmessage true || false
     */
    public void setRenderdelmessage(boolean renderdelmessage) {
        this.renderdelmessage = renderdelmessage;
    }

    /**
     * Liefert die Nachricht zum Löschen einer Gruppe
     * @return String
     */
    public String getDelgruppemessage() {
        return delgruppemessage;
    }

    /**
     * Setzen des Nachrichteninhaltes. (Löschen Gruppe)
     * @param delgruppemessage 
     */
    public void setDelgruppemessage(String delgruppemessage) {
        this.delgruppemessage = delgruppemessage;
    }
    
    /**
     * Liefert die Benutzer Entity, die eingeladen wird,
     * um an einer Liste mitzuwirken. Dabei wird er in die zuvor anegegebene+
     * Gruppe mit aufgenommen.
     * @return Benutzer Entity
     */
    public Benutzer getInvitationUser() {
        return invitationUser;
    }

    /**
     * Setzen des Benutzers, der eingeladen werden soll, um gruppenmäßig 
     * mit an Listen zu arbeiten.
     * @param invitationUser Benutzer Entity
     */
    public void setInvitationUser(Benutzer invitationUser) {
        this.invitationUser = invitationUser;
    }

    /**
     * Hilfsvariable zu Suchen
     * @return true || false
     */
    public boolean isSuchen() {
        return suchen;
    }

    /**
     * Setzen der Hilfsvariable. 
     * @param suchen true || false
     */
    public void setSuchen(boolean suchen) {
        this.suchen = suchen;
    }

    /**
     * Hilfsvariable, Nutzer gefunden
     * @return true || false
     */
    public boolean isNutzergefunden() {
        return nutzergefunden;
    }

    /**
     * Setzen der Hilfsvariable, Nutzer gefunden
     * @param nutzergefunden true || false
     */
    public void setNutzergefunden(boolean nutzergefunden) {
        this.nutzergefunden = nutzergefunden;
    }

    /**
     * DLiefert Gruppennamen, die zuvor selektiert wurde
     * @return String
     */
    public String getSelectedGroup() {
        return selectedGroup;
    }

    /**
     * Setzen der selektierten Gruppe
     * @param selectedGroup String
     */
    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    /**
     * Hilfsvariable zum Anzeigen einer Nachricht
     * @return true || false
     */
    public boolean isRenderMessage() {
        return renderMessage;
    }

    /**
     * Setzen der Hilfsvariablen
     * @param renderMessage true || false
     */
    public void setRenderMessage(boolean renderMessage) {
        this.renderMessage = renderMessage;
    }

    /**
     * Liefert Mail des USers
     * @return String
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Setzen der UserMail 
     * @param userEmail Emailadresse als String
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Hilfsvariable
     * @return String
     */
    public String getUservorhanden() {
        return uservorhanden;
    }

    /**
     * Setzen der Hilfsvariablen
     * @param uservorhanden String
     */
    public void setUservorhanden(String uservorhanden) {
        this.uservorhanden = uservorhanden;
    }
    
    /**
     * Liefert die Instanz/Referenz zur ShopList Bean
     * Dient der Kommunikation und Zugriff auf die Methoden in der ManagedBean
     * ShopList.
     * @return ManagedBean ShopList
     */
    public ShopList getShopList() {
        return shopList;
    }

    /**
     * Setter Methode im Hinblick auf die Kommunikation
     * Dient der Kommunikation und Zugriff auf die Methoden in der ManagedBean
     * ShopList.
     * @param shopList Instanz/Referenz auf die ManagedBean ShopList
     */
    public void setShopList(ShopList shopList) {
        this.shopList = shopList;
    }    

    /**
     * Hilfsvariable, ist Gruppe existent?
     * @return true || false
     */
    public boolean isGroupExists() {
        return groupExists;
    }

    /**
     * Setzen der Hilfsvariablen
     * @param groupExists true @@ false
     */
    public void setGroupExists(boolean groupExists) {
        this.groupExists = groupExists;
    }

    /**
     * Liefert die Instanz/Referenz zur LoginLogout Bean
     * Dient der Kommunikation und Zugriff auf die Methoden in der ManagedBean
     * LoginLogout.
     * @return ManagedBean LoginLogout
     */
    public LoginLogout getLoginLogout() {
        return loginLogout;
    }

    /**
     * Setter Methode im Hinblick auf die Kommunikation
     * Dient der Kommunikation und Zugriff auf die Methoden in der ManagedBean
     * LoginLogout.
     * @param loginLogout Instanz/Referenz auf die ManagedBean LoginLogout
     */
    public void setLoginLogout(LoginLogout loginLogout) {
        this.loginLogout = loginLogout;
    }
    
    /**
     * Liefert den Gruppennamen.
     * @return Gruppenname als String.
     */
    public String getGruppennamen() {
        return gruppennamen;
    }

    /**
     * Setzen des Gruppennamens
     * @param gruppennamen Name der Gruppe als String
     */
    public void setGruppennamen(String gruppennamen) {
        this.gruppennamen = gruppennamen;
    }
    
    /**
     * Methode zum Erstellen einer Gruppe.
     * Setzen sämtliche Hilfsvariablen.
     */
    public void createGruppe()
    {
        //neue Gruppe
        Gruppen g = new Gruppen();
        
        //setze Namen der Gruppe
        g.setName(gruppennamen);
        
        //hole den eingeloggten Nutzer
        Benutzer nut = loginLogout.getRegistrierterNutzer();
        
        //erstelle Gruppe
        GruppenJPA gdi = new GruppenJPA();
        gdi.erstelleGruppe(nut, g);
                
        loginLogout.setGruppenvorhanden(false);
        shopList.generateGruppenDropDown();
        
        //Update von Komponeten im UI
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('gruppenDialog').hide()");
        context.update("formy:za");
        context.update("selectform");
        context.update("exitform");
        context.update("groupinform");
        context.update("delform");
        
        //Aktualisierung der Listen
        shopList.makeZettel();
    }
    
    /**
     * Methode um aus einer Gruppe auszutreten.
     * Dabei wird bei der Auswahl des Namens aus der Drop Down Liste,
     * anschließend das komplette Gruppen Objekt / Entity gesucht
     * und entsprechend in der Liste auf null gesetzt.
     */
    public void exitGruppe(){
        
        RequestContext con = RequestContext.getCurrentInstance();
        Gruppen exitGruppe = null;
       
        //Suche Gruppe zum Löschen zur Eindeutigkeit über die ID
        for(int i=0;i<loginLogout.getGlisten().size();i++){
            if(loginLogout.getGlisten().get(i).getName().equals(shopList.getItem()))
            {
                exitGruppe=loginLogout.getGlisten().get(i);
            }
        }

        //Überprüfung der ID und Setze dann Gruppe in Liste auf null
        List<Listen> l = new ArrayList();
        l.addAll(loginLogout.getUsersListen());
        for(int i=0; i<l.size();i++){
            if(l.get(i).getGruppe()!=null){
                if(l.get(i).getGruppe().getId().equals(exitGruppe.getId())){
                    l.get(i).setGruppe(null);
                }    
            }
        }

        //Persist Listen
        ListenJPA ldi = new ListenJPA();
        ldi.persistChangedListen(l);
       
        for(int i=0;i<shopList.getGruppenlistest().size();i++){
            if(shopList.gruppenlistest.get(i).equals(shopList.getItem())){
                shopList.getGruppenlistest().remove(i);
                shopList.setItem("");
            }
        }
        
        //hole registrierten und eingeloggten Nutzer
        Benutzer nutzer = loginLogout.getRegistrierterNutzer();
        
        //Update aller Listen
        GruppenJPA gdi = new GruppenJPA();
        gdi.removeUservonGruppe(nutzer, exitGruppe);
        loginLogout.setUsersListen(ldi.getListenByUserID(loginLogout.getRegistrierterNutzer().getId()));
        
        //Generier die Drop Down Liste zur Auswahl einer Gruppe neu
        shopList.generateGruppenDropDown();
        
        //Aktualisierung der Zettel
        shopList.makeZettel();
        
        //Update von Komponeten im UI
        con.execute("PF('gruppenexitDialog').hide()");
        con.update("selectform");
        con.update("exitform");
        con.update("groupinform");
        con.update("delform");
    }
    
    /**
     * Lösche Gruppe.Holt aus dem selektierten Namen des Drop Down Menüs, die
     * komplette Entity und löscht sie.
     */
    public void removeGruppe(){
        this.setRenderdelmessage(false);
        this.setRenderdelgruppebutton(false);
        this.setRendergruppesuchenbutton(true);
        this.setRenderdelmessage2(false);
        
        
        //erstelle aus String des DropDowns wieder ein GruppenObjekt/Entity
        Gruppen g=null;
        for(int i=0; i<loginLogout.getGlisten().size();i++){
            if(loginLogout.getGlisten().get(i).getName().equals(this.getSelecteddelGroup())){
                g=loginLogout.getGlisten().get(i);
            }
        }
        
        //Suchen ob die Gruppe überhaupt existiert
        GruppenJPA gdi = new GruppenJPA();
        int id=gdi.findGruppeByGruppenNamen(g);
        
        //entsprechende Nachrichtenausgabe und Steuerung der UI Komponenten
        if(id!=-1){
            this.setDelGruppe(g);
            if(this.getDelGruppe().getGruppenErsteller().getName().equals(loginLogout.getRegistrierterNutzer().getName())){
                this.setDelgruppemessage("Wollen Sie die Gruppe wirklich löschen?");
                this.setDelgruppemessage2("\"Hinweis: Alle Nutzer die zu der Gruppe zugehören, haben automatisch keinen Zugriff mehr auf diese Gruppe.");
                this.setPlaceholder2(false);
                this.setRenderdelmessage(true);
                this.setRenderdelmessage2(true);
                this.setRenderdelgruppebutton(true);
                this.setRendergruppesuchenbutton(false);
            }else{
                this.setDelgruppemessage("Sie sind zum Löschen dieser Gruppe nicht autorisiert!");
                this.setRenderdelmessage(true);
                this.setPlaceholder2(false);
                this.setRenderdelgruppebutton(false);
            }
        }else{
                this.setDelgruppemessage("Gruppe nicht gefunden!");
                this.setRenderdelmessage(true);
                this.setPlaceholder2(false);
                this.setRenderdelgruppebutton(false);
        }
    }
    
    
    /**
     * Methode zum endgültigen Löschen der Gruppe, sofern sie vorher gefunden wurde.
     */
    public void deleteandpersistGruppe(){
        
            RequestContext ctx = RequestContext.getCurrentInstance();
        
            //Gruppe löschen
            GruppenJPA gdi = new GruppenJPA();
            gdi.loescheGruppe(this.getDelGruppe(), loginLogout.getRegistrierterNutzer());
        
            //Update des Drop Down MEnüs mit den Gruppennamen
            shopList.generateGruppenDropDown();
            
            //Aktualisierung der Zettel
            shopList.makeZettel();
                        
            //Update der UI Komponenten und Hilfsvariablen
            ctx.execute("PF('gruppendelDialog').hide()");
            ctx.update("selectform");
            ctx.update("exitform");
            ctx.update("groupinform");
            ctx.update("delform");
            ctx.update("formy:za");
            this.setRenderdelmessage(false);
            this.setRenderdelgruppebutton(false);
            this.setRendergruppesuchenbutton(true);
            this.setRenderdelmessage2(false);
      
    }
    
    /**
     * Methode, die aufgerufen wird, wenn ein Nutzer eingeladen werden soll.
     * Der aktuell eingeloggte Nutzer, kann sich dabei nicht selber einladen.
     */
    public void inviteUser(){
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        UIViewRoot root = ctx.getViewRoot();
        this.setRenderMessage(false);
        this.setSuchen(true);
        this.setNutzergefunden(false);
        
        //Setzen des User Entity, die eingeladen werden soll
        
        UserJPA udi = new UserJPA();
        if(!this.getUserEmail().equals("")){
            this.setInvitationUser(udi.getUser(this.getUserEmail()));
        }
        
        //dabei kann er sich selbst nicht einladen
        if(this.getUserEmail().equals(loginLogout.getRegistrierterNutzer().getEMail())){
            this.setInvitationUser(null);
        }
        
        //solange nicht Null, Anzeigesteuerung und setzen von Hilfsvariablen
        //und Nachrichten
        if(this.getInvitationUser()==null){
            this.setSuchen(true);
            this.setNutzergefunden(false);
            if(this.getUserEmail().equals("")){
                this.setUservorhanden("Bitte eine gültige Emailadresse eingeben!");
            }else{
                if(this.getUserEmail().equals(loginLogout.getRegistrierterNutzer().getEMail())){
                    this.setUservorhanden("Das ist ihre Email. Sie können sich nicht selbst einladen!");
                }else{
                    this.setUservorhanden("Benutzer mit " + this.getUserEmail() +" nicht gefunden.");
                }
            }
            root.findComponent("groupinform:outmessage").getAttributes().put("style", "color: red;");
            this.setRenderMessage(true);
            this.setPlaceholder(false);
        }else{
            //get group
            this.setSuchen(false);
            this.setNutzergefunden(true);
            this.setUservorhanden("Benutzer mit " + this.getUserEmail() +" gefunden.");
            root.findComponent("groupinform:outmessage").getAttributes().put("style", "color: green;");
            this.setRenderMessage(true);
            this.setPlaceholder(false);
        }     
    }
    
    /**
     * Methode zum Setzen des Users zu Benutzergruppen.
     * Persistierung.
     */
    public void addandpersistUser(){
        Gruppen g = null;
        for(int i=0; i<loginLogout.getGlisten().size();i++){
            if(loginLogout.getGlisten().get(i).getName().equals(this.getSelectedGroup())){
                g=loginLogout.getGlisten().get(i);
            }
        }
        
        //Persistierung
        GruppenJPA gdi = new GruppenJPA();
        gdi.addUsertoBenutzergruppen(this.getInvitationUser(), g);
    
        //Setzen von Hilfsvariablen und Dialog schließen.
        this.setUservorhanden("");
        RequestContext ctx = RequestContext.getCurrentInstance();
        ctx.execute("PF('gruppeninDialog').hide()");
    }
    
   
    /**
     * Dialog abbrechen.Zurücksetzen der Hilfsvariablen.
     */
    public void abbortDialog(){
        RequestContext ctx = RequestContext.getCurrentInstance();
        ctx.execute("PF('gruppendelDialog').hide()");
        this.setUservorhanden("");
        this.setRenderdelmessage(false);
        this.setRenderdelgruppebutton(false);
        this.setRendergruppesuchenbutton(true);
        this.setRenderdelmessage2(false);
        this.setPlaceholder(true);
        this.setRenderMessage(false);
        ctx.update("delform");
    }
}
