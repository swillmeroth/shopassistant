package org.ShopAssistant.beans;



import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;

/**
 * JavaBean zum Aufruf von definierten Dialogen
 * @author Andre
 */
@ManagedBean
public class DialogBean {
    RequestContext context;
    
    /**
     * Konstruktor.
     * Definiert den RequestContext (Primefaces)
     */
    public DialogBean(){
        context = RequestContext.getCurrentInstance();
    }
    
    /**
     * Öffnet den Registrierungsdialog
     */
    public void showRegistrationDialog(){
        context.execute("PF('registrationDialog').show()");
    }
    
    /**
     * Schließt den Regsitrierungsdialog
     */
    public void hideRegistrationDialog(){
        context.execute("PF('registrationDialog').hide()");
    }
    
    /**
     * Öfnet den Dialog zur Eingabe des Namens einer neuen Liste
     */
    public void showListnamenDialog(){
        context.execute("PF('listnamenDialog').show()");
    }
    
    /**
     * Schließen des Listennamen Dialogs
     */
    public void hideListnamenDialog(){
        context.execute("PF('listnamenDialog').hide()");
    }

    /**
     * Dialog zum Löschen einer Liste
     */
    public void showListLoeschenDialog(){
        context.execute("PF('listeloeschenDialog').show()");
    }
    
    /**
     * Schließen des Dialogs zum Löschen einer Liste
     */
    public void hideListLoeschenDialog(){
        context.execute("PF('listeloeschenDialog').hide()");
    }
    
    /**
     * Öffnet den Dialog zu mein Konto
     */
    public void showMeinAccountDialog(){
        context.execute("PF('myaccountDialog').show()");
    }
    
    /**
     * Schließen des Dialogs zu Mein Konto
     */
    public void hideMeinAccountDialog(){
        context.execute("PF('myaccountDialog').hide()");
    }

    /**
     * Öffnen des Dialogs mit den Favoriten
     */
    public void showMeineFavoritenDialog(){
        context.execute("PF('favoritenDialog').show()");
    }
    
    /**
     * Schließen des Favoritendialogs
     */
    public void hideMeineFavoritenDialog(){
        context.execute("PF('favoritenDialog').hide()");
    }
    
    /**
     * Öffnen des Dialogs Meine Gruppen
     */
    public void showMeineGruppenDialog(){
        context.execute("PF('gruppenDialog').show()");
    }
    
    /**
     * Schließen des Dialogs Meine Gruppen
     */
    public void hideMeineGruppenDialog(){
        context.execute("PF('gruppenDialog').hide()");
    }
    
    /**
     * Dialog öffnen zum Austreten aus einer Gruppe
     */
    public void showMeineGruppenExitDialog(){
        context.execute("PF('gruppenexitDialog').show()");
    }
    
    /**
     * Dialog schließen zum Austreten aus einer Gruppe
     */
    public void hideMeineGruppenExitDialog(){
        context.execute("PF('gruppenexitDialog').hide()");
    }
    
    /**
     * Dialog zum Einladen in eine Gruppe öffnen.
     */
    public void showMeineGruppeninDialog(){
        context.execute("PF('gruppeninDialog').show()");
    }
    
     /**
     * Dialog zum Einladen in eine Gruppe schließen.
     */
    public void hideMeineGruppeninDialog(){
        context.execute("PF('gruppeninDialog').hide()");
    }
    
    /**
     * Dialog zum Löschen in eine Gruppe öffnen.
    */
    public void showMeineDeleteGruppeninDialog(){
        context.execute("PF('gruppendelDialog').show()");
    }
    
    /**
     * Dialog zum Löschen in eine Gruppe schließen.
    */
    public void hideDeleteGruppeninDialog(){
        context.execute("PF('gruppendelDialog').hide()");
    }
    
    /**
     * Passwort vergessen Dialog öffnen.
     */
    public void showMeinePassVergessenDialog(){
        context.execute("PF('passwortvergesssenDialog').show()");
    }
    
    /**
     * Passwort vergessen Dialog schließen.
     */
    public void hideMeinePassVergessenDialog(){
        context.execute("PF('passwortvergesssenDialog').hide()");
    }
}
