package org.ShopAssistant.beans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.ShopAssistant.JPA.UserJPA;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.LogicClasses.MailSender;
import org.primefaces.context.RequestContext;

/**
 * JavaBean für Useraktionen im ShopAssistant
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable{

     //Serial wegen java.io.InvalidClassException
    private static final long serialVersionUID = 1L;
    
    //Name
    private String name;
    //Email
    private String email;
    //Pass1
    private String passwort1;
    //Pass2
    private String passwort2;
    //Name zur Passwortanforderung
    private String passname;
    
    //Hilfsvariable und Rendering von Nachrichten
    private String passmessage;
    private boolean renderpassmessage;

    /**
     * Konstruktor und Setzen von Hilfsvariablen
     */
    public UserBean(){
        this.passmessage="";
        this.renderpassmessage=false;
    }
    
    /**
     * Liefert Passwort Nachricht.
     * @return String
     */
    public String getPassmessage() {
         return passmessage;
    }

    /**
     * Setzen der Passwortnachricht
     * @param passmessage String
     */
    public void setPassmessage(String passmessage) {
        this.passmessage = passmessage;
    }

    /**
     * Hilfsvariable zur Anzeige der Nachricht
     * @return true || false
     */
    public boolean isRenderpassmessage() {
        return renderpassmessage;
    }

    /**
     * Setzen der Hilfsvariable
     * @param renderpassmessage true || false 
     */
    public void setRenderpassmessage(boolean renderpassmessage) {
        this.renderpassmessage = renderpassmessage;
    }

    /**
     * Liefert Passname zur Passwortanforderung
     * @return String
     */
    public String getPassname() {
        return passname;
    }

    /**
     * Setzen Passname zur Passwortanforderung
     * @param passname String 
     */
    public void setPassname(String passname) {
        this.passname = passname;
    }

    /**
     * Liefert Namen
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setzen des Namens
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Liefert Emailadresse
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setzen der Emailadresse
     * @param email String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Liefert passwort1
     * @return String
     */
    public String getPasswort1() {
        return passwort1;
    }

    /**
     * Setzen des Passworts1
     * @param passwort1 String
     */
    public void setPasswort1(String passwort1) {
        this.passwort1 = passwort1;
    }
    
    /**
     * Liefert Passwort2
     * @return String
     */
    public String getPasswort2() {
        return passwort2;
    }

    /**
     * Setzen des Passworts2
     * @param passwort2 String
     */
    public void setPasswort2(String passwort2) {
        this.passwort2 = passwort2;
    }
    
    
    /**
     * Registrierung eines Nutzers
     */
    public void registerUserData(){
        
        RequestContext context = RequestContext.getCurrentInstance();
        Boolean ok;
        //Nutzer der registriert werden soll, sofern er noch nichjt existiert
        
        Benutzer nutzer=new Benutzer();
        //Name
        nutzer.setName(this.name);
        //Passwort
        nutzer.setPasswort(this.passwort1);
        //Emailadresse
        nutzer.setEMail(this.email);
        
        //Persist, ok or not ok
        //Ausgabe einer entsprechenden Nachricht!
        UserJPA register = new UserJPA();
        ok=register.registerUser(nutzer);
        if(ok){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Willkommen!", "Sie haben sich erfolgreich registriert."));
            this.name="";
            this.email="";
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fehler bei der Registrierung!", "Email oder Name bereits vorhanden."));
            this.name="";
            this.email="";
        }
        //Dialog schließen
        context.execute("PF('registrationDialog').hide()");        
    }
    
    /**
     * Methode zum Anfordern eines Passwortes
     */
    public void passwortAnfordern(){
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext ctx = RequestContext.getCurrentInstance();
        boolean ok;
        
        //Passwortanforderung mit dem eingegeben Namen
        UserJPA uJ = new UserJPA();
        ok=uJ.checkUser(this.getPassname());
        
        //Ausgabe einer UI Nachricht
        if(ok){
            ctx.execute("PF('passwortvergesssenDialog').hide()");
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sie haben Post!", ""));
        }else{
            this.setPassmessage("User nicht gefunden");
            this.setRenderpassmessage(true);
        }
    }
    
    /**
     * Abbrechmethode. Zurücksetzen der Hilfsvariablen.
     */
    public void abbort(){
        this.setPassmessage("");
        this.setRenderpassmessage(false);
    }
}
