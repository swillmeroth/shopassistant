package org.ShopAssistant.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.ShopAssistant.JPA.GruppenJPA;
import org.ShopAssistant.JPA.ListenJPA;
import org.ShopAssistant.JPA.UserJPA;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Entities.Listen;
import org.primefaces.context.RequestContext;

/**
 * JavaBean zum Login und Logout in der Webapplikation ShopAssistant.
 * @author Andre
 */
@SessionScoped
@ManagedBean(name="loginLogout")
public class LoginLogout implements Serializable{
    
    //Serial wegen java.io.InvalidClassException
    private static final long serialVersionUID = 1L;
    
    //ID des eingeloggten Nutzers
    private int id;
    //Name des Users
    private String name;
    //Email des Users
    private String email;
    //Passwort des Users
    private String passwort;  
   
    //-----------------------------------------------
    //eingeloggte/registrierter Nutzer
    private Benutzer registrierterNutzer;
    
    //userslisten des nutzers
    private List<Listen> usersListen;
    
    //gruppenlisten des nutzers
    private List<Gruppen> glisten;
    
    //-----------------------------------------------
    
    //Hilfsvariablen
    private boolean logged;
    private String site;
    private String site2;
    private boolean gruppenvorhanden;
    private boolean gruppennichtvorhanden;
    private boolean renderListenOptionen;
    private boolean renderGruppenOptionen;
    private boolean renderZettel;
    private boolean renderloginerrormessage;
    private boolean renderStatistik;
    private String loginerrormessage;
   

    /**
     * Konstruktor. Setzen der Hilfsvariablen. Setzen des StartFacelets für Header.
     */
    public LoginLogout(){
        registrierterNutzer=new Benutzer();
        this.glisten=new ArrayList();
        this.site2="../LoginLogoutPanel/loginPanel.xhtml";
        this.renderGruppenOptionen=false;
        this.renderListenOptionen=false;
        this.renderZettel=false;
        this.renderloginerrormessage=false;
        this.renderStatistik=false;
   
    }
    
    /**
     * Lifert Gruppe vorhanden?
     * @return true || false
     */
    public boolean isGruppennichtvorhanden() {
        return gruppennichtvorhanden;
    }

    /**
     * Setzen der Hilfsvariable.
     * @param gruppennichtvorhanden  boolean
     */
    public void setGruppennichtvorhanden(boolean gruppennichtvorhanden) {
        this.gruppennichtvorhanden = gruppennichtvorhanden;
    }
    
    /**
     * Hilfsvariable zum Anzeigen der Statistik
     * @return true || false
     */
    public boolean isRenderStatistik() {
        return renderStatistik;
    }

    /**
     * Setzen der Hilfsvariablen im Hinblick auf die Statistik.
     * @param renderStatistik true || false
     */
    public void setRenderStatistik(boolean renderStatistik) {
        this.renderStatistik = renderStatistik;
    }
    
    /**
     * Hilfsvairable, Anzeigen bei Error Login
     * @return 
     */
    public boolean isRenderloginerrormessage() {
        return renderloginerrormessage;
    }

    /**
     * Setzen der Hilfsvariablen
     * @param renderloginerrormessage  true|| false
     */
    public void setRenderloginerrormessage(boolean renderloginerrormessage) {
        this.renderloginerrormessage = renderloginerrormessage;
    }

    /**
     * Liefert die Login Error Nachricht
     * @return String
     */
    public String getLoginerrormessage() {
        return loginerrormessage;
    }

    /** 
     * Setzen der Login Error Nachricht
     * @param loginerrormessage Erro Nachricht als String
     */
    public void setLoginerrormessage(String loginerrormessage) {
        this.loginerrormessage = loginerrormessage;
    }

    /**
     * Hilfsvariable zum Anzeigen der Zettel
     * @return true || false
     */
    public boolean isRenderZettel() {
        return renderZettel;
    }

    /**
     * Setzen der Zettel render Hilfsvariablen.
     * @param renderZettel true || false
     */
    public void setRenderZettel(boolean renderZettel) {
        this.renderZettel = renderZettel;
    }

    /**
     * Hilfsvariable zum Anzeigen von Listenoptionen.
     * @return true || false
     */
    public boolean isRenderListenOptionen() {
        return renderListenOptionen;
    }

    /**
     * Setzen der Hilfsvariablen zum Anzeigen von Listenoptionen.
     * @param renderListenOptionen 
     */
    public void setRenderListenOptionen(boolean renderListenOptionen) {
        this.renderListenOptionen = renderListenOptionen;
    }

    /**
     * Hilfsvariablen zum Anzeigen von Gruppenoptionen.
     * @return true ||false
     */
    public boolean isRenderGruppenOptionen() {
        return renderGruppenOptionen;
    }

    /**
     * Setzen der Hilfsvariablen zum Anzaiegn von Gruppenoptionen
     * @param renderGruppenOptionen 
     */
    public void setRenderGruppenOptionen(boolean renderGruppenOptionen) {
        this.renderGruppenOptionen = renderGruppenOptionen;
    }
    
    /**
     * Hilfsvariable Gruppe vorhanden?
     * @return true || false
     */
    public boolean isGruppenvorhanden() {
        return gruppenvorhanden;
    }

    /**
     * Setzen der Hilfsvariable Gruppe vorhanden.
     * @param gruppenvorhanden true || false
     */
    public void setGruppenvorhanden(boolean gruppenvorhanden) {
        this.gruppenvorhanden = gruppenvorhanden;
    }

    /** 
     * Liefert die Liste an Gruppen zum eingeloggten Nutzer.
     * @return Liste von Gruppen
     */
    public List<Gruppen> getGlisten() {
        return glisten;
    }

    /**
     * Setzen der Liste von Gruppen
     * @param glisten Liste von Gruppen
     */
    public void setGlisten(List<Gruppen> glisten) {
        this.glisten = glisten;
    }

    /**
     * Liefert die komplette Entity des eingeloggten Nutzers
     * @return Benutzer Entity
     */
    public Benutzer getRegistrierterNutzer() {
        return registrierterNutzer;
    }

    /**
     * Setzen des Benutzers, sofern er existiert.
     * @param registrierterNutzer Benutzer Entity
     */
    public void setRegistrierterNutzer(Benutzer registrierterNutzer) {
        this.registrierterNutzer = registrierterNutzer;
    }
    
    /**
     * Liefert ID des Nutzers
     * @return Integer
     */
    public int getId() {
        return id;
    }

    /** 
     * Setzen der ID des Nutzers
     * @param id Integer
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * /Liefert die Listen des eingeloggten Nutzers
     * @return Liste von Listen
     */
    public List<Listen> getUsersListen() {
        return usersListen;
    }

    /** 
     * Setzen der Liste an Listen des eingeloggten Nutzers
     * @param usersListen Liste von Listen
     */
    public void setUsersListen(List<Listen> usersListen) {
        this.usersListen = usersListen;
    }
    
    /**
     * Liefert die Seite zum Header Austausch
     * @return String, in Form einer Zugriffs auf eine Facelet
     */
    public String getSite2() {
        return site2;
    }

    /**
     * Setzen des Headersfacelets.
     * @param site2 Facelet
     */
    public void setSite2(String site2) {
        this.site2 = site2;
    }
    
    /**
     * Liefert Header Facelet
     * @return String
     */
    public String getSite(){
        return site;
    }

    /**
     * Setzen des Header facelets
     * @param site String
     */
    public void setSite(String site) {
        this.site = site;
    }
    
    /**
     * Hilfsvariable eingeloggt oder nicht?
     * @return true || false
     */
    public boolean isLogged() {
        return logged;
    }

    /**
     * Setzen der Hilfsvariable eingeloggt oder nicht
     * @param logged true || false
     */
    public void setLogged(boolean logged) {
        this.logged = logged;
    }
    
    /**
     * Liefert den Namen.
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setzen des Namens.
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Setzen der Emailadresse.
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setzen der Emailadresse.
     * @param email Sting
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /** 
     * Liefert das Passwort
     * @return String
     */
    public String getPasswort() {
        return passwort;
    }

    /**
     * Setzen des Passwortes
     * @param passwort String
     */
    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }
    
    /**
     * Methode die überprüft, ob der User existent ist
     *  wenn ja: einloggen möglich: Panel mit Anrede einblenden
     *  wenn nein: Nachricht ausgeben
     * @return  String
     */
    public String login(){
        RequestContext context = RequestContext.getCurrentInstance();
        
        //Anlegen eines Nutzer, der sich einloggen möchte
        Benutzer nutzerLogin = new Benutzer();
        //Name
        nutzerLogin.setName(this.getName());
        //Email
        nutzerLogin.setEMail(this.getEmail());
        //Passwort
        nutzerLogin.setPasswort(this.getPasswort());
        
        //Connection zur Datenbank
        //User
        UserJPA login = new UserJPA();
        //Listen
        ListenJPA l = new ListenJPA();
        //Gruppen
        GruppenJPA g = new GruppenJPA();
        
        //wenn User existiert, 
        Benutzer nutzermitID = login.loginUser(nutzerLogin);
        //setzen des registrierten Nutzers
        this.setRegistrierterNutzer(nutzermitID);
        
        //Hilfsvariablen setzen und Header Austausch, sofern
        //der Nutzer existiert
        if(this.getRegistrierterNutzer()==null){
            
            //eingeloggt nein
            this.setLogged(false);
            
            //Error Nachricht + Anzeige
            this.setLoginerrormessage("Anmeldedaten falsch!");
            this.setRenderloginerrormessage(true);
            
            //Header bleibt bei Login
            setSite2("../LoginLogoutPanel/loginPanel.xhtml");
        }else{
            //eingeloggt ja + Setzen von Hilfsvariablen
            this.setLogged(true);
            this.setRenderGruppenOptionen(true);
            this.setRenderListenOptionen(true);
            this.setRenderZettel(true);
            this.setLoginerrormessage("");
            this.setRenderloginerrormessage(false);
            this.setRenderStatistik(true);
            //setze namen
            this.setName(registrierterNutzer.getName());
            //setze mail
            this.setEmail(registrierterNutzer.getEMail());
            //setze id
            this.setId(registrierterNutzer.getId());
            //Header wechsel
            this.setSite2("../LoginLogoutPanel/loggedPanel.xhtml");
            //uipdate Header
            context.update("panel2");
            //setze Userslisten
            this.setUsersListen(l.getListenByUserID(this.getId()));
            //setze Gruppenlisten
            this.setGlisten(g.getByUserID(this.getRegistrierterNutzer().getId()));
            if(this.getGlisten().size()>0){
                this.gruppenvorhanden=true;
                this.gruppennichtvorhanden=false;
                
            }else{
                this.gruppenvorhanden=false;
                this.gruppennichtvorhanden=true;
            }
            for(int i=0; i<this.glisten.size();i++){
                System.out.println("Nutzergruppen: " + glisten.get(i).getName());
            }
            //update von UI Komponenten
            context.update("log");
            context.update(":exitform");
            context.update(":selectform");
        }
       //Umleitung zur Startseite
       return "index.xhtml?faces-redirect=true";
    }
    
    /**
     * Methode für Logout. Header Austausch.
     * @return String
     */
    public String logout(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestContext context = RequestContext.getCurrentInstance();
        this.setSite2("../LoginLogoutPanel/loginPanel.xhtml");
        this.setRenderGruppenOptionen(true);
        this.setRenderListenOptionen(true);
        this.setRenderZettel(false);
        this.setRenderStatistik(false);
        context.update("panel2");
        ctx.getExternalContext().invalidateSession();
        return "index.xhtml?faces-redirect=true";
    }
}
