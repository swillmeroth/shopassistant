package org.ShopAssistant.beans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.ShopAssistant.JPA.UserJPA;
import org.primefaces.context.RequestContext;

/**
 * JavaBean für Mein Konto Dialog im ShopAssistant.
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class MeinAccountBean implements Serializable{
    
    
    //Serial wegen java.io.InvalidClassException
    private static final long serialVersionUID = 1L;
    //Kommunikation mit der ManagedBean LoginLogout
    @ManagedProperty(value="#{loginLogout}")
    private LoginLogout loginLogout;
    
    //Hilfsvariablen
    private String tempName;
    private String tempEmail;
    private String name;
    private String passwort1;
    private String passwort2;
    private String passmessage; 
    private String namemessage; 
    private String emailmessage; 
    private boolean nutzernamevalueoutput;
    private boolean nutzernamevalueinput;
    private boolean nutzeremailvalueoutput;
    private boolean nutzeremailvalueinput;
    private boolean nutzerpasswortvalueinput;
    private boolean aenderbuttonname;
    private boolean aenderbuttonsname;
    private boolean aenderbuttonsnamebestatetigen;
    private boolean aenderbuttonpasswort;
    private boolean aenderbuttonemail;
    private boolean aenderbuttonsemail;
    private boolean rendernamemessage;
    private boolean renderemailmessage;
    private boolean renderpassmessage;
    
    /**
     * Konstruktor. Initialisierung der Hilfsvariablen.
     */
    public MeinAccountBean(){
        this.nutzernamevalueoutput=true;
        this.nutzernamevalueinput=false;
        this.nutzeremailvalueoutput=true;
        this.nutzeremailvalueinput=false;
        this.nutzerpasswortvalueinput=false;
        this.aenderbuttonname=true;
        this.aenderbuttonpasswort=true;
        this.aenderbuttonemail=true;
        this.aenderbuttonsname=false;
        this.aenderbuttonsemail=false;
        this.aenderbuttonsnamebestatetigen=false;
        this.renderemailmessage=false;
        this.rendernamemessage=false;
        this.renderpassmessage=false;
        this.passmessage="";
        this.emailmessage="";
        this.namemessage="";
    }
    
    /**
     * Liofert die temporäre Emailadresse.
     * @return String
     */
    public String getTempEmail() {
        return tempEmail;
    }

    /**
     * Setzen der temporären Emailadresse.
     * @param tempEmail String
     */
    public void setTempEmail(String tempEmail) {
        this.tempEmail = tempEmail;
    }
    
    /**
     * Liefert den temporären Namen
     * @return String
     */
    public String getTempName() {
        return tempName;
    }

    /**
     * Setzen des temporären Namen
     * @param tempName String
     */
    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    /**
     * Liefert Passwort 1
     * @return String
     */
    public String getPasswort1() {
        return passwort1;
    }

    /**
     * Setzen Passwort1
     * @param passwort1 String 
     */
    public void setPasswort1(String passwort1) {
        this.passwort1 = passwort1;
    }

    /**
     * Liefert Passwort2
     * @return String
     */
    public String getPasswort2() {
        return passwort2;
    }

    /**
     * Setzen Passwort2
     * @param passwort2 String 
     */
    public void setPasswort2(String passwort2) {
        this.passwort2 = passwort2;
    }
    
    /**
     * Hilfsvariable zum Anzeigen einer Nachricht
     * @return true || false
     */
    public boolean isRendernamemessage() {
        return rendernamemessage;
    }
    
    /**
     * Setzen der Hilfsnachricht
     * @param rendernamemessage true || false
     */
    public void setRendernamemessage(boolean rendernamemessage) {
        this.rendernamemessage = rendernamemessage;
    }

    /**
     * Liefert Nachricht
     * @return String
     */
    public String getNamemessage() {
        return namemessage;
    }

    /** 
     * Setzen der Nachricht.
     * @param namemessage String
     */
    public void setNamemessage(String namemessage) {
        this.namemessage = namemessage;
    }

    /**
     * Hilfsvariable zum Anzeigen einer Nachricht
     * @return true || false
     */
    public boolean isRenderemailmessage() {
        return renderemailmessage;
    }

    /**
     * Setzen der Hilfsvariable zum Anzeigen einer Nachricht
     * @param renderemailmessage true || false
     */
    public void setRenderemailmessage(boolean renderemailmessage) {
        this.renderemailmessage = renderemailmessage;
    }

    /**
     * Liefert Nachricht.
     * @return String
     */
    public String getEmailmessage() {
        return emailmessage;
    }

    /**
     * Setzen der Nachricht
     * @param emailmessage String
     */
    public void setEmailmessage(String emailmessage) {
        this.emailmessage = emailmessage;
    }

    /**
     * Hilfsvariable zum Anzeigen einer Nachricht
     * @return true || false
     */
    public boolean isRenderpassmessage() {
        return renderpassmessage;
    }

    /**
     * Setzen der Hilfsvariable zum Anzeigen einer Nachricht
     * @param renderpassmessage true || false
     */
    public void setRenderpassmessage(boolean renderpassmessage) {
        this.renderpassmessage = renderpassmessage;
    }

    /**
     * Liefert Nachricht
     * @return 
     */
    public String getPassmessage() {
        return passmessage;
    }

    /**
     * Setzen der Nachricht
     * @param passmessage 
     */
    public void setPassmessage(String passmessage) {
        this.passmessage = passmessage;
    }
    
    /**
     * Hilfsvariable für Buttonsteuerung
     * @return true || false
     */
    public boolean isAenderbuttonsnamebestatetigen() {
        return aenderbuttonsnamebestatetigen;
    }

    /**
     * Setzen der Hilfsvariable für Buttonsteuerung
     * @param aenderbuttonsnamebestatetigen true || false
     */
    public void setAenderbuttonsnamebestatetigen(boolean aenderbuttonsnamebestatetigen) {
        this.aenderbuttonsnamebestatetigen = aenderbuttonsnamebestatetigen;
    }
    
    /**
     * Hilfsvariable für Buttonsteuerung
     * @return true || false
     */
    public boolean isAenderbuttonsemail() {
        return aenderbuttonsemail;
    }

    /**
     * Setzen der Hilfsvariable für Buttonsteuerung
     * @param aenderbuttonsemail 
     */
    public void setAenderbuttonsemail(boolean aenderbuttonsemail) {
        this.aenderbuttonsemail = aenderbuttonsemail;
    }

    /**
     * Liefert Namen
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setezn Namen
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }
   
    /**
     * Hilfsvariable Buttonsteuerung
     * @return true || false
     */
    public boolean isAenderbuttonsname() {
        return aenderbuttonsname;
    }

    /**
     * Setzen der Hilfsvariable Buttonsteuerung
     * @param aenderbuttonsname 
     */
    public void setAenderbuttonsname(boolean aenderbuttonsname) {
        this.aenderbuttonsname = aenderbuttonsname;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isAenderbuttonname() {
        return aenderbuttonname;
    }
    
    /**
     * Setzen der Hilfsvariable
     * @param aenderbuttonname true || false
     */
    public void setAenderbuttonname(boolean aenderbuttonname) {
        this.aenderbuttonname = aenderbuttonname;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isAenderbuttonpasswort() {
        return aenderbuttonpasswort;
    }

    /**
     * Setzen der Hilfsvariable
     * @param aenderbuttonpasswort true || false
     */
    public void setAenderbuttonpasswort(boolean aenderbuttonpasswort) {
        this.aenderbuttonpasswort = aenderbuttonpasswort;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isAenderbuttonemail() {
        return aenderbuttonemail;
    }

    /**
     * Setzen der Hilfsvariable
     * @param aenderbuttonemail true || false
     */
    public void setAenderbuttonemail(boolean aenderbuttonemail) {
        this.aenderbuttonemail = aenderbuttonemail;
    }
    
    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isNutzerpasswortvalueinput() {
        return nutzerpasswortvalueinput;
    }

    /**
     * Setzen der Hilfsvariable
     * @param nutzerpasswortvalueinput true || false
     */
    public void setNutzerpasswortvalueinput(boolean nutzerpasswortvalueinput) {
        this.nutzerpasswortvalueinput = nutzerpasswortvalueinput;
    }
    
    /**
     * Liefert instanz/Referenz auf die ManagedBean LoginLogout
     * @return LoginLogout Bean
     */
    public LoginLogout getLoginLogout() {
        return loginLogout;
    }

    /**
     * Setzen der Instanz/Referenz von LoginLogout ManagedBean
     * @param loginLogout LoginLogout Bean
     */
    public void setLoginLogout(LoginLogout loginLogout) {
        this.loginLogout = loginLogout;
    }
    
    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isNutzernamevalueoutput() {
        return nutzernamevalueoutput;
    }

    /**
     * Setzen der Hilfsvariable
     * @param nutzernamevalueoutput true || false
     */
    public void setNutzernamevalueoutput(boolean nutzernamevalueoutput) {
        this.nutzernamevalueoutput = nutzernamevalueoutput;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isNutzernamevalueinput() {
        return nutzernamevalueinput;
    }

    /**
     * Setzen der Hilfsvariable
     * @param nutzernamevalueinput true || false
     */
    public void setNutzernamevalueinput(boolean nutzernamevalueinput) {
        this.nutzernamevalueinput = nutzernamevalueinput;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isNutzeremailvalueoutput() {
        return nutzeremailvalueoutput;
    }

    /**
     * Setzen der Hilfsvariable
     * @param nutzeremailvalueoutput true || false
     */
    public void setNutzeremailvalueoutput(boolean nutzeremailvalueoutput) {
        this.nutzeremailvalueoutput = nutzeremailvalueoutput;
    }

    /**
     * Hilfsvariable
     * @return true || false
     */
    public boolean isNutzeremailvalueinput() {
        return nutzeremailvalueinput;
    }

    /**
     * Setzen der Hilfsvariable
     * @param nutzeremailvalueinput true || false
     */
    public void setNutzeremailvalueinput(boolean nutzeremailvalueinput) {
        this.nutzeremailvalueinput = nutzeremailvalueinput;
    }
   
    /**
     * Methode zur Steuerung der Ansicht zur Änderung des Namens
     */
    public void clickChangeName(){
        this.tempName=loginLogout.getRegistrierterNutzer().getName();
        this.nutzernamevalueinput=true;
        this.nutzernamevalueoutput=false;
        this.aenderbuttonname=false;
    }
    
    /**
     * Persistierung letztlich des neuen Namens.
     */
    public void changeName(){
        RequestContext context = RequestContext.getCurrentInstance();
        
        //Anzeigesteuerung
        this.aenderbuttonname=true;
        this.nutzernamevalueoutput=true;
        this.nutzernamevalueinput=false;
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        
        //Persist
        UserJPA udi = new UserJPA();
        udi.changeUserName(loginLogout.getRegistrierterNutzer());
        
        //Ändern des Namens im Header
        loginLogout.setName(loginLogout.getRegistrierterNutzer().getName());
        //update
        context.update("changename");
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Daten erfolgreich geändert.", ""));
    }
    
    /**
     * Methode zur Steuerung der Ansicht zur Änderung des Passwortes
     */
    public void clickChangePasswort(){
        this.nutzerpasswortvalueinput=true;
        this.aenderbuttonpasswort=false;
    }
    
    /**
     * Persistierung letztlich des neuen Passwortes, sofern sie übereinstimmen.
    */
    public void changePasswort(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestContext context = RequestContext.getCurrentInstance();
        if(passwort1!=null){
            this.passmessage="Passwort ok";
            this.renderpassmessage=true;
        }
        //setze Passwort des eingeloggten Nutzers neu
        loginLogout.getRegistrierterNutzer().setPasswort(passwort1);
        //Persist
        UserJPA udi = new UserJPA();
        udi.changeUserPasswort(loginLogout.getRegistrierterNutzer());
        
        //Anzeigesteuerung
        this.nutzerpasswortvalueinput=false;
        this.aenderbuttonpasswort=true;
        //update UI
        context.update("changepass");
        //Ausgabe einer UI Nachricht
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Daten erfolgreich geändert.", ""));
    }
    
    /**
     * Methode zur Steuerung der Ansicht zur Änderung des Emaiuladresse
     */
    public void clickChangeEmail(){
        this.tempEmail=loginLogout.getRegistrierterNutzer().getEMail();
        this.nutzeremailvalueoutput=false;
        this.aenderbuttonemail=false;
        this.nutzeremailvalueinput=true;
       
    }
    
    /**
     * Persistierung letztlich des neuen Emailadresse, sofern sie übereinstimmen.
     */
    public void changeMail(){
        RequestContext context = RequestContext.getCurrentInstance();
        //Anzeigesteuerung
        this.aenderbuttonemail=true;
        this.nutzeremailvalueoutput=true;
        this.nutzeremailvalueinput=false;
        FacesContext ctx = FacesContext.getCurrentInstance();
        //persist
        UserJPA udi = new UserJPA();
        udi.changeUserMail(loginLogout.getRegistrierterNutzer());
        //update
        context.update("changeemail");
        //UI Nachricht
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Daten erfolgreich geändert.", ""));
    }
    
    /**
     * Abbruch MEthode. Zurücksetzen der Hilfsvariablen
     */
    public void abbort(){
        this.aenderbuttonname=true;
        this.nutzernamevalueoutput=true;
        this.nutzernamevalueinput=false;
        this.aenderbuttonemail=true;
        this.nutzeremailvalueoutput=true;
        this.nutzeremailvalueinput=false;
        this.nutzerpasswortvalueinput=false;
        this.aenderbuttonpasswort=true;
        this.aenderbuttonsname=false;
        this.aenderbuttonsemail=false;
        this.aenderbuttonsnamebestatetigen=false;
           this.passwort1="";
    }
    
    /**
     * Abbrechen button bei Namensänderung
     * Zurücksetzen der Hilfsvariablen.
     */
    public void abbortName(){
        loginLogout.getRegistrierterNutzer().setName(this.tempName);
        this.aenderbuttonname=true;
        this.nutzernamevalueoutput=true;
        this.nutzernamevalueinput=false;
    }
   
    /**
     * Abbrechen Button bei Emailadressenänderung
     * Zurücksetzen der Hilfsvariablen.
    */
    public void abbortEmail(){
        loginLogout.getRegistrierterNutzer().setEMail(this.tempEmail);
        this.aenderbuttonemail=true;
        this.nutzeremailvalueoutput=true;
        this.nutzeremailvalueinput=false;
    }
    
    /**
     * Abbrechen Button bei Passwortsänderung
     * Zurücksetzen der Hilfsvariablen.
    */
    public void abbortPass(){
         this.nutzerpasswortvalueinput=false;
         this.aenderbuttonpasswort=true;
         this.passwort1=" ";
    }
}
