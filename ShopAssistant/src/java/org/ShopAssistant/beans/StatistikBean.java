package org.ShopAssistant.beans;

import java.io.Serializable;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.ShopAssistant.Component.ShopList;
import org.ShopAssistant.Entities.FavoritesResult;
import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BarChartSeries;
/**
 * JavaBean für Statistik
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class StatistikBean implements Serializable{
    
    //Serial wegen java.io.InvalidClassException
    private static final long serialVersionUID = 1L;
    
    //dient der Kommunikation mit der LoginLogout Bean
    @ManagedProperty(value="#{loginLogout}")
    private LoginLogout loginLogout;
    
    //dient der Kommunikation mit der ShopList Bean
    @ManagedProperty(value="#{shopList}")
    private ShopList shopList;

    //Model des Barcharts
    private BarChartModel barModel;
    
    /**
     * Liefert die Instanz/Referenz der ShopList
     * @return shopList
     */
    public ShopList getShopList() {
        return shopList;
    }

    /**
     * Sezen der Instanz/Referenz von ShopList
     * @param shopList ShopList
     */
    public void setShopList(ShopList shopList) {
        this.shopList = shopList;
    }
    
    /**
     * Liefert die Instanz/Referenz der LoginLogout
     * @return LoginLogout
     */
    public LoginLogout getLoginLogout() {
        return loginLogout;
    }

    /**
     * Sezen der Instanz/Referenz von LoginLogout
     * @param loginLogout LoginLogout
     */
    public void setLoginLogout(LoginLogout loginLogout) {
        this.loginLogout = loginLogout;
    }
    
    /**
     * Liefert das BarchartModel
     * @return Model
     */
    public BarChartModel getBarModel() {
        if (barModel == null) {
            createLineModel();
        }
        return barModel;
    }

    /**
     * Setzen des Barchart Models
     * @param lineModel BarChartModel
     */
    public void setBarModel(BarChartModel lineModel) {
        this.barModel = lineModel;
    }
    
    /**
     * leerer Konstruktor
     */
    public StatistikBean(){
    }
    
    /**
     * Initialisierung des BarChartModels
     * @return BarChartModel
     */
    public BarChartModel initBarModel() {
        //neues Model
        BarChartModel model = new BarChartModel();;
        //neue Serie
        BarChartSeries favorites = new BarChartSeries();
        //Label setzen
        favorites.setLabel("Favoriten");
        //datensetzen
        if((!shopList.getFavoritenListe().isEmpty())){
            
            //Favoriten aus ShopList holen und der Serie adden
            for(int i=0; i < shopList.getFavoritenListe().size(); i++){
                int wert=shopList.getFavoritenListe().get(i).getAnzahl();
                favorites.set(i, wert);
            }
            //dem Model hinzufügen
            model.addSeries(favorites);
        }
        //Model zurück
        return model;
    }
    
    /**
     * Methode zur Kreierung der Chart
     */
    public void createLineModel() {
        //Generierung der Favoriten
        shopList.generateFavoriten();
        RequestContext c = RequestContext.getCurrentInstance();
        //Initialisierung
        barModel = initBarModel();
        
        //setze Titel
        barModel.setTitle("Favoriten Chart");
        //Setze Legende north east
        barModel.setLegendPosition("ne");
        //zeige Data Tooltip
        barModel.setShowDatatip(true);

        //X-Achse und Beschriftung von 0-10
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Listenpositionen");
        xAxis.setMin(0);
        xAxis.setMax(9);
        xAxis.setTickCount(1);

        //Y-Achse und Beschriftung in Abhängigkeit der Häufigkeit eines Favs
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Anzahl");
        yAxis.setMin(0);
        yAxis.setTickCount(1);
        if(shopList.getFavoritenListe().isEmpty()){
            yAxis.setMax(1);
        }else{
            int yanzahl = shopList.getFavoritenListe().get(0).getAnzahl()+5;
            yAxis.setMax(yanzahl);
        }

        this.setBarModel(barModel);
        //update
        c.update("chart");
    }
    
    
}
