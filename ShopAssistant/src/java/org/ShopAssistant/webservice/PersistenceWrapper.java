/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.webservice;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Stephan
 */
public final class PersistenceWrapper {
    private static PersistenceWrapper persistence = null;

    // An authentication token storage which stores <service_key, auth_token>.
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("ShopAssistantPU");

    private PersistenceWrapper() {
    }

    public static synchronized PersistenceWrapper getInstance() {
        if ( persistence == null ) {
            persistence = new PersistenceWrapper();
        }
        return persistence;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
}