package org.ShopAssistant.webservice;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
/**
 *
 * @author Stephan + Andre
 */

@Provider
public class CORSFilter implements ContainerResponseFilter {

   @Override
   public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext cres) throws IOException {
        cres.getHeaders().add("Access-Control-Allow-Origin", "*");
        cres.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
        cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        cres.getHeaders().add("Access-Control-Max-Age", "1209600");
        cres.getHeaders().add( "Access-Control-Allow-Headers", "X-AUTH-TOKEN" );
        cres.getHeaders().add( "Access-Control-Allow-Headers", "IONIC-CLIENT" );
        
        //Setzen des Charstes, da sonst die Umlaute nicht angezeigt werden!
        MediaType contentType2 = cres.getMediaType();
        try{
            cres.getHeaders().putSingle("Content-Type", contentType2.toString() + ";charset=UTF-8");
        }catch(java.lang.NullPointerException e){
        }
   }
}
