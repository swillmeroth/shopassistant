package org.ShopAssistant.webservice;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class RequestAuthFilter implements ContainerRequestFilter{

    private final static Logger log = Logger.getLogger(RequestAuthFilter.class.getName() );

    @Override
    public void filter( ContainerRequestContext requestCtx) throws IOException {

        String path = requestCtx.getUriInfo().getPath();
        log.log(Level.INFO, "Filtering request path: {0}", path);
        
        MediaType contentType = requestCtx.getMediaType();
        String content_string = (contentType != null) ? contentType.toString() : "";
        requestCtx.getHeaders().putSingle("Content-Type", content_string + ";charset=UTF-8");

        // IMPORTANT!!! First, Acknowledge any pre-flight test from browsers for this case before validating the headers (CORS stuff)
        if ( requestCtx.getRequest().getMethod().equals( "OPTIONS" ) ) {
            requestCtx.abortWith( Response.status( Response.Status.OK ).build() );

            return;
        }


        ClientAuthenticator authenticator = ClientAuthenticator.getInstance();

        String isMobile =  requestCtx.getHeaderString("IONIC-CLIENT");
        
        if( isMobile == null)
            return;
        
        System.out.println("Header: " + isMobile);
        
        // For any other methods besides login, the authToken must be verified
        if ( isMobile.equals("true") && !path.startsWith( "/auth") ) 
        {
            System.out.println("Now filtering!");
            String authToken = requestCtx.getHeaderString( "X-AUTH-TOKEN" );

            // if it isn't valid, just kick them out.
            if ( !authenticator.isAuthTokenValid( authToken ) ) {
                log.info("INVALID!");
                requestCtx.abortWith( Response.status( Response.Status.UNAUTHORIZED ).build() );
            }
        }
    }

   
    

}
