/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.webservice;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stephan
 */
public final class ClientAuthenticator {
    private static ClientAuthenticator authenticator = null;

    // An authentication token storage which stores <service_key, auth_token>.
    private final Map<String, String> authTokens = new HashMap();

    private ClientAuthenticator() {
    }

    public static synchronized ClientAuthenticator getInstance() {
        if ( authenticator == null ) {
            authenticator = new ClientAuthenticator();
        }
        return authenticator;
    }

    public void storeAuthToken( String username, String authToken ) {
        
            authTokens.put(authToken, username);
    }

    public boolean isAuthTokenValid( String authToken ) {
        return authTokens.containsKey(authToken);
    }

    public void removeAuthToken( String authToken ) {
        authTokens.remove( authToken );
    }
}