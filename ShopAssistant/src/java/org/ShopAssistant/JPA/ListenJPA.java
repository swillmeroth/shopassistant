package org.ShopAssistant.JPA;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Listen;
import org.ShopAssistant.Entities.Listenpositionen;
import org.ShopAssistant.Jersey.ListWebservice_JerseyClient;
import org.ShopAssistant.LogicClasses.NonMobileJSONObject;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 * Klasse mit sämtlichen Methoden, die dazu da sind Listenoperationen
 * durchzuführen
 * @author Andre
 */
public class ListenJPA implements Serializable{

    /**
     * Liefert alle Listen eines Nutzers
     * @param id ID des Nutzers
     * @return Liste von Listen
     */
    public List<Listen> getListenByUserID(int id)
    {
        try {
            List<Listen> resultList;
            
            ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", id);
            
            Response response = client.getLists(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity())
                return null;
            
            resultList = response.readEntity(new GenericType<List<Listen>>() {});
            client.close();
            return resultList;
            
        } catch (JSONException ex) {
            Logger.getLogger(ListenJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Liefert alle Listenpositionen zu einer Liste
     * @param id ID einer Liste
     * @return Liste an Listenpositionen
     */
    public List<Listenpositionen> getListPositionenByListID(int id)
    {   
        try {
            List<Listenpositionen> resultList;
            
            ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("listid", id);
            
            Response response = client.getItems(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity())
                return null;
            
            resultList = response.readEntity(new GenericType<List<Listenpositionen>>() {});
            client.close();
            return resultList;
            
        } catch (JSONException ex) {
            Logger.getLogger(ListenJPA.class.getName()).log(Level.SEVERE, null, ex);
           return null;
        }
    }
     
    /**
     * Hinzufügen einer Liste
     * @param a Liste
     */
    public void addList(Listen a){
        ListWebservice_JerseyClient client = new ListWebservice_JerseyClient(); 
        client.addList(a);
        client.close();
    }
    
    /**
     * Hinzufügen eines Eintrags
     * @param a Listenposition
     */
    public void addNewEintrag(Listenpositionen a){
        ListWebservice_JerseyClient client = new ListWebservice_JerseyClient(); 
        client.addItem(a);
        client.close();
    }
    
    /**
     * Löschen einer Liste
     * @param id ID der Liste
     */
    public void delList(int id)
    {
        try {
            ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("listId", id);
            
            client.removeList(request.toString());
            client.close();
        } catch (JSONException ex) {
             Logger.getLogger(ListenJPA.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    /**
     * Methode zum Ablegen (merge) der veränderten Listen
     * @param li Liste von Listen
     */
    public void persistChangedListen(List<Listen> li){
        ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
        client.mergeLists(li);
        client.close();
    }
    
    /**
     * Methode zum Ablegen (merge) der veränderten Listenpositionen
     * @param l Liste von Listenpositionen
     */
    public void persistChangedPositionen(List<Listenpositionen> l){
        ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN);
        
        for(Listenpositionen pos : l) {
            if(pos.getGekauftDatum() != null) {
                Date date = pos.getGekauftDatum();
                String dateString = df.format(date);
                pos.setGekauftDatumString(dateString);
            }
        }
        
        client.mergeItems(l);
        client.close();
    }
    
    /**
     * Löschen eines Eintrags
     * @param id Id des Eintrags
     */
    public void deleteEntry(int id){
        try {
            ListWebservice_JerseyClient client = new ListWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("itemid", id);
            
            client.removeItem(request.toString());
            client.close();
        } catch (JSONException ex) {
             Logger.getLogger(ListenJPA.class.getName()).log(Level.SEVERE, null, ex);
        }          
    }
    
    /**
     * Hinzufügen eines Favoriten
     * @param lis Listenposition
     */
    public void addFavorit(Listenpositionen lis){
        addNewEintrag(lis);
    }
    
    /**
     * Methode zum Setzen der Gruppe zu einer Liste
     * Ruft die Methode persistchangedListen() auf.
     * @param l Liste von Listen
     */
    public void setListGruppe(List<Listen> l){
        persistChangedListen(l);
    }
}
