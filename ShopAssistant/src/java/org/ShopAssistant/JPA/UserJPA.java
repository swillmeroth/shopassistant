
package org.ShopAssistant.JPA;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Jersey.AuthWebservice_JerseyClient;
import org.ShopAssistant.Jersey.ProfileWebservice_JerseyClient;
import org.ShopAssistant.LogicClasses.NonMobileJSONObject;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 * Klasse für Benutzer Aktionen
 * @author Andre
 */
public class UserJPA implements Serializable{

    /** 
     * Methode zum Registrieren eines Neu-Nutzers
     * @param nutzer zu registriender Benutzer
     * @return boolean true || false
     */
    public boolean registerUser(Benutzer nutzer) {
        try {
          //Name, Email, und Passwort des NeuNutzers
          String name = nutzer.getName();
          String pass = nutzer.getPasswort();
          String email = nutzer.getEMail();

          //ClientInstanz
          AuthWebservice_JerseyClient client = new AuthWebservice_JerseyClient();

          // request Object
          JSONObject request = new NonMobileJSONObject();
          request.put("username", name);
          request.put("email", email);
          request.put("password1", pass);


          //Mittels Client Aufruf der RegistrierungsMethode
          Response response = client.register(request.toString());
            
          if(response.getStatus()==400){
              client.close();
              return false;
          }else{
              client.close();
              return ( response.getStatus() == 200 && response.hasEntity() );
          }
          

      } catch (JSONException ex) {
          Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
          return false;
      }    
    }
    
    /**
     * Entfernen eines Nuters
     * @param nutzer gewünschter Benutzer
     */
    public void removeUser(Benutzer nutzer){
      
        try {
            ProfileWebservice_JerseyClient client = new ProfileWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", nutzer.getId());
            
            client.removeUser(request.toString());
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Login Methode
     * @param nutzer einzuloggender Nutzer
     * @return eingeloggter Nutzer
     */
    public Benutzer loginUser(Benutzer nutzer){
        try {
            String name = nutzer.getName();
            String pass = nutzer.getPasswort();
            
            AuthWebservice_JerseyClient client = new AuthWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("username", name);
            request.put("password", pass);
            
            Response response = client.login(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity())
            {
                client.close();
                return null;
            }
            client.close();
            return response.readEntity(Benutzer.class);
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Methode zum Ändern des Usernamen
     * @param nutzer Entity des eingeloggten Nutzers
     */ 
    public void changeUserName(Benutzer nutzer){
        try {            
            ProfileWebservice_JerseyClient client = new ProfileWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", nutzer.getId());
            request.put("newname", nutzer.getName());
            
            client.changeName(request.toString());
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * Methode zum Ändern des User EMailadresse
     * @param nutzer Entity des eingeloggten Nutzers
     */
    public void changeUserMail(Benutzer nutzer){
         try {            
            ProfileWebservice_JerseyClient client = new ProfileWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", nutzer.getId());
            request.put("email", nutzer.getEMail());
            
            client.changeMail(request.toString());
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * Methode zum Ändern des User Passworts
     * @param nutzer Benutzer Entity des eingeloggten Nutzers
     */
    public void changeUserPasswort(Benutzer nutzer){
          try {            
            ProfileWebservice_JerseyClient client = new ProfileWebservice_JerseyClient();
            
            // request Object
              System.out.println("PASSWORT to CHANGE: " + nutzer.getPasswort());
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", nutzer.getId());
            request.put("password", nutzer.getPasswort());
            
            client.changePassword(request.toString());
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Liefert den Benutzer zu einer Emailadresse
     * @param mail Mailadresse
     * @return Benutzer
     */
    public Benutzer getUser(String mail) {
        try {            
            
            ProfileWebservice_JerseyClient client = new ProfileWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("mail", mail);
            
            Response response = client.getUserByMail(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity())
            {
                client.close();
                return null;
            }
            client.close();
            return response.readEntity(Benutzer.class);
            
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    /**
     * Methode Passwort vergessen
     * @param name des Users
     * @return boolean true: Nutzer gefunden -> Mail 
     *                  false: Nutzer nicht gefunden (NoResultException)    
     */
    public boolean checkUser(String name){
        try {
            AuthWebservice_JerseyClient client = new AuthWebservice_JerseyClient();
            
            // request Object
            System.out.println("Namen zu Suchen: " + name);
            JSONObject request = new NonMobileJSONObject();
            request.put("username", name);
            
            Response response = client.forgotPassword(request.toString());
            client.close();
            // check if Response is 200 and has user object
            return response.getStatus() == 200;
        } catch (JSONException ex) {
            Logger.getLogger(UserJPA.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
