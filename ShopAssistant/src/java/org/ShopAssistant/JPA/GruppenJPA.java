package org.ShopAssistant.JPA;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.Benutzer;
import org.ShopAssistant.Entities.Benutzergruppen;
import org.ShopAssistant.Entities.Gruppen;
import org.ShopAssistant.Jersey.GroupWebservice_JerseyClient;
import org.ShopAssistant.LogicClasses.NonMobileJSONObject;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 * Klasse für die Gruppenaktionen
 * @author Andre
 */
public class GruppenJPA implements Serializable{
    
    /**
     * Liefert die Gruppen im Bezug auf die UserID
     * @param id ID des Nutzers
     * @return Liste von Gruppen
     */
    public List<Gruppen> getByUserID(int id){
        
        try {
            //Jersey Client
            GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", id);
            
            //Aufruf der MEthode im Client
            Response response = client.getUsersGroups(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity()){
                client.close();
                return null;
            }
            
            //Result
            List<Gruppen> resultList = response.readEntity(new GenericType<List<Gruppen>>() {});
            //Client schließen
            client.close();
            return resultList;
            
        } catch (JSONException ex) {
           Logger.getLogger(GruppenJPA.class.getName()).log(Level.SEVERE, null, ex);
           return null;
        }          
    }
    
    
    /**
     * Methode zum Erstellen einer neuen Gruppe
     * @param b aktuelle Benutzer
     * @param g die neue Gruppe
     */
    public void erstelleGruppe(Benutzer b, Gruppen g) {
         try {
            //JErsey Client
            GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", b.getId());
            request.put("groupname", g.getName());
            
            //Aufruf der MEthode im Client
            client.addGroup(request.toString());
            //Client schließen
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(GruppenJPA.class.getName()).log(Level.SEVERE, null, ex);
        }          
    }

    /**
     * Liefert die ID des Gruppenobjektes im Bezug auf den Namen
     * @param g Gruppe
     * @return Integer
     */
    public int findGruppeByGruppenNamen(Gruppen g) {  
        
        GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
        Response response = client.findByName(g.getName());
        
        if( response.getStatus() != 200 || !response.hasEntity())
            return -1;
        
        Gruppen foundGroup = response.readEntity(Gruppen.class);
        client.close();
        return foundGroup.getId();
    }
    
    /**
     * Zusammenführen von Benutzer und Gruppe in BEnutzergruppen
     * @param b aktuelle Benutzer
     * @param g gewählte Gruppe
     * @return String
     */    
    public String addUsertoBenutzergruppen(Benutzer b, Gruppen g) {
         try {
            GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("groupid", g.getId());
            request.put("username", b.getName());
            
            Response response = client.addMember(request.toString());
            client.close();
            return response.toString();
        } catch (JSONException ex) {
            Logger.getLogger(GruppenJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }          
    }

    /**
     * Methode zum Löschen einer Gruppe
     * @param g Selektierte Gruppe
     * @param b aktueller Benutzer
     */
    public void loescheGruppe(Gruppen g, Benutzer b) {
        try {
            GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("groupid", g.getId());
            request.put("userid", b.getId());
            
            client.removeGroup(request.toString());
            client.close();
        } catch (JSONException ex) {
            Logger.getLogger(GruppenJPA.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }

    /**
     * Entfernen eine Nutzers von einer Gruppe
     * @param n aktueller Benutzer
     * @param g selektierte Gruppe
     */
    public void removeUservonGruppe(Benutzer n, Gruppen g) {
        try {
            GroupWebservice_JerseyClient client = new GroupWebservice_JerseyClient();
               
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("groupid", g.getId());
            request.put("userid", n.getId());
            
            client.leaveGroup(request.toString());
                client.close();
        } catch (JSONException ex) {
            Logger.getLogger(GruppenJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
