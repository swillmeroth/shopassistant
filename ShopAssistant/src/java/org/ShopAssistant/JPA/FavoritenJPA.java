/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ShopAssistant.JPA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import org.ShopAssistant.Entities.FavoritesResult;
import org.ShopAssistant.Jersey.FavoritenWebservice_JerseyClient;
import org.ShopAssistant.LogicClasses.NonMobileJSONObject;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 * Klasse um die Favoriten eines Nutzers zu bekommen.
 * Top 10
 * @author Andre
 */
public class FavoritenJPA implements Serializable{
    
    private int ANZAHL_FAVORITEN = 10;
    
    /**
     * Methode um an die Favoriten zu kommen.
     * @param userid ID des Nutzers
     * @return Liste FavoritenResult
     */
    public List<FavoritesResult> getFavorites(int userid) {
        try {
            List<FavoritesResult> resultList=new ArrayList();
            resultList.clear();
            
            //Aufruf des JerseyClients
            FavoritenWebservice_JerseyClient client = new FavoritenWebservice_JerseyClient();
            
            // request Object
            JSONObject request = new NonMobileJSONObject();
            request.put("userid", userid);
            request.put("favcount", ANZAHL_FAVORITEN);
            
            //Aufruf der JerseyMethode getUsersFavorites(..)
            Response response = client.getUsersFavorites(request.toString());
            
            // check if Response is 200 and has user object
            if( response.getStatus() != 200 || !response.hasEntity())
            {
                //Client schließen
                client.close();
                return null;
            }
            
            //ResultListe
            resultList = response.readEntity(new GenericType<List<FavoritesResult>>() {});
            //Client schließen
            client.close();
            return resultList;
            
        } catch (JSONException ex) {
            Logger.getLogger(FavoritenJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
       
    }
    
}
