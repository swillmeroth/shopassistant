-- MySQL Script generated by MySQL Workbench
-- 02/20/15 11:43:25
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ShopAssistantDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ShopAssistantDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ShopAssistantDB` DEFAULT CHARACTER SET utf8 ;
USE `ShopAssistantDB` ;

-- -----------------------------------------------------
-- Table `ShopAssistantDB`.`Gruppen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShopAssistantDB`.`Gruppen` ;

CREATE TABLE IF NOT EXISTS `ShopAssistantDB`.`Gruppen` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ShopAssistantDB`.`Benutzer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShopAssistantDB`.`Benutzer` ;

CREATE TABLE IF NOT EXISTS `ShopAssistantDB`.`Benutzer` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `EMail` VARCHAR(255) NOT NULL,
  `Passwort` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ShopAssistantDB`.`Listen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShopAssistantDB`.`Listen` ;

CREATE TABLE IF NOT EXISTS `ShopAssistantDB`.`Listen` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Ersteller` INT NULL DEFAULT NULL,
  `Gruppe` INT NULL DEFAULT NULL,
  `IstGeloescht` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  INDEX `Ersteller_idx` (`Ersteller` ASC),
  INDEX `Gruppe_idx` (`Gruppe` ASC),
  CONSTRAINT `Ersteller`
    FOREIGN KEY (`Ersteller`)
    REFERENCES `ShopAssistantDB`.`Benutzer` (`ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `Gruppe`
    FOREIGN KEY (`Gruppe`)
    REFERENCES `ShopAssistantDB`.`Gruppen` (`ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ShopAssistantDB`.`Listenpositionen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShopAssistantDB`.`Listenpositionen` ;

CREATE TABLE IF NOT EXISTS `ShopAssistantDB`.`Listenpositionen` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Liste` INT NOT NULL,
  `Name` VARCHAR(255) NOT NULL,
  `Geschaeftsname` VARCHAR(255) NULL,
  `Geschaeftsdetails` VARCHAR(255) NULL,
  `Anzahl` INT NOT NULL DEFAULT 1,
  `Kommentar` TEXT NULL,
  `GekauftVon` INT NULL,
  `ErstelltVon` INT NULL,
  `GekauftDatum` DATE NULL DEFAULT NULL,
  `Gekauft` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  INDEX `Liste_idx` (`Liste` ASC),
  INDEX `ErstelltVon_idx` (`ErstelltVon` ASC),
  INDEX `GekauftVon_idx` (`GekauftVon` ASC),
  CONSTRAINT `Liste`
    FOREIGN KEY (`Liste`)
    REFERENCES `ShopAssistantDB`.`Listen` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ErstelltVon`
    FOREIGN KEY (`ErstelltVon`)
    REFERENCES `ShopAssistantDB`.`Benutzer` (`ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `GekauftVon`
    FOREIGN KEY (`GekauftVon`)
    REFERENCES `ShopAssistantDB`.`Benutzer` (`ID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ShopAssistantDB`.`BenutzerGruppen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShopAssistantDB`.`BenutzerGruppen` ;

CREATE TABLE IF NOT EXISTS `ShopAssistantDB`.`BenutzerGruppen` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `BenutzerID` INT NOT NULL,
  `GruppenID` INT NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `BenutzerID_idx` (`BenutzerID` ASC),
  INDEX `GruppenID_idx` (`GruppenID` ASC),
  CONSTRAINT `BenutzerID`
    FOREIGN KEY (`BenutzerID`)
    REFERENCES `ShopAssistantDB`.`Benutzer` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `GruppenID`
    FOREIGN KEY (`GruppenID`)
    REFERENCES `ShopAssistantDB`.`Gruppen` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
